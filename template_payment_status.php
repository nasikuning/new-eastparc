<?php

/* Template Name: Payment Status */

global $wpdb;

// Get transaction based on given transaction number
$transactionNo = $_POST['transactionNo'];
$sql = "SELECT * FROM " . $wpdb->prefix . "ss_books_trans WHERE trans_no='" . $transactionNo . "'";
$trans = $wpdb->get_row($sql);

$authkey    = (isset($trans->authkey)) ? $trans->authkey : '';
$amount     = (isset($trans->total_amount)) ? $trans->total_amount : '';
$transno    = (isset($trans->trans_no)) ? $trans->trans_no : '';
$status     = (isset($trans->status)) ? $trans->status : '';
$transdate  = (isset($trans->trans_date)) ? $trans->trans_date : '';
$transdatetime = strtotime($transdate);
$expiredtime = $transdatetime + 60*60;
$this_time = strtotime(gmdate('Y-m-d H:i:s',time()+(25200)));

if ($_POST['approvalCode']){
    global $wpdb;
    $update['approvalCode'] = $_POST['approvalCode'];
    $wpdb->query(sql_update($wpdb->prefix.'ss_books_trans', 'trans_no', $transaction_no, $update));
}



function show_fail() {
    global $wpdb;
    $update['message'] = 'Your transaction cannot be processed.';
    $wpdb->query(sql_update($wpdb->prefix.'ss_books_trans', 'trans_no', $transaction_no, $update));
    echo '<OutputPaymentIPAY>
            <status>01</status>
            <reason>
                <english>Your transaction cannot be processed.</english>
                <indonesian>Transaksi Anda tidak dapat diproses.</indonesian>
            </reason>
            <additionalData>Additional Data if any</additionalData>
        </OutputPaymentIPAY>';

}

function show_success($transaction_no) {
    global $wpdb;
    
    // Update transaction status to 'paid'
    setcookie("success_books", $transaction_no, time() + (3600 * 12 * 30 * 365));
    $update['status'] = 'paid';
    $wpdb->query(sql_update($wpdb->prefix.'ss_books_trans', 'trans_no', $transaction_no, $update));
    $update['message'] = 'Success.';
    $wpdb->query(sql_update($wpdb->prefix.'ss_books_trans', 'trans_no', $transaction_no, $update));
    echo '<OutputPaymentIPAY>
            <status>00</status>
            <reason>
                <english>Success</english>
                <indonesian>Sukses</indonesian>
            </reason>
            <additionalData>Additional Data if any</additionalData>
        </OutputPaymentIPAY>';
}

function show_paid() {
    global $wpdb;
    $update['message'] = 'Your transaction has been paid.';
    $wpdb->query(sql_update($wpdb->prefix.'ss_books_trans', 'trans_no', $transaction_no, $update));

    echo '<OutputPaymentIPAY>
            <status>01</status>
            <reason>
                <english>Your transaction has been paid.</english>
                <indonesian>Transaksi Anda telah dibayar.</indonesian>
            </reason>
            <additionalData>Additional Data if any</additionalData>
        </OutputPaymentIPAY>';
}


function show_expired() {
    global $wpdb;
    $update['message'] = 'Your transaction has expired.';
    $wpdb->query(sql_update($wpdb->prefix.'ss_books_trans', 'trans_no', $transaction_no, $update));

    echo '<OutputPaymentIPAY>
            <status>01</status>
            <reason>
                <english>Your transaction has expired.</english>
                <indonesian>Transaksi Anda telah kedaluwarsa.</indonesian>
            </reason>
            <additionalData>Additional Data if any</additionalData>
        </OutputPaymentIPAY>';
}

// Let's start the payment flags
if ($_POST['transactionNo'] == $transno && $_POST['currency'] == 'IDR' && $_POST['totalAmount'] == $amount && $_POST['authKey'] == $authkey) {
    if ($status == 'unpaid') {
        show_success($transno);
    } else if ($this_time > $expiredtime){
        show_expired();
    } else {
        show_paid();
    }
} else {
    show_fail();
}

exit;


