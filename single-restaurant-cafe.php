<?php get_header(); ?>
<div id="wrapper" class="page">
  <?php if (have_posts()): while (have_posts()) : the_post(); ?>
  <?php
    $images = rwmb_meta( 'indohotels_imgRestaurantCafe', 'size=big-slider' ); // Since 4.8.0
    if ( !empty( $images ) ) : ?>
      <div class="section main-slider slider-resto">
        <div id="slider-main" class="owl-carousel">
            <?php 
          foreach ( $images as $image ) {
            echo '<div class="owl-slide" style="background-image: url(\''. $image['full_url'].'\')"></div>';
            }
        ?>
        </div>
        <!-- end .slider-main -->
    </div>
    <!-- end .main-slider -->
    <?php endif; ?>

  <div class="container">
    <div class="section content-resto resto-detail">
      <h1 class="heading-title" <?php echo empty( $images ) ? 'style="margin-top:80px"' : ''; ?>>
        <?php the_title(); ?>
      </h1>
      <div class="row margin-blarge">
        <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="box-schedule">
            <ul class="clearfix">
              <?php if(!empty(rwmb_meta('res_cuisine_type'))) :?>
              <li>
                <span class="scleft"><?php pll_e('Cuisine Type', karisma_text_domain); ?></span>
                <span class="dot">:</span>
                <span class="scright"><?php echo rwmb_meta( 'res_cuisine_type' ); ?></span>
              </li>
              <?php endif; ?>
              <?php if(!empty(rwmb_meta('res_capacity'))) :?>
              <li>
                <span class="scleft"><?php pll_e('Capacity', karisma_text_domain); ?></span>
                <span class="dot">:</span>
                <span class="scright"><?php echo rwmb_meta( 'res_capacity' ); ?> guests</span>
              </li>
              <?php endif; ?>
              <?php if(!empty(rwmb_meta('res_location'))) :?>
              <li>
                <span class="scleft"><?php pll_e('Location', karisma_text_domain); ?></span>
                <span class="dot">:</span>
                <span class="scright"><?php echo rwmb_meta( 'res_location' ); ?></span>
              </li>
              <?php endif; ?>
              <?php if(!empty(rwmb_meta('res_opening_hours'))) :?>
              <li>
                <span class="scleft"><?php pll_e('Opening Hours', karisma_text_domain); ?></span>
                <span class="dot">:</span>
                <span class="scright"><?php echo rwmb_meta( 'res_opening_hours' ); ?></span>
              </li>
              <?php endif; ?>
              <?php if(!empty(rwmb_meta('res_telephone'))) :?>
              <li>
                <span class="scleft"><?php pll_e('Telephone', karisma_text_domain); ?></span>
                <span class="dot">:</span>
                <span class="scright"><?php echo rwmb_meta( 'res_telephone' ); ?></span>
              </li>
              <?php endif; ?>
            </ul>
          </div>
          <!-- end .box-schedule -->
        </div>
        <!-- end .col-md-4 -->
        <div class="col-md-8 col-sm-8 col-xs-12">
          <?php the_content(); ?>
        </div>
        <!-- end .col-md-4 -->
      </div>
      <!-- end .row -->
    </div>
    <!-- end .content-intro -->

  </div>
  <!-- end .container -->
  <div class="main-amenities">
    <h4 class="text-center"><?php pll_e( 'Our Food', karisma_text_domain ); ?></h4>
    <div class="amenities-box">
      <div class="container">
        <div class="row">

          <?php
            $group_values = rwmb_meta( 'standard' );
            $field_key = 'category-menu';
            $sub_field_key = 'food-menu';

            if((count($group_values)) == 3) {
              $wrapper_class_col = '<div class="col-md-4 col-sm-4 col-xs-12">';
            } elseif ((count($group_values)) == 2) {
              $wrapper_class_col = '<div class="col-md-6 col-sm-6 col-xs-12">';
            } else {
              $wrapper_class_col = '<div class="col-md-3 col-sm-3 col-xs-12">';
            }


            if ( ! empty( $group_values ) ) {
                foreach ( $group_values as $group_value ) {
                    $category_menu_value = isset( $group_value[$field_key] ) ? $group_value[$field_key] : '';
                    echo $wrapper_class_col;
                    echo '<p>';
                    echo '<strong>';
                    echo $category_menu_value; // Display category menu value
                    echo '</p>';
                    echo '</strong>';
                    echo '<ul>';
                      foreach ( $group_value[$sub_field_key]['single-food-menu'] as $food_menu ) {
                        $food_menu_value = isset($food_menu ) ? $food_menu : '';
                        echo '<li>';
                        echo $food_menu_value; // Display sub-field value
                        echo '</li>';

                    }
                    echo '</ul>';
                    echo '</div>';
                }
            }
          ?>
        </div>
        <!-- end .row -->
      </div>
      <!-- end .container -->
    </div>
    <!-- end .amenities-box -->
  </div>
  <!-- end .main-amenities -->
  <?php endwhile; ?>

  <?php else: ?>

  <!-- article -->
  <article>

    <h1>
      <?php pll_e( 'Sorry, nothing to display.', karisma_text_domain ); ?>
    </h1>

  </article>
  <!-- /article -->

  <?php endif; ?>
</div>
<!-- end .content -->

<?php get_footer(); ?>
