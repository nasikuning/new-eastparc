## 1.0.4 (15 Sept, 2017)

* Add Popup Options

## 1.0.3 (14 Sept, 2017)

* Add option for moneyback guarantee
* Fix missing style

## 1.0.2 (12 Sept, 2017)

* Add Booking engine JogjaHotels
* Fix missing style

## 1.0.1 (11 Sept, 2017)

* Fix Menu Dashboard for Restaurant & Cafe page
* Fix Navbar can't be click
* Add dropdown simbol in navbar

## 1.0.0 (21 August, 2017)

* Initial commit