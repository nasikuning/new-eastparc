<?php get_header(); ?>
<div id="wrapper" class="page">
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
      <?php
    $images = rwmb_meta( 'indohotels_hotelInfo', 'size=big-slider' ); // Since 4.8.0
    if ( !empty( $images ) ) : ?>
      <div class="section main-slider slider-resto">
        <div id="slider-main" class="owl-carousel">
            <?php 
          foreach ( $images as $image ) {
            echo '<div class="owl-slide" style="background-image: url(\''. $image['full_url'].'\')"></div>';
            }
        ?>
        </div>
        <!-- end .slider-main -->
    </div>
    <!-- end .main-slider -->
    <?php endif; ?>

      <div class="container">
        <div class="section content-resto resto-detail">
          <h1 class="heading-title" <?php echo empty( $images ) ? 'style="margin-top:80px"' : ''; ?>>
            <?php the_title(); ?>
          </h1>
          <div class="row margin-blarge">
            <!-- end .col-md-4 -->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <?php the_content(); ?>
              <?php if(rwmb_meta('indohotels_button_action_url') && rwmb_meta('indohotels_button_action_text')) : ?>
                <div class="text-center">
                  <a class="nbutton" href="<?php pll_e(rwmb_meta('indohotels_button_action_url'), karisma_text_domain); ?>"><?php pll_e(rwmb_meta('indohotels_button_action_text'), karisma_text_domain); ?></a>
                </div>
              <?php endif; ?>
            </div>
            <!-- end .col-md-4 -->
          </div>
          <!-- end .row -->
        </div>
        <!-- end .content-intro -->

      </div>
      <!-- end .container -->
    <?php endwhile; ?>

  <?php else : ?>

    <!-- article -->
    <article>

      <h1>
        <?php pll_e('Sorry, nothing to display.', karisma_text_domain); ?>
      </h1>

    </article>
    <!-- /article -->

  <?php endif; ?>
</div>
<!-- end .content -->

<?php get_footer(); ?>