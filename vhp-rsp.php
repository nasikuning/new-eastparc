<?php

/* Template Name: VHP RSP */

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$response = $_POST;

if (!empty($response)) {
    $vars = json_encode($response);
    $response_file = 'rsp_' . time() . '.json';
    
    // if (file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/rsp/' . $response_file, $vars)) {
    //     // echo 'Done!';
    // }

    $xml_data  = $_POST['xml'];
    $xml_data  = stripslashes($xml_data);
    $extract   = simplexml_load_string($xml_data);
    $trans_id = $extract->NotifDetails->HotelNotifReport->HotelReservations->HotelReservation->ResGlobalInfo->HotelReservationIDs->HotelReservationID['ResID_Value'];


	if ( $trans_id != ""){
		global $wpdb;
		$wpdb->update( 
			'wp_ss_books_trans', 
			array( 
				'db_update_status' => '1'
			), 
			array( 'trans_no' => $trans_id ), 
			array( 
				'%d'
			), 
			array( '%s' ) 
		);
		$wpdb->update( 
			'wp_ss_books_credit_trans', 
			array( 
				'db_update_status' => '1'
			), 
			array( 'trans_no' => $trans_id ), 
			array( 
				'%d'
			), 
			array( '%s' ) 
		);
		$wpdb->update( 
			'wp_indohotels_trans', 
			array( 
				'trans_db_update_stat' => '1'
			), 
			array( 'trans_no' => $trans_id ), 
			array( 
				'%d'
			), 
			array( '%s' ) 
		);
	}
} 

	// $now_date = date('l jS \of F Y h:i:s A');
	$now_date = date('Y-m-d h:i:s',strtotime("-1 days"));
	$query = "SELECT trans_no FROM `wp_ss_books_trans` WHERE `trans_date` < '".$now_date."' AND `status` = 'unpaid'";
	$trans =	$wpdb->get_results($query,ARRAY_A);

	foreach ($trans as $item) {
		$wpdb->delete( 'wp_ss_books_trans_detail', array( 'trans_no' => $item['trans_no'] ), array( '%s' ) );
		$wpdb->delete( 'wp_ss_books_trans', array( 'trans_no' => $item['trans_no'] ), array( '%s' ) );		
	}

	$query = "SELECT trans_no FROM `wp_ss_books_credit_trans` WHERE `trans_date` < '".$now_date."' AND `transactionStatus` IS NULL";
	$trans =	$wpdb->get_results($query,ARRAY_A);

	foreach ($trans as $item) {
		$wpdb->delete( 'wp_ss_books_trans_detail', array( 'trans_no' => $item['trans_no'] ), array( '%s' ) );
		$wpdb->delete( 'wp_ss_books_credit_trans', array( 'trans_no' => $item['trans_no'] ), array( '%s' ) );		
	}





