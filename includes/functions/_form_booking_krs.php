<?php 
function form_booking() { ?>
<div class="mid-booking">
<h2>INSTANT BOOKING</h2>
	<form action="//demo.jogjahotels.id/referrer/booking/" method="GET" role="form" onSubmit="return validateBook()">

		<div class="col-xs-4 field">
			<label for="start_date">Check-in</label>
			<span readonly='true' id="from_date_day"><?php echo gmdate('l',time()+25200); ?></span>
			<input readonly='true' id="from_date" type="text" class="from_date from_date input-sm form-control" name="start" value="<?php echo gmdate('d-m-Y',time()+25200); ?>" />
			<input readonly='true' id="mobile_date" style="display:none" type="text"/>
		</div>
		<div class="col-xs-4 field">
			<label for="">Total Night(s)</label>
			<span class="bookcheck"></span>
			<?php
			$start = isset($_GET['start'])? $_GET['start'] : date('d-m-Y', time());
			$end = isset($_GET['end'])? $_GET['end'] : date('d-m-Y', time() + (60 * 60 * 24));
			$night = (strtotime($end) - strtotime($start)) / (60 * 60 * 24);
			?>
			<div class="box-number night">
				<input type="button" value="" class="qtyminus" field="night"/>
				<input type="text" id="t_night" readonly="true" name="night" class="t_night qty" value="<?php echo $night;?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
				<input type="button" value="" class="qtyplus" field="night"/>
			</div>
		</div>
		<div class="col-xs-4 field">
			<label for="">Check-out</label>
			<span readonly='true' id="to_date_day"><?php echo gmdate('l',time()+25200+(60*60*24)); ?></span>
			<input readonly='true' type="text" id="to_date" class="to_date to_date_mobile input-sm form-control" name="end" value="<?php echo gmdate('d-m-Y',time()+25200+(60*60*24));?>" />
		</div>
		<!-- <input id="property_id" type="hidden" value="cd7ee509-c0bb-4b96-b4ca-3cce08000001"> -->
		<input type="hidden" name="refferer" value="eastparchotel.com">

		<div class="clearfix"></div>
		<div class="form-field-row widget">
			<div class="form-group book-option">
				<label>Number of Room(s)</label>

				<div class="box-number one">
					<input type="button" value="" class="qtyminus" field="rooms"/>
					<input type="text" readonly="true" name="rooms" value="1" class="qty" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
					<input type="button" value="" class="qtyplus" field="rooms"/>
				</div>
			</div>
			<div class="form-group book-option">
				<label>Adult(s)/room</label>
				<div class="box-number two">
					<input type="button" value="" class="qtyminus" field="adults"/>
					<input type="text" readonly="true" name="adults" value="2" class="qty" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
					<input type="button" value="" class="qtyplus" field="adults"/>
				</div>
			</div>
			<div class="form-group book-option">
				<label>Child(ren)/room</label>
				<div class="box-number three">
					<input type="button" value="" class="qtyminus" field="children"/>
					<input type="text" readonly="true" name="children" value="0" class="qty" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
					<input type="button" value="" class="qtyplus" field="children"/>
				</div>
			</div>
		</div> 

		<div class="col-xs-24 m_b_10 p_0">
			<button class="btn btn-primary btn-block" type="submit"> Check Availability</button>
			<?php /*--<a href="http://eastparchotel.com/v1/page/room_suite.html" class="btn btn-primary btn-block"> Check Availability</a>*/?>
		</div>
	</form>
	<div class="powered-by">
		<img src="https://media.jogjahotels.id/website/powered-by-jogjahotels.svg" width="150px">
	</div>
	<?php }