<?php
// Do not load directly...
if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }
if ( !function_exists('header_cover') ) :
	function krs_header_cover() {
		if ( has_post_thumbnail()) : // Check if thumbnail exists 
		$coverimage = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), "cover" );
		$background_header = 'style="background: url(' .$coverimage[0] .') no-repeat;background-size: cover;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;"';

		echo $background_header;
		endif; 
	}
	endif;

	/* Coloring*/
	if ( !function_exists('krs_main_color') ) :
		function krs_main_color() {
			if ( function_exists( 'ot_get_option' ) ) :
				$getMainColor = ot_get_option( 'krs_main_colorpicker');
				$getSeColor = ot_get_option('krs_secound_colorpicker');
				$getTerColor = ot_get_option('krs_tertiary_colorpicker');
				$getMainFonts = ot_get_option('krs_main_fonts');
			if ( ! empty( $getMainColor ) ) : 
				$mainColor = $getMainColor;
				$seColor = $getSeColor;
				$terColor = $getTerColor;
				$fontsColor = $getMainFonts;
				require_once('_custom_css_krs.php');
			endif;
			endif;
		}
		endif;

