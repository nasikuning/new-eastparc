<?php


function wporg_add_custom_box()
{
    $screens = ['rooms'];
    foreach ($screens as $screen) {
        add_meta_box(
            'booking_rooms_box_id',                 // Unique ID
            'Booking Room Box',      // Box title
            'booking_rooms_box_html',  // Content callback, must be of type callable
            $screen                            // Post type
        );
    }
}
add_action('add_meta_boxes', 'wporg_add_custom_box');

function booking_rooms_box_html($post)
{
    global $wpdb;
    $room_book_switch = get_post_meta($post->ID, '_krs_room_book_switch_meta_key', true);
    $direct_url = get_post_meta($post->ID, '_krs_button_direct_url_meta_key', true);
    $direct_text = get_post_meta($post->ID, '_krs_button_direct_text_meta_key', true);
    $base_price = get_post_meta($post->ID, '_krs_room_base_price_meta_key', true);
    $today = date('Y-m-d');

    $room_price_date_q = "SELECT * FROM wp_ss_room_avail WHERE room = '{$post->ID}' AND start >= '{$today}' ORDER BY start ASC";
    $room_price_date = $wpdb->get_results($room_price_date_q, ARRAY_A);

?>
    <!-- <label for="wporg_field">Description for this field</label>
<select name="wporg_field" id="wporg_field" class="postbox">
    <option value="">Select something...</option>
    <option value="something" <?php selected($direct_url, 'something'); ?>>Something</option>
    <option value="else" <?php selected($direct_url, 'else'); ?>>Else</option>
</select> -->
    <div class="rwmb-field rwmb-switch-wrapper">
        <div class="rwmb-label">
            <label for="krs_room_book_switch">Display Pric</label>

        </div>
        <div class="rwmb-input"><label class="rwmb-switch-label rwmb-switch-label--rounded">
                <input type="hidden" id="krs_room_book_switch" name="krs_room_book_switch" value="0">
                <input value="1" type="checkbox" id="krs_room_book_switch" class="rwmb-switch" name="krs_room_book_switch" <?php checked($room_book_switch); ?>>
                <div class="rwmb-switch-status">
                    <span class="rwmb-switch-slider"></span>
                    <span class="rwmb-switch-on">Enable Display Price</span>
                    <!-- <span class="rwmb-switch-off">Direct Booking</span> -->
                </div>
            </label>
        </div>
    </div>
    <div class="rwmb-field rwmb-text-wrapper">
        <div class="rwmb-label">
            <label for="krs_button_direct_url">Link Direct Booking</label>

        </div>
        <div class="rwmb-input"><input value="<?php echo $direct_url; ?>" type="text" id="krs_button_direct_url" class="rwmb-text" name="krs_button_direct_url"></div>
    </div>
    <div class="rwmb-field rwmb-text-wrapper">
        <div class="rwmb-label">
            <label for="krs_button_direct_text">Text Link Direct Booking</label>

        </div>
        <div class="rwmb-input"><input value="<?php echo $direct_text; ?>" type="text" id="krs_button_direct_text" class="rwmb-text" name="krs_button_direct_text"></div>
    </div>
    <div class="rwmb-field rwmb-number-wrapper">
        <div class="rwmb-label">
            <label for="krs_room_base_price">Base Price</label>

        </div>
        <div class="rwmb-input"><input step="1" min="1000" value="<?php echo $base_price; ?>" type="number" id="krs_room_base_price" class="rwmb-number valid" name="krs_room_base_price" aria-invalid="false"></div>
    </div>
    <div class="rwmb-field rwmb-group-wrapper">
        <div class="rwmb-label">
            <label for="krs_room_reg_price">Regular Price</label>
        </div>
        <div class="rwmb-input ui-sortable">

            <?php

            if (isset($room_price_date) && ($room_price_date) && !empty($room_price_date)) :
                foreach ($room_price_date as $key => $price_date) : ?>
                    <div class="rwmb-clone rwmb-group-clone rwmb-sort-clone">
                        <a href="javascript:;" class="rwmb-clone-icon ui-sortable-handle"></a>
                        <div class="rwmb-field rwmb-date-wrapper">
                            <div class="rwmb-label">
                                <label for="date">Date</label>
                            </div>
                            <div class="rwmb-input">
                                <input data-options='{"timeFormat":"HH:mm","separator":" ","dateFormat":"yy-mm-dd","showButtonPanel":true,"changeYear":true,"yearRange":"-100:+100","changeMonth":true,"oneLine":true,"controlType":"select","addSliderAccess":true,"sliderAccessArgs":{"touchonly":true}}' autocomplete="off" value="<?php echo $price_date['start']; ?>" type="text" id="date<?php echo '_'.$key; ?>" class="rwmb-date" name="krs_room_reg_price[<?php echo $key; ?>][date]">
                            </div>
                        </div>
                        <div class="rwmb-field rwmb-number-wrapper">
                            <div class="rwmb-label">
                                <label for="price">Price</label>
                            </div>
                            <div class="rwmb-input">
                                <input step="1" min="0" value="<?php echo $price_date['price']; ?>" type="number" id="price<?php echo '_'.$key; ?>" class="rwmb-number" name="krs_room_reg_price[<?php echo $key; ?>][price]">
                            </div>
                        </div>
                        <a href="#" class="rwmb-button remove-clone" style="display: none;">
                            <span class="dashicons dashicons-dismiss"></span>
                        </a>
                    </div>
                <?php endforeach;
            else : ?>
                <div class="rwmb-clone rwmb-group-clone rwmb-sort-clone">
                    <a href="javascript:;" class="rwmb-clone-icon ui-sortable-handle"></a>
                    <div class="rwmb-field rwmb-date-wrapper">
                        <div class="rwmb-label">
                            <label for="date">Date</label>
                        </div>
                        <div class="rwmb-input">
                            <input data-options='{"timeFormat":"HH:mm","separator":" ","dateFormat":"yy-mm-dd","showButtonPanel":true,"changeYear":true,"yearRange":"-100:+100","changeMonth":true,"oneLine":true,"controlType":"select","addSliderAccess":true,"sliderAccessArgs":{"touchonly":true}}' autocomplete="off" value="" type="text" id="date" class="rwmb-date" name="krs_room_reg_price[0][date]">
                        </div>
                    </div>
                    <div class="rwmb-field rwmb-number-wrapper">
                        <div class="rwmb-label">
                            <label for="price">Price</label>
                        </div>
                        <div class="rwmb-input">
                            <input step="1" min="0" value="" type="number" id="price" class="rwmb-number" name="krs_room_reg_price[0][price]">
                        </div>
                    </div>
                    <a href="#" class="rwmb-button remove-clone" style="display: none;">
                        <span class="dashicons dashicons-dismiss"></span>
                    </a>
                </div>
            <?php endif; ?>
            <a href="#" class="rwmb-button button-primary add-clone">+ Add more</a>

        </div>
    </div>

<?php
}

function wporg_save_postdata($post_id)
{
    global $wpdb;

    if (array_key_exists('krs_room_book_switch', $_POST)) {
        update_post_meta(
            $post_id,
            '_krs_room_book_switch_meta_key',
            $_POST['krs_room_book_switch']
        );
    }
    if (array_key_exists('krs_button_direct_url', $_POST)) {
        update_post_meta(
            $post_id,
            '_krs_button_direct_url_meta_key',
            $_POST['krs_button_direct_url']
        );
    }
    if (array_key_exists('krs_button_direct_text', $_POST)) {
        update_post_meta(
            $post_id,
            '_krs_button_direct_text_meta_key',
            $_POST['krs_button_direct_text']
        );
    }
    if (array_key_exists('krs_room_base_price', $_POST)) {
        update_post_meta(
            $post_id,
            '_krs_room_base_price_meta_key',
            $_POST['krs_room_base_price']
        );
    }
    if (array_key_exists('krs_room_reg_price', $_POST)) {
        // update_post_meta(
        //     $post_id,
        //     '_krs_room_reg_price_meta_key',
        //     $_POST['krs_room_reg_price']
        // );
        // echo '<pre>';
        // print_r($_POST['krs_room_reg_price']);
        // die();
        $wpdb->delete($wpdb->prefix . 'ss_room_avail', array('room' => $post_id, 'start'    => date('Y-m-d') ));

        $room = null;
        $count = 10;
        foreach ($_POST['krs_room_reg_price'] as $room_reg_price) {
            $room_q = "SELECT * FROM wp_ss_room_avail WHERE room = '{$post_id}' AND start >= '{$room_reg_price['date']}'";
            $room = $wpdb->get_row($room_q, ARRAY_A);
            if (empty($room)) {
                $wpdb->insert(
                    $wpdb->prefix . 'ss_room_avail',
                    array(
                        'room'    => $post_id,
                        'start'   => $room_reg_price['date'],
                        'end'     => $room_reg_price['date'],
                        'count'   => $count,
                        'price'   => $room_reg_price['price'],
                    )
                );
            } else {
                $wpdb->update(
                    $wpdb->prefix . 'ss_room_avail',
                    array(
                        'room'    => $post_id,
                        'start'   => $room_reg_price['date'],
                        'end'     => $room_reg_price['date'],
                        'count'   => isset($room['count']) ? $room['count'] : $count,
                        'price'   => $room_reg_price['price'],
                    ),
                    array(
                        'room'  => $post_id,
                    )
                );
            }
        }
        // die();

    }
}
add_action('save_post', 'wporg_save_postdata');

function krs_get_room_price($post_id, $start, $end, $room = 1, $mode)
{
    global $wpdb;
    $room_price_date = null;
    $base_price = get_post_meta($post_id, '_krs_room_base_price_meta_key', true);
    $room_price_date_q = "SELECT * FROM wp_ss_room_avail WHERE room = '{$post_id}' AND start >= '{$start}' AND end < '{$end}'";
    $room_price_date = $wpdb->get_results($room_price_date_q, ARRAY_A);
    $total_price = 0;
    $total_night = 0;
    if ($mode == 'search') {
        foreach ($room_price_date as $key => $price_date) {
            $total_price += ($price_date['price'] * $room);
            $total_night++;
        }
        $total_price = Ceil($total_price / $total_night);
    } else {
        foreach ($room_price_date as $key => $price_date) {
            $total_price += ($price_date['price'] * $room);
            $total_night++;
        }
    }

    if (empty($room_price_date) && !empty($base_price)) {
        $total_price = $base_price;
    }

    return $total_price;
}

function krs_books_set_cookie($type = '', $name = '', $value = '', $prefix = '')
{
    setcookie($prefix . "ss_books_[" . $type . "][" . $name . "]", $value, time() + (3600 * 12 * 30 * 365), '/');
    return true;
}

function krs_books_get_cookie($type = '', $name = '', $default_value = '', $prefix = '')
{
    if ($name == '') {
        if (isset($_COOKIE["ss_books_"])) {
            $value = $_COOKIE["ss_books_"];
        } else {
            $value = $default_value;
        }
    } else {
        if (isset($_COOKIE[$prefix . "ss_books_[" . $type . "][" . $name . "]"])) {
            $value = $_COOKIE[$prefix . "ss_books_" . "[" . $type . "][" . $name . "]"];
        } else {
            $value = $default_value;
        }
    }
    return $value;
}

function krs_books_unset_cookie($type = '', $name = '', $value = '', $prefix = '')
{
    unset($_COOKIE['ss_books_'][$type]);
    setcookie("ss_books_[" . $type . "][room_name]", '', time() - 3600, '/');
    setcookie("ss_books_[" . $type . "][book_room_type]", '', time() - 3600, '/');
    setcookie("ss_books_[" . $type . "][book_name]", '', time() - 3600, '/');
    setcookie("ss_books_[" . $type . "][book_title]", '', time() - 3600, '/');
    setcookie("ss_books_[" . $type . "][book_first_name]", '', time() - 3600, '/');
    setcookie("ss_books_[" . $type . "][book_last_name]", '', time() - 3600, '/');
    setcookie("ss_books_[" . $type . "][book_phone]", '', time() - 3600, '/');
    setcookie("ss_books_[" . $type . "][book_email]", '', time() - 3600, '/');
    setcookie("ss_books_[" . $type . "][book_checkin]", '', time() - 3600, '/');
    setcookie("ss_books_[" . $type . "][book_checkout]", '', time() - 3600, '/');
    setcookie("ss_books_[" . $type . "][book_num_room]", '', time() - 3600, '/');
    setcookie("ss_books_[" . $type . "][promotion_code]", '', time() - 3600, '/');
    setcookie("ss_books_[" . $type . "][total_night]", '', time() - 3600, '/');
    setcookie("ss_books_[" . $type . "][total_price]", '', time() - 3600, '/');
    setcookie("ss_books_[" . $type . "][book_this_room]", '', time() - 3600, '/');
    setcookie("ss_books_[" . $type . "][book_confirm_email]", '', time() - 3600, '/');
    setcookie("ss_books_[" . $type . "][book_date]", '', time() - 3600, '/');
    setcookie("ss_books_[" . $type . "][voucher_code]", '', time() - 3600, '/');
    return $prefix . "ss_books_[" . $type . "]";
}
