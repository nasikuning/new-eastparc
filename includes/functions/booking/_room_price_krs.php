<?php
add_action('admin_menu', 'custom_menu');
add_action('admin_init', 'update_pricing');

function custom_menu()
{

    add_menu_page(
        'Hotel Booking',
        'Hotel Booking',
        'manage_options',
        'hotel-booking',
        'hotel-booking-callback',
        'dashicons-media-spreadsheet',
        '7'
    );
    add_submenu_page('hotel-booking', 'Hotel Booking', 'Booking', 'manage_options', 'hotel-booking', 'hotel_booking_callback');
    add_submenu_page('hotel-booking', 'Room Price', 'Room Price', 'manage_options', 'hotel-booking-price', 'room_price_callback');
}
/**
 * Display callback for the submenu page.
 */
function hotel_booking_callback()
{
?>
    <div class="wrap">
        <h1><?php _e('Hotel Booking', karisma_text_domain); ?></h1>
        <p><?php _e('All booking record', karisma_text_domain); ?></p>
    </div>
<?php
}
/**
 * Display callback for the submenu page.
 */
function room_price_callback()
{
    // return 'ok';
    $room_id = isset($_REQUEST['krs-room']) ? intval($_REQUEST['krs-room']) : null;
    if($room_id) {

        $week_names = krs_date_names();
        $plans = krs_room_get_pricing_plans( $room_id );
        $regular_plan = null;
    


        $count_plants = count( $plans );
        $date_order   = krs_start_of_week_order();
        foreach ( $plans as $k => $plan ) {
            if ( ! $plan->start && ! $plan->end ) {
                $regular_plan = $plan;
                // unset( $plans[ $k ] );
            }
        }
    }

?>
    <div class="wrap" id="krs_hotel_booking_pricing">
        <h1><?php _e('Room Price', karisma_text_domain); ?></h1>
        <form action="" method="POST" name="pricing-table-form">
            <p>
                Select name of room
                <select name="rooms" id="rooms">
                    <option value="">---Select Room---</option>
                    <?php
                    $args = array('post_type' => 'rooms');
                    query_posts($args);
                    if (have_posts()) : while (have_posts()) : the_post(); ?>
                            <option value="<?php the_ID(); ?>" <?php echo $room_id ==  get_the_ID() ? 'selected' : ''; ?>><?php the_title(); ?></option>
                        <?php endwhile; ?>
                    <?php endif; ?>
                    <?php wp_reset_query(); ?>
                </select>
            </p>
            <?php if ($room_id) : ?>

                <div class="krs-pricing-table regular-price clearfix">
                <h3 class="krs-pricing-table-title">
                    <span><?php _e( 'Regular price', karisma_text_domain ); ?></span>
                    <input type="text" class="datepicker"
                           name="date-start[<?php echo sprintf( '%s', $regular_plan ? $regular_plan->ID : '__INDEX__' ); ?>]"
                           size="10" readonly="readonly"/>
                    <input type="hidden"
                           name="date-start-timestamp[<?php echo sprintf( '%s', $regular_plan ? $regular_plan->ID : '__INDEX__' ); ?>]"/>

                    <input type="text" class="datepicker"
                           name="date-end[<?php echo sprintf( '%s', $regular_plan ? $regular_plan->ID : '__INDEX__' ); ?>]"
                           size="10" readonly="readonly"/>
                    <input type="hidden"
                           name="date-end-timestamp[<?php echo sprintf( '%s', $regular_plan ? $regular_plan->ID : '__INDEX__' ); ?>]"/>
                </h3>
                <div class="krs-pricing-controls">
                    <a href="" onclick="return false;" class="dashicons dashicons-edit" data-action="edit"
                       title="<?php _e( 'Edit', karisma_text_domain ); ?>"></a>
                    <a href="" onclick="return false;" class="dashicons dashicons-admin-page" data-action="clone"
                       title="<?php _e( 'Clone', karisma_text_domain ); ?>"></a>
                    <a href="" onclick="return false;" class="dashicons dashicons-trash" data-action="remove"
                       title="<?php _e( 'Remove', karisma_text_domain ); ?>"></a>
                </div>
                    <div class="krs-pricing-list">
                        <table>
                            <thead>
                            <tr>
                                <?php foreach ( $date_order as $i ) { ?>
                                    <th><?php echo esc_html( $week_names[ $i ] ); ?></th>
                                <?php } ?>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <?php
                                $prices  = isset( $regular_plan->prices ) ? $regular_plan->prices : array();
                                $plan_id = isset( $regular_plan->ID ) ? $regular_plan->ID : 0;
                                ?>
                                <?php foreach ( $date_order as $i ) { ?>
                                    <td>
                                        <?php $price = ! empty( $prices[ $i ] ) ? $prices[ $i ] : ''; ?>
                                        <input class="krs-pricing-price" type="number" min="0" step="any"
                                            name="price[<?php echo sprintf( '%s', $plan_id ? $plan_id : '__INDEX__' ); ?>][<?php echo esc_attr( $i ); ?>]"
                                            value="<?php echo esc_attr( $price ); ?>" size="10" readonly="readonly"/>
                                    </td>
                                <?php } ?>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="clearfix"></div>
                <h3 class="clearfix">
                    <?php _e( 'Other plan', karisma_text_domain ); ?>
                    <span class="count"><?php printf( _n( '(%d plan)', '(%d plans)', $count_plants, karisma_text_domain ), $count_plants ); ?></span>
                </h3>
                
                <div id="krs-pricing-plan-list">
				<?php if ( $plans ): ?>
					<?php foreach ( $plans as $plan ) : ?>
						<?php
						$start = strtotime( $plan->start );
						$end   = strtotime( $plan->end );
						?>
                        <div class="krs-pricing-table">
                            <h3 class="krs-pricing-table-title">
                                <span><?php _e( 'Date Range', karisma_text_domain ); ?></span>
                                <input type="text" class="datepicker"
                                       name="date-start[<?php echo esc_attr( $plan->ID ); ?>]" size="10"
                                       value="<?php printf( '%s', date_i18n( hb_get_date_format(), $start ) ); ?>"
                                       readonly="readonly"/>
                                <input type="hidden" name="date-start-timestamp[<?php echo esc_attr( $plan->ID ); ?>]"
                                       value="<?php echo esc_attr( $start ); ?>"/>

                                <input type="text" class="datepicker"
                                       name="date-end[<?php echo esc_attr( $plan->ID ); ?>]" size="10"
                                       value="<?php printf( '%s', date_i18n( krs_get_date_format(), $end ) ); ?>"
                                       readonly="readonly"/>
                                <input type="hidden" name="date-end-timestamp[<?php echo esc_attr( $plan->ID ); ?>]"
                                       value="<?php echo esc_attr( $end ); ?>"/>
                            </h3>
                            <div class="krs-pricing-controls">
                                <a href="" onclick="return false;" class="dashicons dashicons-edit" data-action="edit"
                                   title="<?php _e( 'Edit', karisma_text_domain ); ?>"></a>
                                <a href="" onclick="return false;" class="dashicons dashicons-admin-page" data-action="clone" title="<?php //_e( 'Clone', karisma_text_domain ); ?>"></a>
                                <a href="" onclick="return false;" class="dashicons dashicons-trash" data-action="remove"
                                   title="<?php _e( 'Remove', karisma_text_domain ); ?>"></a>
                            </div>

                            <div class="krs-pricing-list">
                                <table>
                                    <thead>
                                    <tr>
										<?php foreach ( $date_order as $i ) { ?>
                                            <th><?php echo esc_html( $week_names[ $i ] ); ?></th>
										<?php } ?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
										<?php $prices = $plan->prices; ?>
										<?php foreach ( $date_order as $i ) { ?>
                                            <td>
												<?php $price = ! empty( $prices[ $i ] ) ? $prices[ $i ] : ''; ?>
                                                <input class="krs-pricing-price" type="number" min="0" step="any"
                                                       name="price[<?php echo esc_attr( $plan->ID ); ?>][<?php echo esc_attr( $i ); ?>]"
                                                       value="<?php echo esc_attr( $price ); ?>" size="10"
                                                       readonly="readonly"/>
                                            </td>
										<?php } ?>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
					<?php endforeach; ?>

				<?php else: ?>
                    <p id="krs-no-plan-message"> <?php _e( 'No addition plans', karisma_text_domain ); ?></p>
				<?php endif; ?>

            </div>

                <p>
                    <input type="hidden" name="room_id" value="<?php echo esc_attr($room_id) ?>" />
                    <button class="button button-primary"><?php _e('Update', karisma_text_domain); ?></button>
                </p>
                <?php wp_nonce_field('krs-update-pricing-plan', 'krs-update-pricing-plan-field'); ?>
            <?php endif; ?>

        </form>
    </div>
    <div id="tmpl-krs-pricing-table" style="display:none;" class="template-custom">
        <div class="krs-pricing-table">
        <h3 class="krs-pricing-table-title">
            <span><?php _e( 'Date Range',karisma_text_domain ); ?></span>
            <input type="text" class="datepicker" name="date-start[__INDEX__]" size="10" readonly="readonly"/>
            <input type="hidden" name="date-start-timestamp[__INDEX__]"/>
            <input type="text" class="datepicker" name="date-end[__INDEX__]" size="10" readonly="readonly"/>
            <input type="hidden" name="date-end-timestamp[__INDEX__]"/>
        </h3>
        <div class="krs-pricing-controls">
            <a href="" onclick="return false;" class="dashicons dashicons-edit" data-action="edit"
               title="<?php _e( 'Clone',karisma_text_domain ); ?>"></a>

            <a href="" onclick="return false;" class="dashicons dashicons-trash" data-action="remove"
               title="<?php _e( 'Remove',karisma_text_domain ); ?>"></a>
        </div>

        <div class="krs-pricing-list">
            <table>
                <thead>
                <tr>
					<?php foreach ( $date_order as $i ) { ?>
                        <th><?php echo esc_html( $week_names[ $i ] ); ?></th>
					<?php } ?>
                </tr>
                </thead>
                <tbody>
                <tr>
					<?php foreach ( $date_order as $i ) { ?>
                        <td>
                            <input class="krs-pricing-price" type="number" min="0" step="any"
                                   name="price[__INDEX__][<?php echo esc_attr( $i ); ?>]" value="" size="10"
                                   readonly="readonly"/>
                        </td>
					<?php } ?>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    </div>
<?php
}

if ( ! function_exists( 'krs_date_names' ) ) {
	function krs_date_names() {
		$date_names = array(
			__( 'Sun', karisma_text_domain ),
			__( 'Mon', karisma_text_domain ),
			__( 'Tue', karisma_text_domain ),
			__( 'Wed', karisma_text_domain ),
			__( 'Thu', karisma_text_domain ),
			__( 'Fri', karisma_text_domain ),
			__( 'Sat', karisma_text_domain )
		);

		return apply_filters( 'krs_date_names', $date_names );
	}
}

if ( ! function_exists( 'krs_start_of_week_order' ) ) {
	function krs_start_of_week_order() {
		$start = get_option( 'start_of_week' );

		$order = array();

		for ( $i = (int) $start; $i < 7; $i ++ ) {
			$order[] = $i;
		}

		for ( $j = 0; $j < $start; $j ++ ) {
			$order[] = $j;
		}

		return $order;
	}
}

if ( ! function_exists( 'krs_get_date_format' ) ) {

	function krs_get_date_format() {
		$dateFormat = get_option( 'date_format' );

		$dateCustomFormat = get_option( 'date_format_custom' );
		if ( ! $dateFormat && $dateCustomFormat ) {
			$dateFormat = $dateCustomFormat;
		}

		return $dateFormat;
	}

}

function krs_room_set_pricing_plan($args = array())
{

    $args = wp_parse_args($args, array(
        'start_time' => null,
        'end_time'   => null,
        'pricing'    => null,
        'room_id'    => null,
        'plan_id'    => null
    ));

    global $wpdb;
    $hotel_booking_plans                = 'hotel_booking_plans';
    $wpdb->hotel_booking_plans          = $wpdb->prefix . $hotel_booking_plans;

    $wpdb->insert(
        $wpdb->hotel_booking_plans,
        array(
            'start_time' => $args['start_time'] ? date('Y-m-d H:i:s', $args['start_time']) : null,
            'end_time'   => $args['end_time'] ? date('Y-m-d H:i:s', $args['end_time']) : null,
            'pricing'    => maybe_serialize($args['pricing']),
            'room_id'    => $args['room_id']
        ),
        array('%s', '%s', '%s', '%d')
    );

    $plan_id = absint($wpdb->insert_id);

    do_action('hotel_booking_created_pricing_plan', $plan_id, $args);

    return $plan_id;
}


// get pricing plans
function krs_room_get_pricing_plans($room_id = null)
{
    if (!$room_id) {
        throw new Exception(__('Room id is not exists.', karisma_text_domain), 503);
    }

    global $wpdb;
    $hotel_booking_plans                = 'hotel_booking_plans';
    $wpdb->hotel_booking_plans          = $wpdb->prefix . $hotel_booking_plans;

    if (!isset($wpdb->hotel_booking_plans)) {
        // hotel_booking_set_table_name();
    }

    $sql = $wpdb->prepare("
				SELECT plans.* FROM $wpdb->hotel_booking_plans AS plans
					INNER JOIN $wpdb->posts AS room ON room.ID = plans.room_id
				WHERE
					plans.room_id = %d
					AND room.post_type = %s
					AND room.post_status = %s
				ORDER BY plan_id DESC
			", $room_id, 'rooms', 'publish');

    $cols  = $wpdb->get_results($sql);
    $plans = array();

    foreach ($cols as $k => $plan) {
        $pl = new stdClass;

        $pl->ID    = $plan->plan_id;
        $pl->start = $plan->start_time;
        $pl->end   = $plan->end_time;
        $pl->prices    = maybe_unserialize($plan->pricing);

        $plans[$plan->plan_id] = $pl;
    }


    return apply_filters('krs_room_get_pricing_plans', $plans, $room_id);
}

function krs_room_remove_pricing($plan_id = null)
{

    if (!$plan_id) {
        return;
    }

    global $wpdb;
    $hotel_booking_plans                = 'hotel_booking_plans';
    $wpdb->hotel_booking_plans          = $wpdb->prefix . $hotel_booking_plans;

    $wpdb->delete($wpdb->hotel_booking_plans, array('plan_id' => $plan_id), array('%d'));

    do_action('krs_room_remove_pricing', $plan_id);

    return $plan_id;
}
function sanitize_params_submitted($value, $type_content = 'text')
{
    $value = wp_unslash($value);

    if (is_string($value)) {
        switch ($type_content) {
            case 'html':
                $value = wp_kses_post($value);
                break;
            case 'textarea':
                $value = sanitize_textarea_field($value);
                break;
            default:
                $value = sanitize_text_field($value);
        }
    } elseif (is_array($value)) {
        foreach ($value as $k => $v) {
            $value[$k] = sanitize_params_submitted($v, $type_content);
        }
    }

    return $value;
}

function update_pricing()
{
    if (!isset($_POST['krs-update-pricing-plan-field']) || !wp_verify_nonce(
        sanitize_text_field($_POST['krs-update-pricing-plan-field']),
        'krs-update-pricing-plan'
    )) {
        return;
    }

    if (empty($_POST['price'])) {
        return;
    }

    if (!isset($_POST['room_id'])) {
        return;
    }

    $room_id = absint($_POST['room_id']);
    $plans   = krs_room_get_pricing_plans($room_id);

    $ignore = array();

    $prices = (array) $_POST['price'];

    foreach (array_keys($prices) as $key) {
        $key         = sanitize_text_field($key);
        $start       = isset($_POST['date-start-timestamp'][$key]) ? sanitize_text_field($_POST['date-start-timestamp'][$key]) : '';
        $end         = isset($_POST['date-end-timestamp'][$key]) ? sanitize_text_field($_POST['date-end-timestamp'][$key]) : '';
        $prices_post = (array) $_POST['price'][$key];
        $prices      = sanitize_params_submitted($prices_post);

        $plan_id  = krs_room_set_pricing_plan(array(
            'start_time' => $start,
            'end_time'   => $end,
            'pricing'    => $prices,
            'room_id'    => $room_id,
            'plan_id'    => $key
        ));
        $ignore[] = $plan_id;
    }



    foreach ($plans as $id => $plan) {
        if (!in_array($id, $ignore)) {
            krs_room_remove_pricing($id);
        }
    }

}