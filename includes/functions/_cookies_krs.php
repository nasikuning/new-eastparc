<?php
// Do not load directly...
if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }

function set_new_cookie() {
    if(!isset($_COOKIE["popup"])) : 
    	setcookie("popup", "huahaua", time()+3600, '/'); // 1 Jam
  	endif; 
}
add_action( 'init', 'set_new_cookie');