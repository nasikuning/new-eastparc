<?php
// check Polylang exist
if ( function_exists('pll_register_string') ) {
    //Home Pages
    pll_register_string('Room Details', 'Room Details', karisma_text_domain);
    pll_register_string('Awards & Accolades', 'Awards & Accolades', karisma_text_domain);
    pll_register_string('with Balcony', 'with Balcony', karisma_text_domain);
    pll_register_string('without Balcony', 'without Balcony', karisma_text_domain);
    pll_register_string('rooms', 'Rooms', karisma_text_domain);

    //Template Beauty & Retails
    pll_register_string('Read More', 'Read More', karisma_text_domain);
    pll_register_string('See More', 'See More', karisma_text_domain);
    
    //Single Room
    pll_register_string('In-Room Amenities', 'In-Room Amenities', karisma_text_domain);
    pll_register_string('For Your Convenience', 'For Your Convenience', karisma_text_domain);
    pll_register_string('For Your Comfort', 'For Your Comfort', karisma_text_domain);
    pll_register_string('For Your Indulgence', 'For Your Indulgence', karisma_text_domain);
    pll_register_string('Book Now', 'Book Now', karisma_text_domain);

    pll_register_string('Bed Size', 'Bed Size', karisma_text_domain);
    pll_register_string('Occupancy', 'Occupancy', karisma_text_domain);
    pll_register_string('Room Size', 'Room Size', karisma_text_domain);
    pll_register_string('View', 'View', karisma_text_domain);

    //Single Restaurant and Cafe
    pll_register_string('Cuisine Type', 'Cuisine Type', karisma_text_domain);
    pll_register_string('Capacity', 'Capacity', karisma_text_domain);
    pll_register_string('Location', 'Location', karisma_text_domain);
    pll_register_string('Opening Hours', 'Opening Hours', karisma_text_domain);
    pll_register_string('Telephone', 'Telephone', karisma_text_domain);
    pll_register_string('Our Food', 'Our Food', karisma_text_domain);

    //Single Meetings and Events
    pll_register_string('proposal.', 'If you need a proposal, please click the button below.', karisma_text_domain);
    pll_register_string('Request For Proposal.', 'Request For Proposal', karisma_text_domain);

    //Single Beauty and Retail
    pll_register_string('View Menu', 'View Menu', karisma_text_domain);
    pll_register_string('Website', 'Website', karisma_text_domain);

    //Not Found
    pll_register_string('Sorry, nothing to display.', 'Sorry, nothing to display.', karisma_text_domain);
    
  }