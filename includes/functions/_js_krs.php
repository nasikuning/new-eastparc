<?php

// Do not load directly...
if (!defined('ABSPATH')) {
    die('Direct access forbidden.');
}


// Load karisma scripts (header.php)
function krs_loadjs()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {


        wp_register_script('bootstrap', krs_style . 'vendor/bootstrap/js/bootstrap.min.js', array(), false, true);
        wp_register_script('animated-js', krs_style . 'js/animated.js', array(), false, true);
        wp_register_script('viewportchecker', krs_style . 'js/lib/viewportchecker.js', array('jquery'), false, true);
        wp_register_script('global-js', krs_url . 'asset/js/global.min.js',  array(), false, true);
        wp_register_script('main-js', krs_style . 'js/main.js',  array(), false, true);
        wp_register_script('owl-js', krs_style . 'vendor/owl/owl.carousel.min.js', array(), false, true);
        wp_register_script('slick-js', krs_style . 'vendor/slick/slick.min.js', array(), false, true);
        wp_register_script('magnific-js', krs_style . 'vendor/magnific/jquery.magnific-popup.min.js', array(), false, true);
        wp_register_script('moment-js', krs_style . 'vendor/caleran/js/moment.min.js', array(), false, true);
        wp_register_script('caleran-js', krs_style . 'vendor/caleran/js/caleran.min.js', array(), false, true);
        wp_register_script('calender-js', krs_style . 'js/calender-eastparc.js',  array(), false, true);
        wp_register_script('maps', '//maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyDmvXT3dnOfHS52c5vcBSf9kyOC_zQqVuA',  array(), false, true);
        wp_register_script('intlTelInput', krs_style . 'js/intlTelInput.min.js',  array(), false, true);


        wp_enqueue_script('bootstrap');
        // wp_enqueue_script('viewportchecker'); 
        // wp_enqueue_script('animated-js');
        wp_enqueue_script('owl-js');
        wp_enqueue_script('slick-js');
        wp_enqueue_script('magnific-js');

        wp_enqueue_script('moment-js');
        wp_enqueue_script('caleran-js');
        wp_enqueue_script('calender-js');

        wp_enqueue_script('intlTelInput');
        wp_enqueue_script('maps');

        wp_enqueue_script('global-js');
        wp_enqueue_script('main-js');

        wp_localize_script('caleran-js', 'magicalData', array(
            'ajaxurl' => admin_url('admin-ajax.php'),
            'nonce' => wp_create_nonce('wp_rest'),
            'property_id' => 'cd7ee509-c0bb-4b96-b4ca-3cce08000001',

        ));
    }

    if (is_admin()) {
        // wp_register_script('booking-js', krs_url . 'asset/js/booking.js',  array(), false, true);
        // wp_enqueue_script('booking-js');
        // wp_register_script('rwmb-clone-js', 'https://www.eastparchotel.com/wp-content/plugins/meta-box/js/clone.js',  array(), false, false);
        // wp_enqueue_script('rwmb-clone-js');
        wp_enqueue_script( 'jquery-ui-datepicker' );

        wp_register_script('custom-room', krs_style . 'js/custom_rooms.js',  array(), false, true);
        wp_enqueue_script('custom-room');
        
    }
}
