<?php
// Do not load directly...
if (!defined('ABSPATH')) {
    die('Direct access forbidden.');
}

/* META BOX ROOM CUSTOM FIELD */

add_filter('rwmb_meta_boxes', 'room_info_meta_boxes');
add_filter('rwmb_meta_boxes', 'restaurant_cafe_meta_boxes');
add_filter('rwmb_meta_boxes', 'page_meta_boxes');
add_filter('rwmb_meta_boxes', 'contact_meta_box');
add_filter('rwmb_meta_boxes', 'gallery_photos_meta_box');
add_filter('rwmb_meta_boxes', 'beauty_retail_meta_boxes');
add_filter('rwmb_meta_boxes', 'meetings_events_meta_boxes');
add_filter('rwmb_meta_boxes', 'hotel_info_meta_boxes');
add_filter('rwmb_meta_boxes', 'deals_meta_boxes');

//Rooms
function room_info_meta_boxes($meta_boxes)
{
    $prefix = 'indohotels_';
    // $meta_boxes[] = array(
    //     'title' => __('Room Price / Booking Button', karisma_text_domain),
    //     'post_types' => 'rooms',
    //     'fields' => array(
    //         array(
    //             'id'      => "{$prefix}room_book_switch",
    //             'name'    => __('Display Price / Direct Booking', karisma_text_domain),
    //             'type'      => 'switch',
    //             'style'     => 'rounded',
    //             'on_label'  => 'Display Price',
    //             'off_label' => 'Direct Booking',
    //         ),
    //         array(
    //             'id'      => "{$prefix}button_direct_url",
    //             'name'    => __('Link Direct Booking', karisma_text_domain),
    //             'type' => 'text',
    //         ),
    //         array(
    //             'id'      => "{$prefix}button_direct_text",
    //             'name'    => __('Text Link Direct Booking', karisma_text_domain),
    //             'type' => 'text',
    //         ),
    //         array(
    //             'id'      => "{$prefix}room_base_price",
    //             'name'    => __('Base Price', karisma_text_domain),
    //             'type' => 'number',
    //         ),
    //         array(
    //             'id'      => "{$prefix}room_reg_price",
    //             'name'    => __('Regular Price', karisma_text_domain),
    //             'type'    => 'group',
    //             'clone' => true,
    //             'sort_clone' => true,
    //             'fields' => array(
    //                 array(
    //                     'name' => 'Date',
    //                     'id' => 'date',
    //                     'type' => 'date',
    //                 ),
    //                 array(
    //                     'name' => 'Price',
    //                     'id' => 'price',
    //                     'type' => 'number',
    //                 ),
      
    //             ),
    //         ),
    //     ),
    // );
    $meta_boxes[] = array(
        'id'         => 'gallery_room',
        'context'    => 'normal',
        'priority'   => 'high',
        'title'      => __('Room photo gallery', karisma_text_domain),
        'post_types' => 'rooms',
        'fields'     => array(
            // IMAGE ADVANCED - RECOMMENDED
            array(
                'name'             => __('Upload Gallery Photos (Recommended)', karisma_text_domain),
                'id'               => "{$prefix}imgadv",
                'type'             => 'image_advanced',
                // Delete image from Media Library when remove it from post meta?
                // Note: it might affect other posts if you use same image for multiple posts
                'force_delete'     => false,
                // Maximum image uploads
                'max_file_uploads' => 6,
                // Display the "Uploaded 1/2 files" status
                'max_status'       => true,
            ),
        ),
    );
    $meta_boxes[] = array(
        'id'         => 'info_room',
        'context'    => 'normal',
        'priority'   => 'high',
        'title'      => __('Information Room', karisma_text_domain),
        'post_types' => 'rooms',
        'fields'     => array(
            array(
                'id'   => 'room_size',
                'name' => __('Room Size', karisma_text_domain),
                'type' => 'number',
            ),
            // SELECT BOX
            array(
                'name'        => __('Have Balcony ?', karisma_text_domain),
                'id'          => "{$prefix}room_balcony",
                'type'        => 'select',
                'options'     => array(
                    'with Balcony' => __('with Balcony', karisma_text_domain),
                    'without Balcony' => __('without Balcony', karisma_text_domain),
                ),
                'multiple'    => false,
                'placeholder' => __('Select an Item', karisma_text_domain),
            ),
            array(
                'id'      => 'room_view',
                'name'    => __('View', karisma_text_domain),
                'type'    => 'text',
            ),
            array(
                'id'      => 'room_occupancy',
                'name'    => __('occupancy', karisma_text_domain),
                'type'    => 'number',
            ),
            array(
                'id'   => 'bed_size',
                'name' => __('Bed Size', karisma_text_domain),
                'type' => 'textarea',
            ),
        ),
    );
    $meta_boxes[] = array(
        'title' => __('Room Features', karisma_text_domain),
        'post_types' => 'rooms',
        'fields' => array(
            array(
                'id'      => 'room_convenience',
                'name'    => __('Your Convenience', karisma_text_domain),
                'type'    => 'text',
                'clone' => true,
                'sort_clone' => true,
            ),
            array(
                'id'      => 'room_indulgence',
                'name'    => __('Your Indulgence', karisma_text_domain),
                'type'    => 'text',
                'clone'   => true,
                'sort_clone' => true,
            ),
            array(
                'id'      => 'room_comfort',
                'name'    => __('Your Comfort', karisma_text_domain),
                'type'    => 'text',
                'clone'   => true,
                'sort_clone' => true,
            ),
        ),
    );

    $meta_boxes[] = array(
        'id'         => 'room_extra_text',
        'context'    => 'normal',
        'priority'   => 'high',
        'title'      => __('Help Fact', karisma_text_domain),
        'post_types' => 'rooms',
        'fields'     => array(
            array(
                'name'             => __('Extra Title', karisma_text_domain),
                'id'               => "{$prefix}extra_title",
                'type'             => 'text',
            ),
            array(
                'name'             => __('Extra Text', karisma_text_domain),
                'id'               => "{$prefix}extra_text",
                'type'             => 'wysiwyg',
            ),
        ),
    );
    return $meta_boxes;
}
//Restaurant & Cafe
function restaurant_cafe_meta_boxes($meta_boxes)
{
    $prefix = 'indohotels_';
    $meta_boxes[] = array(
        'id'         => 'gallery_restaurant_cafe',
        'context'    => 'normal',
        'priority'   => 'high',
        'title'      => __('Restaurant & Cafe photo gallery', karisma_text_domain),
        'post_types' => 'restaurant-cafe',
        'fields'     => array(
            array(
                'name'             => __('Upload Gallery Photos (Recommended)', karisma_text_domain),
                'id'               => "{$prefix}imgRestaurantCafe",
                'type'             => 'image_advanced',
                // Delete image from Media Library when remove it from post meta?
                // Note: it might affect other posts if you use same image for multiple posts
                'force_delete'     => false,
                // Maximum image uploads
                'max_file_uploads' => 6,
                // Display the "Uploaded 1/2 files" status
                'max_status'       => true,
            ),
        ),
    );
    $meta_boxes[] = array(
        'id'         => 'info_restaurant_cafe',
        'context'    => 'normal',
        'priority'   => 'high',
        'title'      => __('Information Restaurant & Cafe', karisma_text_domain),
        'post_types' => 'restaurant-cafe',
        'fields'     => array(
            array(
                'name'             => __('Logo Restaurant or Cafe', karisma_text_domain),
                'id'               => "{$prefix}logoRestaurantCafe",
                'type'             => 'image_advanced',
                // Delete image from Media Library when remove it from post meta?
                // Note: it might affect other posts if you use same image for multiple posts
                'force_delete'     => false,
                // Maximum image uploads
                'max_file_uploads' => 1,
                // Display the "Uploaded 1/2 files" status
                'max_status'       => true,
            ),
            array(
                'id'      => 'res_cuisine_type',
                'name'    => __('Cuisine Type', karisma_text_domain),
                'type'    => 'text',
            ),
            array(
                'id'      => 'res_capacity',
                'name'    => __('Capacity', karisma_text_domain),
                'type'    => 'number',
            ),
            array(
                'id'   => 'res_location',
                'name' => __('Location', karisma_text_domain),
                'type' => 'textarea',
            ),
            array(
                'id'   => 'res_opening_hours',
                'name' => __('Opening Hours', karisma_text_domain),
                'type' => 'textarea',
            ),
            array(
                'id'   => 'res_telephone',
                'name' => __('Telephone', karisma_text_domain),
                'type' => 'text',
            ),
            array(
                'id'   => 'res_email',
                'name' => __('Email', karisma_text_domain),
                'type' => 'text',
            ),
        ),
    );
    $meta_boxes[] = array(
        'title'  => __('Restaurant & Cafe Menu'),
        'post_types' => 'restaurant-cafe',
        'fields' => array(
            array(
                'id'     => 'standard',
                // Group field
                'type'   => 'group',
                // Clone whole group?
                'clone'  => true,
                // Drag and drop clones to reorder them?
                'sort_clone' => true,
                'add_button' => __('Add another category', karisma_text_domain),
                // Sub-fields
                'fields' => array(
                    array(
                        'name' => __('Category Menu', karisma_text_domain),
                        'id'   => 'category-menu',
                        'type' => 'text',
                    ),
                    array(
                        'name'    => __('Food Menu', karisma_text_domain),
                        'id'      => 'food-menu',
                        'type'    => 'group',
                        'fields' => array(
                            array(
                                'id'   => 'single-food-menu',
                                'type' => 'text',
                                'clone' => true,
                                'add_button' => __('Add another menu', karisma_text_domain),
                            ),
                        ),
                    ),
                ),
            ),
        ),
    );
    return $meta_boxes;
}
// Hotel Information
function hotel_info_meta_boxes($meta_boxes)
{
    $prefix = 'indohotels_';
    $meta_boxes[] = array(
        'id'         => 'hotel_info_button_action',
        'context'    => 'normal',
        'priority'   => 'high',
        'title'      => __('Button Action', karisma_text_domain),
        'post_types' => 'hotel-info',
        'fields'     => array(
            array(
                'id'      => "{$prefix}button_action_url",
                'name'    => __('Link Action', karisma_text_domain),
                'type' => 'text',
            ),
            array(
                'id'      => "{$prefix}button_action_text",
                'name'    => __('Text Action', karisma_text_domain),
                'type' => 'text',
            ),
        ),
    );
    $meta_boxes[] = array(
        'id'         => 'hotel_info_retail',
        'context'    => 'normal',
        'priority'   => 'high',
        'title'      => __('Hotel Info photo gallery', karisma_text_domain),
        'post_types' => 'hotel-info',
        'fields'     => array(
            array(
                'name'             => __('Upload Gallery Photos (Recommended)', karisma_text_domain),
                'id'               => "{$prefix}hotelInfo",
                'type'             => 'image_advanced',
                'force_delete'     => false,
                'max_file_uploads' => 6,
                'max_status'       => true,
            ),
        ),
    );
    return $meta_boxes;
}
// Deals
function deals_meta_boxes($meta_boxes)
{
    $prefix = 'indohotels_';
    
    $meta_boxes[] = array(
        'id'         => 'deals_button_action',
        'context'    => 'normal',
        'priority'   => 'high',
        'title'      => __('Button Action', karisma_text_domain),
        'post_types' => 'deals',
        'fields'     => array(
            array(
                'id'      => "{$prefix}button_action_url",
                'name'    => __('Link Action', karisma_text_domain),
                'type' => 'text',
            ),
            array(
                'id'      => "{$prefix}button_action_text",
                'name'    => __('Text Action', karisma_text_domain),
                'type' => 'text',
            ),
        ),
    );
    $meta_boxes[] = array(
        'id'         => 'deals_retail',
        'context'    => 'normal',
        'priority'   => 'high',
        'title'      => __('Deals photo gallery', karisma_text_domain),
        'post_types' => 'deals',
        'fields'     => array(
            array(
                'name'             => __('Upload Gallery Photos (Recommended)', karisma_text_domain),
                'id'               => "{$prefix}imgDeals",
                'type'             => 'image_advanced',
                'force_delete'     => false,
                'max_file_uploads' => 6,
                'max_status'       => true,
            ),
        ),
    );
    return $meta_boxes;
}
// Beauty & Retail
function beauty_retail_meta_boxes($meta_boxes)
{
    $prefix = 'indohotels_';
    $meta_boxes[] = array(
        'id'         => 'gallery_beauty_retail',
        'context'    => 'normal',
        'priority'   => 'high',
        'title'      => __('Beauty & Retail photo gallery', karisma_text_domain),
        'post_types' => 'beauty-retail',
        'fields'     => array(
            array(
                'name'             => __('Upload Gallery Photos (Recommended)', karisma_text_domain),
                'id'               => "{$prefix}imgBeautyRetail",
                'type'             => 'image_advanced',
                // Delete image from Media Library when remove it from post meta?
                // Note: it might affect other posts if you use same image for multiple posts
                'force_delete'     => false,
                // Maximum image uploads
                'max_file_uploads' => 6,
                // Display the "Uploaded 1/2 files" status
                'max_status'       => true,
            ),
        ),
    );
    $meta_boxes[] = array(
        'id'         => 'info_beauty_retail',
        'context'    => 'normal',
        'priority'   => 'high',
        'title'      => __('Information Beauty & Retail', karisma_text_domain),
        'post_types' => 'beauty-retail',
        'fields'     => array(
            array(
                'name'             => __('Logo Beauty & Retail', karisma_text_domain),
                'id'               => "{$prefix}logoBeautyRetail",
                'type'             => 'image_advanced',
                // Delete image from Media Library when remove it from post meta?
                // Note: it might affect other posts if you use same image for multiple posts
                'force_delete'     => false,
                // Maximum image uploads
                'max_file_uploads' => 1,
                // Display the "Uploaded 1/2 files" status
                'max_status'       => true,
            ),
            array(
                'id'      => 'res_cuisine_type',
                'name'    => __('Cuisine Type', karisma_text_domain),
                'type'    => 'text',
            ),
            array(
                'id'      => 'res_capacity',
                'name'    => __('Capacity', karisma_text_domain),
                'type'    => 'number',
            ),
            array(
                'id'   => 'res_location',
                'name' => __('Location', karisma_text_domain),
                'type' => 'textarea',
            ),
            array(
                'id'   => 'res_opening_hours',
                'name' => __('Opening Hours', karisma_text_domain),
                'type' => 'textarea',
            ),
            array(
                'id'   => 'res_telephone',
                'name' => __('Telephone', karisma_text_domain),
                'type' => 'text',
            ),
            array(
                'id'   => 'res_email',
                'name' => __('Email', karisma_text_domain),
                'type' => 'text',
            ),
        ),
    );
    $meta_boxes[] = array(
        'id'         => 'optional_beauty_retail',
        'context'    => 'normal',
        'priority'   => 'high',
        'title'      => __('Another menu', karisma_text_domain),
        'post_types' => 'beauty-retail',
        'fields'     => array(
            array(
                'name'             => __('Button Menu', karisma_text_domain),
                'id'               => "{$prefix}button_menu",
                'type'             => 'text',
            ),
            array(
                'name'             => __('Button Website', karisma_text_domain),
                'id'               => "{$prefix}button_website",
                'type'             => 'text',
            ),
        ),
    );
    return $meta_boxes;
}
// Meetings & Events
function meetings_events_meta_boxes($meta_boxes)
{
    $prefix = 'indohotels_';
    $meta_boxes[] = array(
        'id'         => 'table_meetings_events',
        'context'    => 'normal',
        'priority'   => 'high',
        'title'      => __('Meetings & Events Extra Text', karisma_text_domain),
        'post_types' => 'meetings-events',
        'fields'     => array(
            array(
                'name'             => __('Extra Text', karisma_text_domain),
                'id'               => "{$prefix}extra_text",
                'type'             => 'wysiwyg',
            ),
        ),
    );
    $meta_boxes[] = array(
        'id'         => 'proposal_meetings_events',
        'context'    => 'normal',
        'priority'   => 'high',
        'title'      => __('Proposal Info & Settings', karisma_text_domain),
        'post_types' => 'meetings-events',
        'fields'     => array(
            array(
                'name'             => __('Url RSVP', karisma_text_domain),
                'id'               => "{$prefix}url_rsvp",
                'type'             => 'text',
            ),
        ),
    );
    $meta_boxes[] = array(
        'id'         => 'gallery_meetings_events',
        'context'    => 'normal',
        'priority'   => 'high',
        'title'      => __('Meetings & Events photo gallery', karisma_text_domain),
        'post_types' => 'meetings-events',
        'fields'     => array(
            array(
                'name'             => __('Upload Gallery Photos (Recommended)', karisma_text_domain),
                'id'               => "{$prefix}imgMeetingsEvents",
                'type'             => 'image_advanced',
                // Delete image from Media Library when remove it from post meta?
                // Note: it might affect other posts if you use same image for multiple posts
                'force_delete'     => false,
                // Maximum image uploads
                'max_file_uploads' => 6,
                // Display the "Uploaded 1/2 files" status
                'max_status'       => true,
            ),
        ),
    );
    $meta_boxes[] = array(
        'id'         => 'info_meetings_events',
        'context'    => 'normal',
        'priority'   => 'high',
        'title'      => __('Information Meetings Events', karisma_text_domain),
        'post_types' => 'meetings-events',
        'fields'     => array(
            array(
                'name'             => __('Logo Meetings & Events', karisma_text_domain),
                'id'               => "{$prefix}logoMeetingsEvents",
                'type'             => 'image_advanced',
                // Delete image from Media Library when remove it from post meta?
                // Note: it might affect other posts if you use same image for multiple posts
                'force_delete'     => false,
                // Maximum image uploads
                'max_file_uploads' => 1,
                // Display the "Uploaded 1/2 files" status
                'max_status'       => true,
            ),
            array(
                'id'      => 'res_cuisine_type',
                'name'    => __('Cuisine Type', karisma_text_domain),
                'type'    => 'text',
            ),
            array(
                'id'      => 'res_capacity',
                'name'    => __('Capacity', karisma_text_domain),
                'type'    => 'number',
            ),
            array(
                'id'   => 'res_location',
                'name' => __('Location', karisma_text_domain),
                'type' => 'textarea',
            ),
            array(
                'id'   => 'res_opening_hours',
                'name' => __('Opening Hours', karisma_text_domain),
                'type' => 'textarea',
            ),
            array(
                'id'   => 'res_telephone',
                'name' => __('Telephone', karisma_text_domain),
                'type' => 'phone',
            ),
            array(
                'id'   => 'res_email',
                'name' => __('Email', karisma_text_domain),
                'type' => 'email',
            ),
        ),
    );
    return $meta_boxes;
}

// Pages
function page_meta_boxes($meta_boxes)
{
    $prefix = 'indohotels_';
    $meta_boxes[] = array(
        'id'         => 'gallery_page',
        'context'    => 'normal',
        'priority'   => 'high',
        'title'      => __('Room photo gallery', karisma_text_domain),
        'post_types' => 'page',
        'fields'     => array(
            // IMAGE ADVANCED - RECOMMENDED
            array(
                'name'             => __('Upload Gallery Photos (Recommended)', karisma_text_domain),
                'id'               => "{$prefix}imgpages",
                'type'             => 'image_advanced',
                // Delete image from Media Library when remove it from post meta?
                // Note: it might affect other posts if you use same image for multiple posts
                'force_delete'     => false,
                // Maximum image uploads
                'max_file_uploads' => 30,
                // Display the "Uploaded 1/2 files" status
                'max_status'       => true,
            ),
        ),
    );
    $meta_boxes[] = array(
        'id'         => 'gallery_page',
        'context'    => 'normal',
        'priority'   => 'high',
        'title'      => __('Room photo gallery', karisma_text_domain),
        'post_types' => 'page',
        'fields'     => array(
            // IMAGE ADVANCED - RECOMMENDED
            array(
                'name'             => __('Upload Gallery Photos (Recommended)', karisma_text_domain),
                'id'               => "{$prefix}imgpages",
                'type'             => 'image_advanced',
                // Delete image from Media Library when remove it from post meta?
                // Note: it might affect other posts if you use same image for multiple posts
                'force_delete'     => false,
                // Maximum image uploads
                'max_file_uploads' => 60,
                // Display the "Uploaded 1/2 files" status
                'max_status'       => true,
            ),
        ),
    );

    return $meta_boxes;
}

function contact_meta_box($meta_boxes)
{
    $meta_boxes[] = array(
        'title'  => __('Google Map', karisma_text_domain),
        'post_types' => 'page',
        'fields' => array(
            // Map requires at least one address field (with type = text)
            array(
                'id'   => 'address',
                'name' => __('Address', karisma_text_domain),
                'type' => 'text',
                'std'  => __('Eastparc', karisma_text_domain),
            ),
            array(
                'id'            => 'map',
                'name'          => __('Location', karisma_text_domain),
                'type'          => 'map',
                // Default location: 'latitude,longitude[,zoom]' (zoom is optional)
                'std'           => '-7.781704, 110.408405',
                // Name of text field where address is entered. Can be list of text fields, separated by commas (for ex. city, state)
                'address_field' => 'address',
                'api_key'       => 'AIzaSyBIkMEljx9JsxD0Rl3Bo-Upd7PfUkLHalM', // https://metabox.io/docs/define-fields/#section-map
            ),
        ),
    );
    $meta_boxes[] = array(
        'title'  => __('Contact Details', karisma_text_domain),
        'post_types' => 'page',
        'fields' => array(
            array(
                'id'      => 'contact_title',
                'name'    => __('Headline Contact', karisma_text_domain),
                'type'    => 'text',
            ),
            array(
                'id'      => 'contact_address',
                'name'    => __('Your Address', karisma_text_domain),
                'type'    => 'textarea',
            ),
            array(
                'id'      => 'contact_phone',
                'name'    => __('Phone Number', karisma_text_domain),
                'type'    => 'text',
                'clone'   => true,
            ),
            array(
                'id'      => 'contact_mobile',
                'name'    => __('Mobile Number', karisma_text_domain),
                'type'    => 'number',
                'clone'   => true,
            ),
            array(
                'id'      => 'contact_fax',
                'name'    => __('Fax Number', karisma_text_domain),
                'type'    => 'text',
                'clone'   => true,
            ),
            array(
                'id'      => 'contact_email',
                'name'    => __('Email', karisma_text_domain),
                'type'    => 'email',
                'clone'   => true,
            ),
        ),
    );
    return $meta_boxes;
}
function gallery_photos_meta_box($meta_boxes)
{
    $prefix = 'indohotels_';
    $meta_boxes[] = array(
        'id'         => 'gallery_photos',
        'context'    => 'normal',
        'priority'   => 'high',
        'title'      => __('Photo gallery', karisma_text_domain),
        'post_types' => 'gallery',
        'fields'     => array(
            // IMAGE ADVANCED - RECOMMENDED
            array(
                'name'             => __('Upload Gallery Photos (Recommended)', karisma_text_domain),
                'id'               => "{$prefix}imgPhotoGallery",
                'type'             => 'image_advanced',
                // Delete image from Media Library when remove it from post meta?
                // Note: it might affect other posts if you use same image for multiple posts
                'force_delete'     => false,
                // Maximum image uploads
                'max_file_uploads' => 30,
                // Display the "Uploaded 1/2 files" status
                'max_status'       => true,
            ),
        ),
    );
    $meta_boxes[] = array(
        'title'  => __('Home Gallery Details', karisma_text_domain),
        'post_types' => 'gallery',
        'fields' => array(
            array(
                'id'      => 'gallery_openning',
                'name'    => __('Opening Hour', karisma_text_domain),
                'type'    => 'time',
            ),
            array(
                'id'      => 'gallery_closing',
                'name'    => __('Closing Hour', karisma_text_domain),
                'type'    => 'time',
            ),
            array(
                'id'      => 'gallery_telephone',
                'name'    => __('Gallery Telephone', karisma_text_domain),
                'type'    => 'number',
            ),
        ),
    );
    return $meta_boxes;
}
