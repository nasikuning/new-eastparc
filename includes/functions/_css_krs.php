<?php

// Do not load directly...
if (!defined('ABSPATH')) {
    die('Direct access forbidden.');
}

// Load karisma styles
function krs_styles()
{
    wp_register_style('roboto', '//fonts.googleapis.com/css?family=Roboto:300,400,500', array(), '1.0', 'all');
    wp_register_style('karisma-style', krs_url . 'style.css', array(), '0.1.5', 'all');
    wp_register_style('bootstrap-min', krs_style . 'vendor/bootstrap/css/bootstrap.min.css', array(), '3.3.7', 'all');
    wp_register_style('font-awesome-min', krs_style . 'vendor/font-awesome/css/font-awesome.min.css', array(), '4.7.0', 'all');
    wp_register_style('myfontastic', krs_style . 'vendor/myfontastic/icons.css', array(), '1.0', 'all');
    wp_register_style('animate-css', krs_style . 'vendor/animate/css/animate.min.css', array(), '0.7.0', 'all');
    wp_register_style('jquery-ui', '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.min.css', array(), '1.0', 'all');
    wp_register_style('owl', krs_style . 'vendor/owl/owl.carousel.min.css', array(), '1.2', 'all');
    wp_register_style('owl-theme', krs_style . 'vendor/owl/owl.theme.default.min.css', array(), '1.2', 'all');
    wp_register_style('slick', krs_style . 'vendor/slick/slick.css', array(), '1.2', 'all');
    wp_register_style('slick-theme', krs_style . 'vendor/slick/slick-theme.css', array(), '1.2', 'all');
    wp_register_style('magnific-popup', krs_style . 'vendor/magnific/magnific-popup.css', array(), '1.2', 'all');
    wp_register_style('caleran', krs_url . 'asset/css/caleran.min.css', array(), '1.5', 'all');
    wp_register_style('intlTelInput', krs_url . 'asset/css/intlTelInput.min.css', array(), '1.5', 'all');
    wp_register_style('addon', krs_url . 'asset/css/addon.css', array(), '1.0', 'all');


    wp_enqueue_style('jquery-ui');
    wp_enqueue_style('bootstrap-min');
    // wp_enqueue_style('animate-css'); 
    wp_enqueue_style('font-awesome-min');
    wp_enqueue_style('myfontastic');
    wp_enqueue_style('roboto');
    wp_enqueue_style('owl');
    wp_enqueue_style('owl-theme');
    wp_enqueue_style('slick');
    wp_enqueue_style('slick-theme');
    wp_enqueue_style('magnific-popup');
    wp_enqueue_style('intlTelInput');
    wp_enqueue_style('caleran');
    wp_enqueue_style('karisma-style');
    wp_enqueue_style('addon');


}

function krs_styles_admin($hook)
{
    // print_r($hook);die();
    if($hook != 'hotel-booking_page_hotel-booking-price'){
        return;
    }
    wp_register_style('booking-addon', krs_url . 'asset/css/booking-addon.css', array(), '1.0', 'all');
    wp_enqueue_style('booking-addon');
}
