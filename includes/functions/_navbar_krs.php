<?php

// karisma navigation
function karisma_nav()
{
    wp_nav_menu(
        array(
            'theme_location'  => 'header-menu',
            'menu'            => 'header-menu',
            'depth'             => 2,
            'container'         => 'div',
            'container_class'   => 'collapse navbar-collapse',
            'container_id'      => 'bs-example-navbar-collapse-1',
            'menu_class'        => 'nav navbar-nav',
            'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
            'walker'            => ''
            )
        );
}
function karisma_nav_mobile()
{
    wp_nav_menu(
        array(
            'theme_location'  => 'header-menu',
            'menu'            => 'header-menu',
            'depth'             => 2,
            'container'         => 'div',
            'container_class'   => 'collapse navbar-collapse',
            'container_id'      => 'bs-example-navbar-collapse-1',
            'menu_class'        => 'monav',
            'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
            'walker'            => ''
            )
        );
}
function karisma_nav_footer()
{
    wp_nav_menu(
        array(
            'theme_location'  => 'footer-menu',
            'menu'            => 'footer-menu',
            'container'       => 'div',
            'container_class' => 'menu-{menu slug}-container',
            'container_id'    => '',
            'menu_class'      => 'menu',
            'menu_id'         => '',
            'echo'            => true,
            'fallback_cb'     => 'wp_page_menu',
            'before'          => '',
            'after'           => '',
            'link_before'     => '',
            'link_after'      => '',
            'items_wrap'      => '<ul class="footer-menu clearfix">%3$s</ul>',
            'depth'           => 0,
            'walker'          => ''
            )
        );
}
function special_nav_class ($classes, $item) {
    if (in_array('current-menu-item', $classes) ){
        $classes[] = 'active ';
    }
    return $classes;
}
function special_nav_submenu_class ($classes, $item) {
    if (in_array('menu-item-has-children', $classes) ){
        $classes[] = 'has-submenu ';
    }
    return $classes;
}
// Register karisma Navigation
function register_karisma_menu()
{
register_nav_menus(array( // Using array to specify more menus if needed
    'header-menu' => __('Header Menu', 'karisma_text_domain'), // Main Navigation
    'footer-menu' => __('Footer Menu', 'karisma_text_domain') // Extra Navigation if needed (duplicate as many as you need!)
    ));
}

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = '')
{
    $args['container'] = false;
    return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
    return is_array($var) ? array() : '';
}

function navbar_extra_class($menu) {
        $menu = preg_replace('/ class="sub-menu"/','/ class="sub-menu dropdown-menu" /',$menu);      
        return $menu;  
}

add_filter( 'nav_menu_link_attributes', 'wpse156165_menu_add_class', 10, 3 );

function wpse156165_menu_add_class( $atts, $item, $args ) {
    $class = 'nav-li'; // or something based on $item
    $atts['class'] = $class;
    return $atts;
}