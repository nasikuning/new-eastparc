<?php
// Do not load directly...
if (!defined('ABSPATH')) {
    die('Direct access forbidden.');
}
/**
 * Remove the slug from published post permalinks. Only affect our custom post type, though.
 */
function krs_remove_cpt_slug($post_link, $post, $leavename)
{

    if ('hotel-info' != $post->post_type || 'publish' != $post->post_status) {
        return $post_link;
    }

    $post_link = str_replace('/' . $post->post_type . '/', '/', $post_link);

    return $post_link;
}


/**
 * Have WordPress match postname to any of our public post types (post, page, race)
 * All of our public post types can have /post-name/ as the slug, so they better be unique across all posts
 * By default, core only accounts for posts and pages where the slug is /post-name/
 */
function krs_parse_request_trick($query)
{

    // Only noop the main query
    if (!$query->is_main_query())
        return;

    // Only noop our very specific rewrite rule match
    if (2 != count($query->query) || !isset($query->query['page'])) {
        return;
    }

    // 'name' will be set if post permalinks are just post_name, otherwise the page rule will match
    if (!empty($query->query['name'])) {
        $query->set('post_type', array('post', 'page'));
    }
}

// Sync custom field room price between language
add_filter( 'pll_translate_post_meta', 'translate_post_meta', 10, 3 );
 
function translate_post_meta( $value, $key, $lang ) {
    if ( 'indohotels_room_base_price' === $key ) {
            $value = pll_get_post( $value, $lang );
    }
    if ( 'indohotels_room_reg_price' === $key ) {
            $value = pll_get_post( $value, $lang );
    }
    return $value;
}

/*------------------------------------*\
Custom Post Types
\*------------------------------------*/
function hotel_info()
{
    register_taxonomy_for_object_type('category', 'hotel-info'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'hotel-info');
    register_post_type(
        'hotel-info', // Register Custom Post Type
        array(
            'labels' => array(
                'name' => __('Hotel Information', karisma_text_domain), // Rename these to suit
                'singular_name' => __('Hotel Information', karisma_text_domain),
                'add_new' => __('Add New', karisma_text_domain),
                'add_new_item' => __('Add New Hotel Information Post', karisma_text_domain),
                'edit' => __('Edit', karisma_text_domain),
                'edit_item' => __('Edit Hotel Information Post', karisma_text_domain),
                'new_item' => __('New Hotel Information Post', karisma_text_domain),
                'view' => __('View Hotel Information Post', karisma_text_domain),
                'view_item' => __('View Hotel Information Post', karisma_text_domain),
                'search_items' => __('Search Hotel Information Post', karisma_text_domain),
                'not_found' => __('No Hotel Information found', karisma_text_domain),
                'not_found_in_trash' => __('No Hotel Information Posts found in Trash', karisma_text_domain)
            ),
            'menu_icon'   => 'dashicons-info',
            'publicly_queryable' => true,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => true,
            'capability_type' => 'post',
            'menu_position' => 4,
            'public' => true,
            'hierarchical' => false, // Allows your posts to behave like Hierarchy Pages
            'has_archive' => true,
            'supports' => array(
                'thumbnail',
                'title',
                'editor',
                'excerpt',
            ), // Go to Dashboard Custom HTML5 Blank post for supports
            'can_export' => true, // Allows export in Tools > Export
            'taxonomies' => array(
                'category'
            )
        )
    );
}
function rooms_post()
{
    register_taxonomy_for_object_type('category', 'rooms'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'rooms');
    register_post_type(
        'rooms', // Register Custom Post Type
        array(
            'labels' => array(
                'name' => __('Rooms', karisma_text_domain), // Rename these to suit
                'singular_name' => __('Rooms', karisma_text_domain),
                'add_new' => __('Add New', karisma_text_domain),
                'add_new_item' => __('Add New Rooms Post', karisma_text_domain),
                'edit' => __('Edit', karisma_text_domain),
                'edit_item' => __('Edit Rooms Post', karisma_text_domain),
                'new_item' => __('New Rooms Post', karisma_text_domain),
                'view' => __('View Rooms Post', karisma_text_domain),
                'view_item' => __('View Rooms Post', karisma_text_domain),
                'search_items' => __('Search Rooms Post', karisma_text_domain),
                'not_found' => __('No Rooms found', karisma_text_domain),
                'not_found_in_trash' => __('No Rooms Posts found in Trash', karisma_text_domain)
            ),
            'menu_icon'   => 'dashicons-admin-home',
            'publicly_queryable' => true,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => true,
            'capability_type' => 'post',
            'menu_position' => 4,
            'public' => true,
            'hierarchical' => false, // Allows your posts to behave like Hierarchy Pages
            'has_archive' => true,
            'show_in_nav_menus' => true,
            'supports' => array(
                'thumbnail',
                'title',
                'editor',
                'excerpt',
            ), // Go to Dashboard Custom HTML5 Blank post for supports
            'can_export' => true, // Allows export in Tools > Export
            'taxonomies' => array(
                'category'
            )
        )
    );
}
function restaurant_cafe_post()
{
    register_taxonomy_for_object_type('category', 'restaurant-cafe'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'restaurant-cafe');
    register_post_type(
        'restaurant-cafe', // Register Custom Post Type
        array(
            'labels' => array(
                'name' => __('Restaurant & Cafe', karisma_text_domain), // Rename these to suit
                'singular_name' => __('Restaurant & Cafe', karisma_text_domain),
                'add_new' => __('Add New', karisma_text_domain),
                'add_new_item' => __('Add New Restaurant or Cafe Post', karisma_text_domain),
                'edit' => __('Edit', karisma_text_domain),
                'edit_item' => __('Edit Restaurant or Cafe Post', karisma_text_domain),
                'new_item' => __('New Restaurant or Cafe Post', karisma_text_domain),
                'view' => __('View Restaurant & Cafe Post', karisma_text_domain),
                'view_item' => __('View RRestaurant & Cafe Post', karisma_text_domain),
                'search_items' => __('Search Restaurant & Cafe Post', karisma_text_domain),
                'not_found' => __('No Rooms found', karisma_text_domain),
                'not_found_in_trash' => __('No Restaurant & Cafe Posts found in Trash', karisma_text_domain)
            ),
            'menu_icon'   => 'dashicons-store',
            'publicly_queryable' => true,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => true,
            'capability_type' => 'post',
            'menu_position' => 4,
            'public' => true,
            'hierarchical' => false, // Allows your posts to behave like Hierarchy Pages
            'has_archive' => true,
            'supports' => array(
                'thumbnail',
                'title',
                'editor',
                'excerpt',
            ), // Go to Dashboard Custom HTML5 Blank post for supports
            'can_export' => true, // Allows export in Tools > Export
            'taxonomies' => array(
                'category'
            )
        )
    );
}
function beauty_retail_post()
{
    register_taxonomy_for_object_type('category', 'beauty-retail'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'beauty-retail');
    register_post_type(
        'beauty-retail', // Register Custom Post Type
        array(
            'labels' => array(
                'name' => __('Beauty & Retail', karisma_text_domain), // Rename these to suit
                'singular_name' => __('Beauty & Retail', karisma_text_domain),
                'add_new' => __('Add New', karisma_text_domain),
                'add_new_item' => __('Add New Beauty & Retail Post', karisma_text_domain),
                'edit' => __('Edit', karisma_text_domain),
                'edit_item' => __('Edit Beauty or Retail Post', karisma_text_domain),
                'new_item' => __('New Beauty or Retail Post', karisma_text_domain),
                'view' => __('View Beauty & Retail Post', karisma_text_domain),
                'view_item' => __('View Beauty & Retail Post', karisma_text_domain),
                'search_items' => __('Search Beauty & Retail Post', karisma_text_domain),
                'not_found' => __('No Rooms found', karisma_text_domain),
                'not_found_in_trash' => __('No Beauty or Retail Posts found in Trash', karisma_text_domain)
            ),
            'menu_icon'   => 'dashicons-heart',
            'publicly_queryable' => true,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => true,
            'capability_type' => 'post',
            'menu_position' => 4,
            'public' => true,
            'hierarchical' => false, // Allows your posts to behave like Hierarchy Pages
            'has_archive' => true,
            'supports' => array(
                'thumbnail',
                'title',
                'editor',
                'excerpt',
            ), // Go to Dashboard Custom HTML5 Blank post for supports
            'can_export' => true, // Allows export in Tools > Export
            'taxonomies' => array(
                'category'
            )
        )
    );
}
function meetings_events_post()
{
    register_taxonomy_for_object_type('category', 'meetings-events'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'meetings-events');
    register_post_type(
        'meetings-events', // Register Custom Post Type
        array(
            'labels' => array(
                'name' => __('Meetings & Events', karisma_text_domain), // Rename these to suit
                'singular_name' => __('Meetings & Events', karisma_text_domain),
                'add_new' => __('Add New', karisma_text_domain),
                'add_new_item' => __('Add New Meetings or Events', karisma_text_domain),
                'edit' => __('Edit', karisma_text_domain),
                'edit_item' => __('Edit Meetings or Events', karisma_text_domain),
                'new_item' => __('New Meetings or Events Post', karisma_text_domain),
                'view' => __('View Meetings & Events Post', karisma_text_domain),
                'view_item' => __('View Meetings & Events Post', karisma_text_domain),
                'search_items' => __('Search Meetings & Events Post', karisma_text_domain),
                'not_found' => __('No Rooms found', karisma_text_domain),
                'not_found_in_trash' => __('No Meetings or Events Posts found in Trash', karisma_text_domain)
            ),
            'menu_icon'   => 'dashicons-businessman',
            'publicly_queryable' => true,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => true,
            'capability_type' => 'post',
            'menu_position' => 4,
            'public' => true,
            'hierarchical' => false, // Allows your posts to behave like Hierarchy Pages
            'has_archive' => true,
            'supports' => array(
                'thumbnail',
                'title',
                'editor',
                'excerpt',
            ), // Go to Dashboard Custom HTML5 Blank post for supports
            'can_export' => true, // Allows export in Tools > Export
            'taxonomies' => array(
                'category'
            )
        )
    );
}
function deals_post()
{
    register_post_type(
        'deals', // Register Custom Post Type
        array(
            'labels' => array(
                'name' => __('Deals', karisma_text_domain), // Rename these to suit
                'singular_name' => __('Deals', karisma_text_domain),
                'add_new' => __('Add New', karisma_text_domain),
                'add_new_item' => __('Add New Deals Post', karisma_text_domain),
                'edit' => __('Edit', karisma_text_domain),
                'edit_item' => __('Edit Deals Post', karisma_text_domain),
                'new_item' => __('New Deals Post', karisma_text_domain),
                'view' => __('View Deals Post', karisma_text_domain),
                'view_item' => __('View Deals Post', karisma_text_domain),
                'search_items' => __('Search Deals Post', karisma_text_domain),
                'not_found' => __('No Deals found', karisma_text_domain),
                'not_found_in_trash' => __('No Deals Posts found in Trash', karisma_text_domain)
            ),
            'menu_icon'   => 'dashicons-clipboard',
            'menu_position'       => 3,
            'query_var' => true,
            'capability_type' => 'post',

            'public' => true,
            'hierarchical' => false, // Allows your posts to behave like Hierarchy Pages
            'has_archive' => false,
            'supports' => array(
                'thumbnail',
                'title',
                'editor',
                'excerpt',
            ), // Go to Dashboard Custom HTML5 Blank post for supports
            'can_export' => true, // Allows export in Tools > Export
        )
    );
}
