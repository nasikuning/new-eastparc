<?php
function get_payment_flag(){
        if (!is_page('book-result')){
        if (isset($_POST['klikPayCode']) && isset($_POST['transactionNo']) && isset($_POST['signature'])) {
            global $wpdb;
            $query = "  
                    SELECT * 
                    FROM 
                        wp_ss_books_trans t,
                        wp_ss_books_trans_detail d
                    WHERE 
                        t.trans_no = d.trans_no AND 
                        t.trans_no='".$_POST['transactionNo']."' AND 
                        t.signature='".$_POST['signature']."'
                    ";
            $transaction  = $wpdb->get_results($query,ARRAY_A);
            
            header("Content-type: text/xml");
            $doc = new DOMDocument();
            // we want a nice output
            $doc->formatOutput = true;

            $root = $doc->createElement('OutputListTransactionIPAY');
            $root = $doc->appendChild($root);

                $title = $doc->createElement('klikPayCode');
                $title = $root->appendChild($title);

                    $text = $doc->createTextNode($_POST['klikPayCode']);
                    $text = $title->appendChild($text);

                $title = $doc->createElement('transactionNo');
                $title = $root->appendChild($title);

                	if ($transaction){
                        $transactionNo = $_POST['transactionNo'];
                    }else{
                	 	$transactionNo="";
                    }

                    $text = $doc->createTextNode($transactionNo);
                    $text = $title->appendChild($text);

                $title = $doc->createElement('currency');
                $title = $root->appendChild($title);
            		if ($transaction){
                        $currency ="IDR";
                    }else{
                	 	$currency="";
                    }
                    $text = $doc->createTextNode($currency);
                    $text = $title->appendChild($text);

                $title = $doc->createElement('fullTransaction');
                $title = $root->appendChild($title);

                    $sub_title = $doc->createElement('amount');
                    $sub_title = $title->appendChild($sub_title);

                        foreach ($transaction as $item) {
                            $item_amount = $item['total_amount'];
                        }

                        $text = $doc->createTextNode($item_amount);
                        $text = $sub_title->appendChild($text);

                    $sub_title = $doc->createElement('description');
                    $sub_title = $title->appendChild($sub_title);

                    	if ($transaction){
	                        $item_room_id="booking room id: ";
	                    }else{
                    	 	$item_room_id="";
	                    }
                        foreach ($transaction as $item) {
                            $item_room_id .= $item['room_id']." ";
                        }

                        $text = $doc->createTextNode($item_room_id);
                        $text = $sub_title->appendChild($text);

                $title = $doc->createElement('installmentTransactions');
                $title = $root->appendChild($title);
                /*
                    $sub_title = $doc->createElement('itemName');
                    $sub_title = $title->appendChild($sub_title);

                        $item_room_id="";
                        foreach ($transaction as $item) {
                            $item_room_id .= $item['room_id']." ";
                        }

                        $text = $doc->createTextNode('book room id '.$item_room_id);
                        $text = $sub_title->appendChild($text);
                        
                    $sub_title = $doc->createElement('quantity');
                    $sub_title = $title->appendChild($sub_title);

                        $text = $doc->createTextNode('1');
                        $text = $sub_title->appendChild($text);

                    $sub_title = $doc->createElement('amount');
                    $sub_title = $title->appendChild($sub_title);

                        foreach ($transaction as $item) {
                            $item_amount = $item['total_amount'];
                        }

                        $text = $doc->createTextNode($item_amount);
                        $text = $sub_title->appendChild($text);

                    $sub_title = $doc->createElement('tenor');
                    $sub_title = $title->appendChild($sub_title);

                        $text = $doc->createTextNode('01');
                        $text = $sub_title->appendChild($text);

                    $sub_title = $doc->createElement('codePlan');
                    $sub_title = $title->appendChild($sub_title);

                        $text = $doc->createTextNode('000');
                        $text = $sub_title->appendChild($text);
                    
                    $sub_title = $doc->createElement('merchantId');
                    $sub_title = $title->appendChild($sub_title);

                        $text = $doc->createTextNode('EASTPA123');
                        $text = $sub_title->appendChild($text);
				*/
                $title = $doc->createElement('miscFee');
                $title = $root->appendChild($title);

                	if ($transaction){
                        $miscFee="0.00";
                    }else{
                	 	$miscFee="";
                    }

                    $text = $doc->createTextNode($miscFee);
                    $text = $title->appendChild($text);
                  
            echo $doc->saveXML();
            die();
        }
        }
    }

    add_action('wp','get_payment_flag');