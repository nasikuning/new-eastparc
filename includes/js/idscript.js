function getParameterByName(name, url) {
    if (!url) {
      url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function getQueryStringValue (key) {  
  return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));  
}  



jQuery(document).ready(function() {
    var checkin = getQueryStringValue("checkin");
    var checkout = getQueryStringValue("checkout");
    var siteurl = window.location.href.split('?')[0];

    jQuery('#ids-frame').attr("src","http://127.0.0.1:8200/property-results/a16ebe6f-3c6a-4f9b-8b35-d8dd7ee1d002/?siteurl="+siteurl+"&checkin="+checkin+"&checkout="+checkout);
    iFrameResize();
});
