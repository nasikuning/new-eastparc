<?php /* Template Name: succesxass form */ ?>
<?php get_header(); ?>
<?php 
    global $ss_theme_opt; 
    
    //-----------PAGE HEADER-------------------
    if($ss_theme_opt['page-header-use-row-class']){
        $page_header_div_class = ' row';
    }
    
    if(isset($ss_theme_opt['page-header-background-color'])){
        $page_header_style = "background-color: ".$ss_theme_opt['page-header-background-color']."";
    }

    if(isset($ss_theme_opt['page-header-background']['url']) && $ss_theme_opt['page-header-background']['url'] != ''){
        $page_header_style = "background-image: url('".$ss_theme_opt['page-header-background']['url']."')";
    }

    if($ss_theme_opt['page-header-use-featured-image']){
        if(has_post_thumbnail()){
            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
            $page_header_style = "background-image: url('".$image[0]."')";
        }
    }

    $page_header_custom_class = $ss_theme_opt['page-header-custom-class'];
    if($ss_theme_opt['page-header-overlay']){
        $page_header_custom_class .= ' overlay-bg';
    }
    if($ss_theme_opt['page-header-full-width']){
        $page_header_custom_class .= ' full-width';
    }


    //-----------PAGE FOOTER-------------------
    if($ss_theme_opt['page-footer-use-row-class']){
        $page_footer_div_class = ' row';
    }
    
    if(isset($ss_theme_opt['page-footer-background-color'])){
        $page_footer_style = "background-color: ".$ss_theme_opt['page-footer-background-color']."";
    }

    if(isset($ss_theme_opt['page-footer-background']['url']) && $ss_theme_opt['page-footer-background']['url'] != ''){
        $page_footer_style = "background-image: url('".$ss_theme_opt['page-footer-background']['url']."')";
    }

    $page_footer_custom_class = $ss_theme_opt['page-footer-custom-class'];
    if($ss_theme_opt['page-footer-overlay']){
        $page_footer_custom_class .= ' overlay-bg';
    }
    if($ss_theme_opt['page-footer-full-width']){
        $page_footer_custom_class .= ' full-width';
    }


    //---------SIDEBAR-----------------------
    $show_sidebar = true;

    //check if sidebar only show on certain page based on theme option
    if($ss_theme_opt['page-sidebar-page-id'] != ""){
        //get page list as array and remove empty space
        $str_page = str_replace(' ','',$ss_theme_opt['page-sidebar-page-id']);
        $arr_page = explode(',',$str_page); //convert to array

        if(!is_page($arr_page)){
            $show_sidebar = false;
        }
    }

    //---------CONTENT------------------------
    //prepare class for content
    $page_content_class = array();
    if(!empty($ss_theme_opt['page-content-custom-class'])){
        $page_content_class[] = $ss_theme_opt['page-content-custom-class'];
    }
    
    if($show_sidebar && is_active_sidebar( 'page-sidebar' ) ){
        $page_content_class[] = $ss_theme_opt['page-content-custom-class-sidebar-shown'];
    }

    $str_page_content_class = implode(' ',$page_content_class);

?>


    <?php if ( is_active_sidebar( 'page-header' ) ) { ?>
        <section id="section-page-header" class="theme-section <?php echo $page_header_custom_class; ?>" style="<?php echo $page_header_style; ?>" >
            <div class="container">
                <div id="page-header" class="header-wrapper<?php echo $page_header_div_class; ?>">
                    <?php dynamic_sidebar( 'page-header' ); ?>
                </div>
            </div>
        </section>
    <?php } ?>



    <div id="content" class="page">

        <div class="container">

            <div class="row">
   <h3> super secret form </h3>
    <?php
    class KlikPayKeys {
        var $trace = array();
        
        final public function signature($klikPayCode, $transactionNo, $currency = "IDR", $clearKey, $transactionDate, $totalAmount) {
            $this->trace = array(
                "klikPayCode" => $klikPayCode,
                "transactionNo" => $transactionNo,
                "currency" => $currency,
                "transactionDate" => $transactionDate,
                "totalAmount" => $totalAmount
            );
            $klikPayCode = preg_match('/[A-Z0-9a-z]+/',$klikPayCode,$m) ? substr($m[0],0,10) : "";
            $transactionNo = preg_match('/[A-Z0-9a-z]+/',$transactionNo,$m) ? substr($m[0],0,18) : "";
            $currency = preg_match('/[A-Z]+/',$currency,$m) ? substr($m[0],0,5) : "";
            $transactionDate = preg_match('/\d\d\/\d\d\/\d{4} \d\d:\d\d:\d\d/',$transactionDate,$m) ? substr($m[0],0,19) : "";
            $clearKey = preg_match('/[0-9A-Za-z]+/',$clearKey,$m) ? substr($m[0],0,32) : "";
            $totalAmount = preg_match('/(\d+)\.?/',$totalAmount,$m) ? substr($m[1],0,12) : "";      
            $keyId = strlen($clearKey)==32?$clearKey:$this->keyid($clearKey);
            $this->trace["keyId"] = $keyId;
            $str1 = $klikPayCode.$transactionNo.$currency.$keyId;
            $str2 = intval(str_replace("/","",substr($transactionDate,0,10))) + intval($totalAmount);
            $this->trace["firstval"] = $str1;
            $this->trace["secondval"] = $str2;
            $str1 = $this->dohash($str1);
            $str2 = $this->dohash($str2);
            $str3 = str_replace("-","",bcadd($str1,$str2));
            $this->trace["thirdval"] = $str3;
            return $str3;
        }
        final public function authkey($klikPayCode,$transactionNo,$currency="IDR",$transactionDate,$clearKey) {
            ini_set("default_charset", "utf-8");
            $this->trace = array(
                "klikPayCode" => $klikPayCode,
                "transactionNo" => $transactionNo,
                "currency" => $currency,
                "transactionDate" => $transactionDate,
                "clearKey" => $clearKey
            );
            $keyId = $this->keyid($clearKey);
            return $this->tripledes($this->tomd5($this->concat($klikPayCode,$transactionNo,$currency,$transactionDate,$keyId)),$keyId);
        }
        final public function keyid($clearKey) {
            $hexs = array('0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F');
            for($i=0,$x=mb_strlen($clearKey, 'utf-8'),$r="";$i<$x;$i++)
            $r .= $hexs[(ord($clearKey[$i])&0xFF)/16] . $hexs[(ord($clearKey[$i])&0xFF)%16];
            return $r;
        }
        private function dohash($str) {
            $min = -2147483648;
            $max = 2147483647;
            $str = (string) $str;
            for($i=0,$x=strlen($str),$hash=0;$i<$x;$i++){
                $hash = bcadd(bcmul($hash,31),ord($str[$i]));
                while ($hash > $max)
                $hash = bcsub(bcsub(bcadd($hash,$min),$max),1);
                while ($hash < $min)
                $hash = bcadd(bcsub(bcadd($hash,$max),$min),1);
            }
            return $hash;
        }
        private function concat($klikPayCode,$transactionNo,$currency="IDR",$transactionDate,$keyId) {
            if (strlen($klikPayCode) < 3) return "ERROR: klikPayCode invalid";
            if (strlen($transactionNo) < 1) return "ERROR: transactionNo invalid";
            if (!preg_match('/^(\d\d)\/(\d\d)\/(\d{4}) (\d\d):(\d\d):(\d\d)$/',$transactionDate,$m)) return "ERROR: Format transactionDate invalid";
            if (intval($m[1]) > 31 || intval($m[2]) > 12) return "ERROR: Format transactionDate invalid";
            $klikPayCode = str_pad(substr($klikPayCode,0,10),10,"0",STR_PAD_RIGHT);
            $this->trace['data01'] = $klikPayCode;
            $transactionNo = str_pad(substr($transactionNo,0,18),18,"A",STR_PAD_RIGHT);
            $this->trace['data02'] = $transactionNo;
            $currency = str_pad(substr($currency,0,5),5,"1",STR_PAD_RIGHT);
            $this->trace['data03'] = $currency;
            $transactionDate = str_pad(substr($transactionDate,0,19),19,"C",STR_PAD_LEFT);
            $this->trace['data04'] = $transactionDate;
            $this->trace['step01'] = $keyId;
            $keyId = str_pad($keyId,32,"E",STR_PAD_RIGHT);
            $str = $klikPayCode.$transactionNo.$currency.$transactionDate.$keyId;
            $this->trace['step02'] = $str;
            return $str;
        }
        private function tomd5($str) {
            if (strlen($str) != 84) return "ERROR: String Format invalid";
            $this->trace['step03'] = strtoupper(md5($str));
            return strtoupper(md5($str));
        }
        private function tripledes($str,$key) {
            if (strlen($str) != 32) return "ERROR: String md5 invalid ($str)";
            if (strlen($key) != 32) return "ERROR: KeyId invalid";
            try {
                for ($i=0,$n=strlen($str)/2;$i<$n;$i++)
                    $bhk[$i] = chr(intval(substr($str,2*$i,2),16));
                for ($i=0,$n=strlen($key)/2;$i<$n;$i++)
                    $bk[$i] = chr(intval(substr($key,2*$i,2),16));
                for ($i=0;$i<8;$i++)
                    $bk[] = $bk[$i];
                $bk = implode("",$bk);
                $bhk = implode("",$bhk);
                $mmod = mcrypt_module_open(MCRYPT_TRIPLEDES, '', 'ecb', '');
                mcrypt_generic_init($mmod,$bk, str_pad("",8,"\0",STR_PAD_LEFT));
                $cout = mcrypt_generic($mmod,$bhk);
                mcrypt_module_close($mmod);
                for ($i=0,$n=strlen($cout),$out="";$i<$n;$i++) {
                    $v = dechex(ord($cout[$i])&0xFF);
                    $out .= strlen($v)==1 ? "0".$v : $v;
                }
                $this->trace['step04'] = strtoupper($out);
                return strtoupper($out);
            } catch (Exception $e) {
                echo "ERROR: General Exception :\n".print_r($e,1);
                die;
            }
        }
    }


    $klikPayCode = "02EAST0461";
    $transactionNo = "EASTPARC".time();
    $currency = "IDR";
    $clearKey = "dNie56bn74ndfcxl";
    $trans_date = strtotime(date('Y-m-d H:i:s'));
    $transactionDate = date('d/m/Y H:i:s', $trans_date);
    $totalAmount = "50000.00";

    $bcakey = new KlikPayKeys;
    $signature = $bcakey->signature($klikPayCode, $transactionNo, $currency, $clearKey, $transactionDate, $totalAmount);
    $authkey = $bcakey->authkey($klikPayCode, $transactionNo, $currency, $transactionDate, $clearKey);
    
    
    global $wpdb;
    // Save the transaction data into database
    $trans['trans_no']     = $transactionNo;
    $trans['trans_date']   = date('Y-m-d H:i:s', $trans_date);
    $trans['pay_type']     = '01';
    $trans['currency']     = 'IDR';
    $trans['total_amount'] = $totalAmount;
    $trans['signature']    = $signature;
    $trans['authkey']      = $authkey;  
    $trans['status']       = 'unpaid';
    
    $wpdb->query(sql_new($wpdb->prefix . 'ss_books_trans', $trans));
    
    // Save the detail transaction
   
    $trans_detail['trans_no']   = $transactionNo;
    $trans_detail['room_id']    = 2;
    $trans_detail['checkin']    = '2017-01-27 10:10:10'; 
    $trans_detail['checkout']   = '2017-01-28 10:10:10';
    $trans_detail['arrival_time']   = $detail['book_arrival'];
    $trans_detail['name']       = "Tester please ignore";
    $trans_detail['title']      = "Tester";
    $trans_detail['first_name'] = "please";
    $trans_detail['last_name']  = "ignore";
    $trans_detail['phone']  = "00000000";
    $trans_detail['email']      = "sidiq@softwareseni.com";
    $trans_detail['qty']        = "1";
    $trans_detail['adult']      = "1";
    $trans_detail['child']      = "1";
    $trans_detail['request']    = "this for testing";
    $trans_detail['amount']     = "50000";
    
    $wpdb->query(sql_new($wpdb->prefix . 'ss_books_trans_detail', $trans_detail));
   


    // $post_url = "https://sandbox.sprintasia.net/klikpay/";
    // $post_url = "http://simpg.sprintasia.net:8779/klikpay/webgw";
    // $post_url = "https://202.6.215.230:8081/purchasing/purchase.do?action=loginRequest";
    
    $post_url = " https://klikpay.klikbca.com/purchasing/purchase.do?action=loginRequest";
    
    // $post_url = "http://www.eastparchotel.com/book-result/?booking_number=".$transactionNo;

    ?>

    <div>
        <input type="checkbox" id="confirmation">
        I understand and agree with the <a href="<?php bloginfo('url');?>/syarat-ketentuan">Terms and Conditions</a> for this booking.
    </div>
    <form id="payment_form" method="post" action="<?php echo $post_url; ?>" >
        <input type="hidden" name="klikPayCode" value="02EAST0461">
        <input type="hidden" name="transactionNo" value="<?php echo $transactionNo;?>">
        <input type="hidden" name="totalAmount" value="50000.00">
        <input type="hidden" name="currency" value="IDR">
        <input type="hidden" name="payType" value="01">
        <input type="hidden" name="callback" value="//www.eastparchotel.com/rooms-suites/payment/status/?trans=<?php echo $transactionNo;?>">
        <input type="hidden" name="transactionDate" value="<?php echo date("d/m/Y H:i:s"); ?>">
        <input type="hidden" name="descp" value="">
        <input type="hidden" name="miscFee" value="">
        <input type="hidden" name="signature" value="<?php echo $signature;?>">
        <button type="submit" class="btn btn-primary">Check Out</button>
        <a href="<?php echo $current_url.'?step=cancel'; ?>" class="btn btn-danger">cancel booking</a>
    </form>

    <div class="BCAKlikPay"><img class="col-md-3" src="<?php echo plugins_url();?>/ss-books/images/BCAKlikPay.jpg"></div>
</div>

<script type="text/javascript">

jQuery( "#payment_form" ).submit(function( event ) {
    if(jQuery("#confirmation").prop('checked') != true){
       event.preventDefault();     
    }
});
    
</script>


</div>
            </div>    
        </div>
    <?php if ( is_active_sidebar( 'page-footer' ) ) { ?>
        <section id="section-page-footer" class="theme-section <?php echo $page_footer_custom_class; ?>" style="<?php echo $page_footer_style; ?>" >
            <div class="container">
                <div id="page-footer" class="footer-wrapper<?php echo $page_footer_div_class; ?>">
                    <?php dynamic_sidebar( 'page-footer' ); ?>
                </div>
            </div>
        </section>
    <?php } ?>
        
<?php get_footer(); ?>


