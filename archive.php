<?php get_header(); ?>
<div id="wrapper" class="page">
  <div class="container">
	<div class="section content-aboutus">
	  <h1><?php _e( 'Archives', karisma_text_domain ); ?></h1>
			<?php get_template_part('loop'); ?>
			<?php get_template_part('pagination'); ?>
	</div><!-- end .content-intro -->
  </div><!-- end .container -->
</div><!-- end .content -->
<?php get_footer(); ?>