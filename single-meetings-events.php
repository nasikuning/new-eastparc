<?php get_header(); ?>
<div id="wrapper" class="page">
<?php if (have_posts()): while (have_posts()) : the_post(); ?>
  <div class="section main-slider slider-meeting">
    <div id="slider-main" class="owl-carousel">
    <?php
    $images = rwmb_meta( 'indohotels_imgMeetingsEvents', 'size=gallery-slide' ); // Since 4.8.0
    if ( !empty( $images ) ) {
        foreach ( $images as $image ) {
        echo '<div class="owl-slide" style="background-image: url(\''. $image['full_url'].'\')"></div>';
          }
    }
    ?>
    </div><!-- end .slider-main -->
  </div><!-- end .main-slider -->

    <div class="container">
      <div class="section content-meeting meeting-detail">
        <h1 class="heading-title"><?php the_title(); ?></h1>

        <div class="row margin-blarge">
          <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="box-schedule">
          <ul class="clearfix">
            <?php if(!empty(rwmb_meta('res_cuisine_type'))) :?>
            <li>
              <span class="scleft"><?php pll_e( 'Cuisine Type', karisma_text_domain ); ?></span>
              <span class="dot">:</span>
              <span class="scright"><?php echo rwmb_meta( 'res_cuisine_type' ); ?></span>
            </li>
            <?php endif; ?>
            <?php if(!empty(rwmb_meta('res_capacity'))) :?>
            <li>
              <span class="scleft"><?php pll_e( 'Capacity', karisma_text_domain ); ?></span>
              <span class="dot">:</span>
              <span class="scright"><?php echo rwmb_meta( 'res_capacity' ); ?> <?php pll_e('guests',karisma_text_domain); ?></span>
            </li>
            <?php endif; ?>
            <?php if(!empty(rwmb_meta('res_location'))) :?>
            <li>
              <span class="scleft"><?php pll_e( 'Location', karisma_text_domain ); ?></span>
              <span class="dot">:</span>
              <span class="scright"><?php echo rwmb_meta( 'res_location' ); ?></span>
            </li>
            <?php endif; ?>
            <?php if(!empty(rwmb_meta('res_opening_hours'))) :?>
            <li>
              <span class="scleft"><?php pll_e( 'Opening Hours', karisma_text_domain ); ?></span>
              <span class="dot">:</span>
              <span class="scright"><?php echo rwmb_meta( 'res_opening_hours' ); ?></span>
            </li>
            <?php endif; ?>
            <?php if(!empty(rwmb_meta('res_telephone'))) :?>
            <li>
              <span class="scleft"><?php pll_e( 'Telephone', karisma_text_domain ); ?></span>
              <span class="dot">:</span>
              <span class="scright"><?php echo rwmb_meta( 'res_telephone' ); ?></span>
            </li>
            <?php endif; ?>
            <?php if(!empty(rwmb_meta('res_email'))) :?>
            <li>
              <span class="scleft"><?php pll_e( 'Email', karisma_text_domain ); ?></span>
              <span class="dot">:</span>
              <span class="scright"><?php echo rwmb_meta( 'res_email' ); ?></span>
            </li>
            <?php endif; ?>
          </ul>
        </div><!-- end .box-schedule -->
          </div><!-- end .col-md-4 -->
          <div class="col-md-8 col-sm-8 col-xs-12">
          <?php echo rwmb_meta( 'indohotels_extra_text' ); ?>
          </div><!-- end .col-md-4 -->

          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="box-proposal">
              <p class="text-center"><?php pll_e( 'If you need a proposal, please click the button below.', karisma_text_domain ); ?></p>
              <div class="double-option">
                <?php /* <a href="<?php echo get_site_url('','/eastparc/rsvp/'); ?>" class="nbutton"><?php pll_e( 'Request For Proposal', karisma_text_domain ); ?></a> */ ?>
                <a href="<?php echo rwmb_meta('indohotels_url_rsvp'); ?>" class="nbutton"><?php pll_e( 'Request For Proposal', karisma_text_domain ); ?></a>
              </div>
            </div><!-- end .box-proposal -->
          </div>

          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="info-room">

              <div class="function-room">
                <?php the_content(); ?>
                </div><!-- end .function-room -->
            </div><!-- end .info-room -->
          </div>

        </div><!-- end .row -->

      </div><!-- end .content-intro -->

    </div><!-- end .container -->
    <?php endwhile; ?>

			<?php else: ?>

				<!-- article -->
				<article>

					<h1><?php pll_e( 'Sorry, nothing to display.', karisma_text_domain ); ?></h1>

				</article>
				<!-- /article -->

			<?php endif; ?>
  </div><!-- end .content -->

  <?php get_footer(); ?>
