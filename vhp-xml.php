<?php
/* Template Name: VHP XML */


global $wpdb;

$sql_trans_id= "SELECT DISTINCT d.trans_no FROM " . $wpdb->prefix . "ss_books_trans_detail as d "
        . "LEFT JOIN " . $wpdb->prefix . "ss_books_trans as t "
        . "ON d.trans_no = t.trans_no "
        . "WHERE t.db_update_status = 0 AND t.status = 'paid' AND d.room_id != 0";
$transactions_id = $wpdb->get_results($sql_trans_id);

$sql_trans_id2= "SELECT DISTINCT d.trans_no FROM " . $wpdb->prefix . "ss_books_trans_detail as d "
        . "LEFT JOIN " . $wpdb->prefix . "ss_books_credit_trans as t "
        . "ON d.trans_no = t.trans_no "
        . "WHERE t.db_update_status = 0 AND t.status = 'paid' AND d.room_id != 0";
$transactions_id2 = $wpdb->get_results($sql_trans_id2);

$query = "SELECT * FROM {$wpdb->prefix}ss_room";
$rooms = $wpdb->get_results($query);

$q_name = "SELECT * FROM wp_ss_books_room_list";
$get_room_name = $wpdb->get_results($q_name,ARRAY_A);

if ($_GET['sidebug']){
    echo "<pre>";
    print_r($transactions_id2);
    echo "</pre>";
}

$now = date('Y-m-d\TH:i:sP');
header('Content-Type: application/xml; charset=utf-8');
// header("Content-type: text/xml");
?>
<OTA_ResRetrieveRS xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.opentravel.org/OTA/2003/05" Version="2.000" MaxResponses="10" TimeStamp="<?php echo $now; ?>" xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05 OTA_ResRetrieveRS.xsd">
    <Success/>
    <?php
    foreach ($transactions_id as $transaction_id) :

        $sql = "SELECT * FROM " . $wpdb->prefix . "ss_books_trans_detail as d "
            . "LEFT JOIN " . $wpdb->prefix . "ss_books_trans as t "
            . "ON d.trans_no = t.trans_no "
            . "WHERE t.trans_no = '". $transaction_id->trans_no ."' AND t.db_update_status = 0 AND t.status = 'paid' AND d.room_id != 0";
        $transactions = $wpdb->get_results($sql); 
        $create_date = date('Y-m-d H:i:s e', strtotime($transactions[0]->trans_date));  
        $count=1;
        ?>
        <?php foreach ($transactions as $trans): ?>
        <ReservationsList>
            <HotelReservation CreateDateTime="<?php echo $create_date; ?>" ResStatus="Approved">
                <POS>
                    <Source>
                        <RequestorID Type="1" ID="eastparc_vhp"/>
                        <BookingChannel Type="1">
                            <CompanyName Code="VHP">Visual Hotel Program</CompanyName>
                        </BookingChannel>
                    </Source>
                </POS>
                <RoomStays>
                    
                    <RoomStay>
                        <?php
                           
                            $name_array = explode(' ', $trans->name);

                            // Format customer name
                            $first_name = $name_array[0];
                            $last_name = '';
                            if (isset($name_array[1])) {
                                for ($i = 1; $i <= count($name_array) - 1; $i++) {
                                    if ($i == 1) {
                                        $last_name = $name_array[$i];
                                    } else {
                                        $last_name .= ' ' . $name_array[$i];
                                    }
                                }
                            }

                            $sql_base_in = "SELECT price FROM " . $wpdb->prefix . "ss_room_avail WHERE room = " . $trans->room_id . " AND start = '" . $trans->checkin . "' LIMIT 1";
                            $sql_base_out = "SELECT price FROM " . $wpdb->prefix . "ss_room_avail WHERE room = " . $trans->room_id . " AND start = '" . $trans->checkout . "' LIMIT 1";
                            $base_in      = $wpdb->get_row($sql_base_in);
                            $base_out     = $wpdb->get_row($sql_base_out);
                        ?>
                        <RoomRates MoreRatesExitInd="false">
                            <RoomRate RoomID="<?php echo $rooms[($trans->room_id)-1]->web_id; ?>" RatePlanCode="<?php echo $rooms[($trans->room_id)-1]->rate_plan_code;?>" NumberOfUnits="<?php echo $trans->qty; ?>">
                                <Rates>
                                    <?php 
                                      
                                        $query = "  SELECT * 
                                                    FROM wp_ss_room_avail a
                                                    WHERE a.room = {$rooms[($trans->room_id)-1]->web_id}
                                                    AND a.start >=  '{$trans->checkin}'
                                                    AND a.end < '{$trans->checkout}'
                                                ";

                                        $room_price = $wpdb->get_results($query,ARRAY_A);
                                        $room_price_list="<table><tr><td>Day</td><td>Price</td></tr>";?>
                                    <?php foreach ($room_price as $price){ ?>
                                        <Rate EffectiveDate="<?php echo date('D, d M Y', strtotime($price['start'])); ?>">
                                        <Base AmountBeforeTax="<?php echo $price['price']/(1.2); ?>" AmountAfterTax="<?php echo $price['price'] ?>" CurrencyCode="IDR"/>
                                        </Rate>
                                    <?php } ?>
                                </Rates>
                                <RoomRateDescription>
                                    <Text></Text>
                                </RoomRateDescription>
                            </RoomRate>
                        </RoomRates>

                        <TimeSpan Start="<?php echo $trans->checkin; ?>" End="<?php echo $trans->checkout; ?>"/>
                        <Total AmountBeforeTax="<?php echo $trans->amount/(1.2); ?>" AmountAfterTax="<?php echo $trans->amount; ?>"/>
                        <BasicPropertyInfo HotelCode="1" HotelName="EASTPARC"/>
                        <Comments>
                            <Comment>Room Name : <?php echo $get_room_name[($trans->room_id)-1]['room_name'] ?> </Comment>
                        </Comments>
                        <GuestCounts IsPerRoom="false">
                            <GuestCount AgeQualifyingCode="10" Count="<?php echo $trans->adult; ?>"/>
                            <GuestCount AgeQualifyingCode="8" Count="<?php echo $trans->child; ?>"/>
                        </GuestCounts>

                    </RoomStay>
              
                </RoomStays>

                <ResGuests>
                    <ResGuest>
                        <Profiles>
                            <ProfileInfo>
                                <Profile ProfileType="1">
                                    <Customer>
                                        <PersonName>
                                            <GivenName><?php echo $first_name; ?></GivenName>
                                            <Surname><?php echo $last_name; ?></Surname>
                                        </PersonName>
                                        <Telephone PhoneNumber="<?php echo $trans->phone; ?>"/>
                                        <Email><?php echo $trans->email; ?></Email>
                                        <Address>
                                            <CountryName>IDN</CountryName>
                                        </Address>
                                    </Customer>
                                </Profile>
                            </ProfileInfo>
                        </Profiles>
                    </ResGuest>
                </ResGuests>

                <ResGlobalInfo>
                    <HotelReservationIDs>
                        <HotelReservationID ResID_Type="1" ResID_Value="<?php echo $trans->trans_no; ?><?php if($count!=1)echo "_".$count; ?>" ResID_Source="EASTPARC" ResID_SourceContext="EastparcHotelProgram"/>
                    </HotelReservationIDs>
                    <Total CurrencyCode="IDR" AmountAfterTax="<?php echo $trans->amount; ?>" AmountBeforeTax="<?php echo $trans->amount/(1.2); ?>"/>
                </ResGlobalInfo>
            </HotelReservation>
        </ReservationsList>
        <?php $count++;endforeach; ?>
    <?php endforeach; ?>
    <?php
    foreach ($transactions_id2 as $transaction_id) :

        $sql2 = "SELECT * FROM " . $wpdb->prefix . "ss_books_trans_detail as d "
            . "LEFT JOIN " . $wpdb->prefix . "ss_books_credit_trans as t "
            . "ON d.trans_no = t.trans_no "
            . "WHERE t.trans_no = '". $transaction_id->trans_no ."' AND t.db_update_status = 0 AND t.status = 'paid' AND d.room_id != 0";
        $transactions2 = $wpdb->get_results($sql2);
        if ($_GET['sidebug']){
            echo "<pre>";
            print_r($transactions2);
            echo "</pre>";
        }
        $create_date = date('Y-m-d H:i:s e', strtotime($transactions2[0]->trans_date));  
        $count=1;
        ?>
        <?php foreach ($transactions2 as $trans):?>
        <ReservationsList>
            <HotelReservation CreateDateTime="<?php echo $create_date; ?>" ResStatus="Approved">
                <POS>
                    <Source>
                        <RequestorID Type="1" ID="eastparc_vhp"/>
                        <BookingChannel Type="1">
                            <CompanyName Code="VHP">Visual Hotel Program</CompanyName>
                        </BookingChannel>
                    </Source>
                </POS>

                <RoomStays>
                    
                    <RoomStay>
                        <?php
                            $create_date = date('Y-m-d H:i:s e', strtotime($trans->trans_date));
                            $name_array = explode(' ', $trans->name);

                            // Format customer name
                            $first_name = $name_array[0];
                            $last_name = '';
                            if (isset($name_array[1])) {
                                for ($i = 1; $i <= count($name_array) - 1; $i++) {
                                    if ($i == 1) {
                                        $last_name = $name_array[$i];
                                    } else {
                                        $last_name .= ' ' . $name_array[$i];
                                    }
                                }
                            }

                            $sql_base_in = "SELECT price FROM " . $wpdb->prefix . "ss_room_avail WHERE room = " . $trans->room_id . " AND start = '" . $trans->checkin . "' LIMIT 1";
                            $sql_base_out = "SELECT price FROM " . $wpdb->prefix . "ss_room_avail WHERE room = " . $trans->room_id . " AND start = '" . $trans->checkout . "' LIMIT 1";
                            $base_in      = $wpdb->get_row($sql_base_in);
                            $base_out     = $wpdb->get_row($sql_base_out);
                        ?>
                        <RoomRates MoreRatesExitInd="false">
                            <RoomRate RoomID="<?php echo $rooms[($trans->room_id)-1]->web_id; ?>" RatePlanCode="<?php echo $rooms[($trans->room_id)-1]->rate_plan_code;?>" NumberOfUnits="<?php echo $trans->qty; ?>">
                                <Rates>
                                    <?php 
                                      
                                        $query = "  SELECT * 
                                                    FROM wp_ss_room_avail a
                                                    WHERE a.room = {$rooms[($trans->room_id)-1]->web_id}
                                                    AND a.start >=  '{$trans->checkin}'
                                                    AND a.end < '{$trans->checkout}'
                                                ";

                                        $room_price = $wpdb->get_results($query,ARRAY_A);
                                        $room_price_list="<table><tr><td>Day</td><td>Price</td></tr>";?>
                                    <?php foreach ($room_price as $price){ ?>
                                        <Rate EffectiveDate="<?php echo date('D, d M Y', strtotime($price['start'])); ?>">
                                        <Base AmountBeforeTax="<?php echo $price['price']/(1.2); ?>" AmountAfterTax="<?php echo $price['price']; ?>" CurrencyCode="IDR"/>
                                        </Rate>
                                    <?php } ?>
                                </Rates>
                                <RoomRateDescription>
                                    <Text></Text>
                                </RoomRateDescription>
                            </RoomRate>
                        </RoomRates>

                        <TimeSpan Start="<?php echo $trans->checkin; ?>" End="<?php echo $trans->checkout; ?>"/>
                        <Total AmountBeforeTax="<?php echo $trans->amount/(1.2); ?>" AmountAfterTax="<?php echo $trans->amount; ?>"/>
                        <BasicPropertyInfo HotelCode="1" HotelName="EASTPARC"/>
                        <Comments>
                            <Comment>Room Name : <?php echo $get_room_name[($trans->room_id)-1]['room_name'] ?> </Comment>
                        </Comments>
                        <GuestCounts IsPerRoom="false">
                            <GuestCount AgeQualifyingCode="10" Count="<?php echo $trans->adult; ?>"/>
                            <GuestCount AgeQualifyingCode="8" Count="<?php echo $trans->child; ?>"/>
                        </GuestCounts>

                    </RoomStay>
               
                </RoomStays>

                <ResGuests>
                    <ResGuest>
                        <Profiles>
                            <ProfileInfo>
                                <Profile ProfileType="1">
                                    <Customer>
                                        <PersonName>
                                            <GivenName><?php echo $first_name; ?></GivenName>
                                            <Surname><?php echo $last_name; ?></Surname>
                                        </PersonName>
                                        <Telephone PhoneNumber="<?php echo $trans->phone; ?>"/>
                                        <Email><?php echo $trans->email; ?></Email>
                                        <Address>
                                            <CountryName>IDN</CountryName>
                                        </Address>
                                    </Customer>
                                </Profile>
                            </ProfileInfo>
                        </Profiles>
                    </ResGuest>
                </ResGuests>

                <ResGlobalInfo>
                    <HotelReservationIDs>
                        <HotelReservationID ResID_Type="1" ResID_Value="<?php echo $trans->trans_no; ?><?php if($count!=1)echo "_".$count; ?>" ResID_Source="EASTPARC" ResID_SourceContext="EastparcHotelProgram"/>
                    </HotelReservationIDs>
                    <Total CurrencyCode="IDR" AmountAfterTax="<?php echo $trans->amount; ?>" AmountBeforeTax="<?php echo $trans->amount/(1.2); ?>"/>
                </ResGlobalInfo>
            </HotelReservation>
        </ReservationsList>
         <?php $count++; endforeach; ?>
    <?php endforeach; ?>

     <?php $sql3 = "SELECT * FROM wp_indohotels_trans as d WHERE d.trans_db_update_stat = 0 AND d.trans_status = 2 AND d.trans_room_id != 0"; ?>
   <?php $transactions3 = $wpdb->get_results($sql3); ?>
   <?php
        if ($_GET['sidebug'])
        {
            echo "<pre>";
            print_r($transactions3);
            echo "</pre>";
        }
        ?>
        <?php foreach ( $transactions3 as $trans ) : ?>
        <?php $create_date = date('Y-m-d H:i:s e', strtotime( $trans->trans_date ) ); ?>
        <ReservationsList>
            <HotelReservation CreateDateTime="<?php echo $create_date; ?>" ResStatus="Approved">
                <POS>
                    <Source>
                        <RequestorID Type="1" ID="eastparc_vhp"/>
                        <BookingChannel Type="1">
                            <CompanyName Code="VHP">Visual Hotel Program</CompanyName>
                        </BookingChannel>
                    </Source>
                </POS>
                <RoomStays>
                    <RoomStay>
                        <RoomRates MoreRatesExitInd="false">
                            <RoomRate RoomID="<?php echo $trans->trans_room_id; ?>" RatePlanCode="<?php echo $trans->trans_rate_plan; ?>" NumberOfUnits="<?php echo $trans->trans_room_qty; ?>">
                                <Rates>
                                    <?php $price_each_day = json_decode($trans->trans_cookie, true); ?>
                                    <?php foreach ($price_each_day as $date => $price) { ?>
                                        <Rate EffectiveDate="<?php echo date('D, d M Y', $date+25200 ); ?>">
                                        <Base AmountBeforeTax="<?php echo $price*100/121; ?>" AmountAfterTax="<?php echo $price; ?>" CurrencyCode="IDR"/>
                                        </Rate>
                                    <?php } ?>
                                </Rates>
                                <RoomRateDescription>
                                    <Text></Text>
                                </RoomRateDescription>
                            </RoomRate>
                        </RoomRates>

                        <TimeSpan Start="<?php echo $trans->trans_checkin; ?>" End="<?php echo $trans->trans_checkout; ?>"/>
                        <Total AmountBeforeTax="<?php echo $trans->trans_total_amount*100/121; ?>" AmountAfterTax="<?php echo $trans->trans_total_amount; ?>"/>
                        <BasicPropertyInfo HotelCode="1" HotelName="EASTPARC"/>
                        <Comments>
                            <Comment>Room Name : <?php echo $trans->trans_room_name; ?> </Comment>
                        </Comments>
                        <GuestCounts IsPerRoom="false">
                            <GuestCount AgeQualifyingCode="10" Count="<?php echo $trans->trans_adults; ?>"/>
                            <GuestCount AgeQualifyingCode="8" Count="<?php echo $trans->trans_children; ?>"/>
                        </GuestCounts>

                    </RoomStay>

                </RoomStays>

                <ResGuests>
                    <ResGuest>
                        <Profiles>
                            <ProfileInfo>
                                <Profile ProfileType="1">
                                    <Customer>
                                        <PersonName>
                                            <GivenName><?php echo $trans->trans_first_name; ?></GivenName>
                                            <Surname><?php echo isset($trans->trans_last_name)? $trans->trans_last_name : '.'; ?></Surname>
                                        </PersonName>
                                        <Telephone PhoneNumber="<?php echo $trans->trans_phone; ?>"/>
                                        <Email><?php echo $trans->trans_email; ?></Email>
                                        <Address>
                                            <CountryName>IDN</CountryName>
                                        </Address>
                                    </Customer>
                                </Profile>
                            </ProfileInfo>
                        </Profiles>
                    </ResGuest>
                </ResGuests>

                <ResGlobalInfo>
                    <HotelReservationIDs>
                        <HotelReservationID ResID_Type="1" ResID_Value="<?php echo $trans->trans_no; ?>" ResID_Source="EASTPARC" ResID_SourceContext="EastparcHotelProgram"/>
                    </HotelReservationIDs>
                    <Total CurrencyCode="IDR" AmountAfterTax="<?php echo $trans->trans_total_amount; ?>" AmountBeforeTax="<?php echo $trans->trans_total_amount*100/121; ?>"/>
                </ResGlobalInfo>
            </HotelReservation>
        </ReservationsList>
         <?php $count++; endforeach; ?>
         <?php $sql4 = "SELECT * FROM wp_indohotels_trans as d WHERE d.trans_db_update_stat = 0 AND d.trans_status = 10 AND d.trans_room_id != 0"; ?>
        <?php $transactions4 = $wpdb->get_results($sql4); ?>
         <?php
        if ($_GET['sidebug'])
        {
            echo "<pre>";
            print_r($transactions3);
            echo "</pre>";
        }
        ?>
        <?php foreach ( $transactions4 as $trans ) : ?>
        <?php $create_date = date('Y-m-d H:i:s e', strtotime( $trans->trans_date ) ); ?>
        <ReservationsList>
            <HotelReservation CreateDateTime="<?php echo $create_date; ?>" ResStatus="Approved">
                <POS>
                    <Source>
                        <RequestorID Type="1" ID="eastparc_vhp"/>
                        <BookingChannel Type="1">
                            <CompanyName Code="VHP">Visual Hotel Program</CompanyName>
                        </BookingChannel>
                    </Source>
                </POS>
                <RoomStays>
                    <RoomStay>
                        <RoomRates MoreRatesExitInd="false">
                            <RoomRate RoomID="<?php echo $trans->trans_room_id; ?>" RatePlanCode="<?php echo $trans->trans_rate_plan; ?>" NumberOfUnits="<?php echo $trans->trans_room_qty; ?>">
                                <Rates>
                                    <?php $price_each_day = json_decode($trans->trans_cookie, true); ?>
                                    <?php foreach ($price_each_day as $date => $price) { ?>
                                        <Rate EffectiveDate="<?php echo date('D, d M Y', $date+25200 ); ?>">
                                        <Base AmountBeforeTax="<?php echo $price*100/121; ?>" AmountAfterTax="<?php echo $price; ?>" CurrencyCode="IDR"/>
                                        </Rate>
                                    <?php } ?>
                                </Rates>
                                <RoomRateDescription>
                                    <Text></Text>
                                </RoomRateDescription>
                            </RoomRate>
                        </RoomRates>

                        <TimeSpan Start="<?php echo $trans->trans_checkin; ?>" End="<?php echo $trans->trans_checkout; ?>"/>
                        <Total AmountBeforeTax="<?php echo $trans->trans_total_amount*100/121; ?>" AmountAfterTax="<?php echo $trans->trans_total_amount; ?>"/>
                        <BasicPropertyInfo HotelCode="1" HotelName="EASTPARC"/>
                        <Comments>
                            <Comment>Lucky Draw Room Name : <?php echo $trans->trans_room_name; ?> </Comment>
                        </Comments>
                        <GuestCounts IsPerRoom="false">
                            <GuestCount AgeQualifyingCode="10" Count="<?php echo $trans->trans_adults; ?>"/>
                            <GuestCount AgeQualifyingCode="8" Count="<?php echo $trans->trans_children; ?>"/>
                        </GuestCounts>

                    </RoomStay>

                </RoomStays>

                <ResGuests>
                    <ResGuest>
                        <Profiles>
                            <ProfileInfo>
                                <Profile ProfileType="1">
                                    <Customer>
                                        <PersonName>
                                            <GivenName><?php echo $trans->trans_first_name; ?></GivenName>
                                            <Surname><?php echo isset($trans->trans_last_name)? $trans->trans_last_name : '.'; ?></Surname>
                                        </PersonName>
                                        <Telephone PhoneNumber="<?php echo $trans->trans_phone; ?>"/>
                                        <Email><?php echo $trans->trans_email; ?></Email>
                                        <Address>
                                            <CountryName>IDN</CountryName>
                                        </Address>
                                    </Customer>
                                </Profile>
                            </ProfileInfo>
                        </Profiles>
                    </ResGuest>
                </ResGuests>

                <ResGlobalInfo>
                    <HotelReservationIDs>
                        <HotelReservationID ResID_Type="1" ResID_Value="<?php echo $trans->trans_no; ?>" ResID_Source="EASTPARC" ResID_SourceContext="EastparcHotelProgram"/>
                    </HotelReservationIDs>
                    <Total CurrencyCode="IDR" AmountAfterTax="<?php echo $trans->trans_total_amount; ?>" AmountBeforeTax="<?php echo $trans->trans_total_amount*100/121; ?>"/>
                </ResGlobalInfo>
            </HotelReservation>
        </ReservationsList>
         <?php $count++; endforeach; ?>
</OTA_ResRetrieveRS>