<?php get_header(); ?>

  <div id="wrapper" class="page detail-facility">
  <?php if (have_posts()): while (have_posts()) : the_post(); ?>
    <div class="section facility-detail">
      <div class="slider-for bigslide">

        <?php 
        $images = rwmb_meta( 'indohotels_imgPhotoGallery', 'size=gallery-slide' ); // Since 4.8.0
        if ( !empty( $images ) ) {
            foreach ( $images as $image ) {
        ?>
            <div class="item">
                <span>
                    <img data-lazy="<?php echo $image['full_url']; ?>" class="img-responsive" />
                </span>
            </div>
        <?php
            }
        }
        ?>
      </div>

      <div class="container slideshow">
        <div class="slider-nav global-thumbimg">
            <?php 
            $images = rwmb_meta( 'indohotels_imgPhotoGallery', 'size=gallery-slide' ); // Since 4.8.0
            if ( !empty( $images ) ) {
                foreach ( $images as $image ) {
            ?>
                <div class="item">
                    <span>
                        <img src="<?php echo $image['full_url']; ?>" class="img-responsive" />
                    </span>
                </div>
            <?php
                }
            }
            ?>
              
            </div><!-- end .slider-nav -->
      </div><!-- end .container -->
    </div>
    <?php endwhile; ?>

			<?php else: ?>

				<!-- article -->
				<article>

					<h1><?php pll_e( 'Sorry, nothing to display.', karisma_text_domain ); ?></h1>

				</article>
				<!-- /article -->

			<?php endif; ?>
  </div><!-- end .content -->

  <?php get_footer(); ?>