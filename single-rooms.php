<?php get_header(); ?>
<div id="wrapper" class="page">
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
      <div class="section main-slider slider-room">
        <div id="slider-main" class="owl-carousel">
          <?php
          $images = rwmb_meta('indohotels_imgadv', 'size=gallery-slide'); // Since 4.8.0
          if (!empty($images)) {
            foreach ($images as $image) {
              echo '<div class="owl-slide" style="background-image: url(\'' . $image['full_url'] . '\')"></div>';
            }
          }
          ?>
        </div>
        <!-- end .slider-main -->
      </div>
      <!-- end .main-slider -->
      <div class="section content-rooms room-detail">
        <div class="container">
          <h1 class="heading-title">
            <?php the_title(); ?>
          </h1>
          <p class="subtitle"><?php pll_e(rwmb_meta('indohotels_room_balcony'), karisma_text_domain); ?></p>


          <div class="row margin-blarge">
            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="box-schedule">
                <ul class="clearfix">
                  <?php if (!empty(rwmb_meta('bed_size'))) : ?>
                    <li>
                      <span class="scleft"><?php pll_e('Bed Size', karisma_text_domain); ?></span>
                      <span class="dot">:</span>
                      <span class="scright"><?php echo rwmb_meta('bed_size'); ?></span>
                    </li>
                  <?php endif; ?>
                  <?php if (!empty(rwmb_meta('room_occupancy'))) : ?>
                    <li>
                      <span class="scleft"><?php pll_e('Occupancy', karisma_text_domain); ?></span>
                      <span class="dot">:</span>
                      <span class="scright"><?php echo rwmb_meta('room_occupancy'); ?> <?php pll_e('Persons', karisma_text_domain); ?></span>
                    </li>
                  <?php endif; ?>
                  <?php if (!empty(rwmb_meta('room_size'))) : ?>
                    <li>
                      <span class="scleft"><?php pll_e('Room Size', karisma_text_domain); ?></span>
                      <span class="dot">:</span>
                      <span class="scright"><?php echo rwmb_meta('room_size'); ?> m<sup>2</sup></span>
                    </li>
                  <?php endif; ?>
                  <?php if (!empty(rwmb_meta('room_view'))) : ?>
                    <li>
                      <span class="scleft"><?php pll_e('View', karisma_text_domain); ?></span>
                      <span class="dot">:</span>
                      <span class="scright"><?php echo rwmb_meta('room_view'); ?></span>
                    </li>
                  <?php endif; ?>
                </ul>
              </div>
              <!-- end .box-schedule -->
            </div>
            <!-- end .col-md-4 -->
            <div class="col-md-8 col-sm-8 col-xs-12">
              <?php the_content(); // Dynamic Content 
              ?>
            </div>
            <!-- end .col-md-4 -->
          </div>
          <!-- end .row -->
        </div>
        <!-- end .container -->
      </div>
      <!-- end .content-intro -->

      <div class="main-amenities">
        <h4 class="text-center">
          <?php pll_e('In-Room Amenities', karisma_text_domain); ?>
        </h4>
        <div class="amenities-box">
          <div class="container">
            <div class="row">
              <div class="col-md-4 col-sm-4 col-xs-12">
                <p><strong><?php pll_e('For Your Convenience', karisma_text_domain); ?></strong></p>
                <ul>
                  <?php
                  $values = rwmb_meta('room_convenience');
                  foreach ($values as $value) {
                    echo '<li>' . $value . '</li>';
                  }
                  ?>
                </ul>
              </div>
              <!-- end .col-md-4 -->

              <div class="col-md-4 col-sm-4 col-xs-12">
                <p><strong><?php pll_e('For Your Comfort', karisma_text_domain); ?></strong></p>
                <ul>
                  <?php
                  $values = rwmb_meta('room_comfort');
                  foreach ($values as $value) {
                    echo '<li>' . $value . '</li>';
                  }
                  ?>
                </ul>
              </div>
              <!-- end .col-md-4 -->

              <div class="col-md-4 col-sm-4 col-xs-12">
                <p><strong><?php pll_e('For Your Indulgence', karisma_text_domain); ?></strong></p>
                <ul>
                  <?php
                  $values = rwmb_meta('room_indulgence');
                  foreach ($values as $value) {
                    echo '<li>' . $value . '</li>';
                  }
                  ?>
                </ul>
              </div>
              <!-- end .col-md-4 -->
            </div>
            <!-- end .row -->
          </div>
          <!-- end .container -->
        </div>
        <!-- end .amenities-box -->

        <?php if (rwmb_meta('indohotels_extra_title')) : ?>
          <div class="helpful-fact">
            <h4 class="text-center">
              <?php pll_e(rwmb_meta('indohotels_extra_title'), karisma_text_domain); ?>
            </h4>
            <div class="fact-box">
              <?php pll_e(rwmb_meta('indohotels_extra_text'), karisma_text_domain); ?>
            </div>
            <!-- end .fact-box -->
          </div>
          <!-- end .helpful-fact -->
        <?php endif; ?>

        <div class="double-option">
          <!-- <a href="https://booking.eastparchotel.com/website/property/eastparc-hotel-yogyakarta" class="nbutton">
        <?php //pll_e('Book Now', karisma_text_domain); 
        ?>
      </a> -->
          <?php
          $startDate = $_REQUEST['start'] ? $_REQUEST['start'] : date('d-m-Y');
          $endDate = $_REQUEST['end'] ? $_REQUEST['end'] : date('d-m-Y', strtotime("+1 day"));
          $room_book_switch = get_post_meta(get_the_ID(), '_krs_room_book_switch_meta_key', true);
          $direct_url = get_post_meta(get_the_ID(), '_krs_button_direct_url_meta_key', true);
          $direct_text = get_post_meta(get_the_ID(), '_krs_button_direct_text_meta_key', true);

          $interval = date_diff(date_create($startDate), date_create($endDate));
          $night = $interval->format('%a');
          $room = 1;
          $price = krs_get_room_price(get_the_ID(), date('Y-m-d', strtotime($startDate)), date('Y-m-d', strtotime($endDate)), 1, 'search');

          if (($room_book_switch == '1' || empty($direct_url)) && $price) {
            $target = '';
            $url = '/book-room/?krs_rooms=' . get_the_ID() . '&start=' . $startDate . '&end=' . $endDate . '&night=' . $night . '&room=' . $room;
            $text = 'Book Now';
          } else {
            $target = 'target="_blank"';
            $url = $direct_url;
            $text = $direct_text;
          }
          ?>
          <?php if ($url) : ?>
            <a <?php echo $target; ?> href="<?php echo $url; ?>" class="nbutton">
              <?php pll_e($text, karisma_text_domain); ?>
            </a>
          <?php endif; ?>
        </div>
        <!-- end .double-option -->
      </div>
      <!-- end .main-amenities -->
    <?php endwhile; ?>

  <?php else : ?>

    <!-- article -->
    <article>

      <h1>
        <?php pll_e('Sorry, nothing to display.', karisma_text_domain); ?>
      </h1>

    </article>
    <!-- /article -->

  <?php endif; ?>
</div>
<!-- end .content -->

<?php get_footer(); ?>