<footer class="main-footer">
  <div class="footer-link">
    <div class="container">
      <?php karisma_nav_footer(); ?>
    </div> <!-- end .container -->
  </div>
  <!-- end .footer-content -->
  <div class="footer-info">
    <div class="container">
      <div class="row">
        <div class="col-md-4 info-socmed">
          <?php krs_sn(); ?>
        </div>
        <!-- end .col-md-4 -->
        <div class="col-md-8">
          <div class="verticent">
            <p class="verticent-inner">
              <?php echo ot_get_option('krs_footcredits'); ?>
            </p>
          </div>
        </div>
        <!-- end .col-md-8 -->
      </div>
      <!-- end .row -->
    </div>
    <!-- end .container -->
  </div>
  <!-- end .footer-info -->
</footer>


<?php wp_footer(); ?>

<script>
  // jQuery(document).ready(function() {
  // 	jQuery('.gallery-deals').magnificPopup({
  // 		delegate: 'a',
  // 		type: 'image',
  // 		tLoading: 'Loading image #%curr%...',
  // 		mainClass: 'mfp-img-mobile',
  // 		gallery: {
  // 			enabled: true,
  // 			navigateByImgClick: true,
  // 			preload: [0,1] // Will preload 0 - before current, and 1 after the current image
  // 		},
  // 		image: {
  // 			tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
  // 			titleSrc: function(item) {
  // 				return item.el.attr('title') + '<small></small>';
  // 			}
  // 		}
  // 	});
  // });
</script>
<script>
  jQuery('#videoleft').magnificPopup({
    items: {
      src: "<?php echo ot_get_option('krs_video_slide_1'); ?>"
    },
    type: 'iframe',
    iframe: {
      markup: '<div class="mfp-iframe-scaler">' +
        '<div class="mfp-close"></div>' +
        '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>' +
        '</div>',
      patterns: {
        youtube: {
          index: 'youtube.com/',
          id: 'v=',
          src: '//www.youtube.com/embed/%id%?autoplay=1'
        }
      },
      srcAction: 'iframe_src',
    }
  });
</script>
<script>
  jQuery('#videoright').magnificPopup({
    items: {
      src: "<?php echo ot_get_option('krs_video_slide_2'); ?>"
    },
    type: 'iframe',
    iframe: {
      markup: '<div class="mfp-iframe-scaler">' +
        '<div class="mfp-close"></div>' +
        '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>' +
        '</div>',
      patterns: {
        youtube: {
          index: 'youtube.com/',
          id: 'v=',
          src: '//www.youtube.com/embed/%id%?autoplay=1'
        }
      },
      srcAction: 'iframe_src',
    }
  });
</script>


<?php if (!isset($_COOKIE["popup"])) : ?>
  <?php if (ot_get_option('popup_ads_actived') == 'on') : ?>
    <div class="intro-popup white-popup homepop">
      <div class="intro-content hidden-xs">
        <img src="<?php echo ot_get_option('popup_ads_desktop'); ?>" alt="" class="img-responsive" />
      </div>
      <!-- end .intro-content -->
    <?php endif; ?>
    <?php if (ot_get_option('popup_ads_mobile_actived') == 'on') : ?>
      <div class="intro-contentmb hidden-sm hidden-md hidden-lg">
        <img src="<?php echo ot_get_option('popup_ads_mobile'); ?>" alt="" class="img-responsive" />
      </div>
      <!-- end .intro-content -->
    </div>
    <!-- end #intro-popup -->
  <?php endif; ?>
<?php endif; ?>
</body>

</html>