<?php /* Template Name: book result */ ?>

<?php global $wpdb; ?>
<?php get_header(); ?>
<?php 
    global $ss_theme_opt; 
    
    //-----------PAGE HEADER-------------------
    if($ss_theme_opt['page-header-use-row-class']){
        $page_header_div_class = ' row';
    }
    
    if(isset($ss_theme_opt['page-header-background-color'])){
        $page_header_style = "background-color: ".$ss_theme_opt['page-header-background-color']."";
    }

    if(isset($ss_theme_opt['page-header-background']['url']) && $ss_theme_opt['page-header-background']['url'] != ''){
        $page_header_style = "background-image: url('".$ss_theme_opt['page-header-background']['url']."')";
    }

    if($ss_theme_opt['page-header-use-featured-image']){
        if(has_post_thumbnail()){
            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
            $page_header_style = "background-image: url('".$image[0]."')";
        }
    }

    $page_header_custom_class = $ss_theme_opt['page-header-custom-class'];
    if($ss_theme_opt['page-header-overlay']){
        $page_header_custom_class .= ' overlay-bg';
    }
    if($ss_theme_opt['page-header-full-width']){
        $page_header_custom_class .= ' full-width';
    }


    //-----------PAGE FOOTER-------------------
    if($ss_theme_opt['page-footer-use-row-class']){
        $page_footer_div_class = ' row';
    }
    
    if(isset($ss_theme_opt['page-footer-background-color'])){
        $page_footer_style = "background-color: ".$ss_theme_opt['page-footer-background-color']."";
    }

    if(isset($ss_theme_opt['page-footer-background']['url']) && $ss_theme_opt['page-footer-background']['url'] != ''){
        $page_footer_style = "background-image: url('".$ss_theme_opt['page-footer-background']['url']."')";
    }

    $page_footer_custom_class = $ss_theme_opt['page-footer-custom-class'];
    if($ss_theme_opt['page-footer-overlay']){
        $page_footer_custom_class .= ' overlay-bg';
    }
    if($ss_theme_opt['page-footer-full-width']){
        $page_footer_custom_class .= ' full-width';
    }


    //---------SIDEBAR-----------------------
    $show_sidebar = true;

    //check if sidebar only show on certain page based on theme option
    if($ss_theme_opt['page-sidebar-page-id'] != ""){
        //get page list as array and remove empty space
        $str_page = str_replace(' ','',$ss_theme_opt['page-sidebar-page-id']);
        $arr_page = explode(',',$str_page); //convert to array

        if(!is_page($arr_page)){
            $show_sidebar = false;
        }
    }

    //---------CONTENT------------------------
    //prepare class for content
    $page_content_class = array();
    if(!empty($ss_theme_opt['page-content-custom-class'])){
        $page_content_class[] = $ss_theme_opt['page-content-custom-class'];
    }
    
    if($show_sidebar && is_active_sidebar( 'page-sidebar' ) ){
        $page_content_class[] = $ss_theme_opt['page-content-custom-class-sidebar-shown'];
    }

    $str_page_content_class = implode(' ',$page_content_class);

?>


    <?php if ( is_active_sidebar( 'page-header' ) ) { ?>
        <section id="section-page-header" class="theme-section <?php echo $page_header_custom_class; ?>" style="<?php echo $page_header_style; ?>" >
            <div class="container">
                <div id="page-header" class="header-wrapper<?php echo $page_header_div_class; ?>">
                    <?php dynamic_sidebar( 'page-header' ); ?>
                </div>
            </div>
        </section>
    <?php } ?>



    <div id="content" class="page">

        <div class="container">

            <div class="row">

            <?php 
                global $wpdb;
                $success_id = $_GET["booking_number"];
                $query = "  
                        SELECT * 
                        FROM 
                            wp_ss_books_trans t,
                            wp_ss_books_trans_detail d
                        WHERE 
                            t.trans_no = d.trans_no AND 
                            t.trans_no='".$success_id."'
                        ";
                $transactions  = $wpdb->get_results($query,ARRAY_A);
                $room_q ="SELECT * FROM wp_ss_room";
                $room = $wpdb->get_results($room_q,ARRAY_A);

                if(isset($_POST['credit-payment'])){
                    $card_detail['books_card_transno']=$success_id;
                    $card_detail['books_card_type']=$_POST['card_type'];
                    $card_detail['books_card_holder']=$_POST['card_holder'];
                    $card_detail['books_card_number']=$_POST['card_number1'].$_POST['card_number2'].$_POST['card_number3'].$_POST['card_number4'];
                    $card_detail['books_card_expired']= $_POST['card_expired'];
                    $wpdb->query(sql_new($wpdb->prefix.'ss_books_credit_card', $card_detail));
                }

                // echo "<pre>";
                // print_r($_POST);
                //                 echo "</pre>";


                if ($transactions[0]){
                    
                     $header  = "MIME-Version: 1.0\r\n";
                    $header .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
                    $msg = '
                            <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
                            <html xmlns="http://www.w3.org/1999/xhtml">
                            <head></head>
                            <body>
                            ';
                    $msg .= '<div class="header" style="text-align:center;font-size:18px;">
                                <container>
                                    <row class="collapse">
                                        <columns small="6">
                                            <img width="250px" src="http://demosite.softwareseni.com/eastparc/wp-content/uploads/2016/05/simple-logo.png"> 
                                        </columns>
                                    </row>
                                </container>
                            </div>
                            ';

                    $msg .= '<container>
                                <spacer size="16"></spacer>
                                <row>
                                    <columns small="12">
                                        <h1 style="text-align: center;color: #717275;font-size:18px !important;">  Dear '.$transactions[0]["name"].' </h1>
                            ';
                    $msg .= '           <p class="lead" style="text-align: center;color: #fff;background-color: #be494c;padding: 15px;"> Thank you for choosing Eastparc Hotel Yogyakarta. </p>'; 
                    $msg .= '           <p style="color: #727376;">Further to your request, we are pleased to serve you at the hotel to finish your transaction: </p>';
                    $msg .= '           <p style="margin-top: 15px;color: #000000;border-top: 1px solid #be494c;padding-top: 40px;">Confirmation No : '.$success_id.'</p>
                                        <callout class="primary">
                        ';
                    $current_room=0;    
                    foreach ( $transactions as $transaction) {       
                        $get_room = "SELECT web_id
                                     FROM wp_ss_room
                                     WHERE id={$transaction['room_id']}
                                    ";
                        $web_id_room = $wpdb->get_results($get_room,ARRAY_A);
                        if(isset($_GET['debug'])){
                            // echo "<pre>";
                            // print_r($web_id_room);
                            // echo "</pre>";
                        }
                        $query = "  SELECT start,price 
                                    FROM wp_ss_room_avail a
                                    WHERE a.room = {$web_id_room[0]['web_id']}
                                    AND a.start >=  '{$transaction['checkin']}'
                                    AND a.end < '{$transaction['checkout']}'
                                ";
                        $room_price = $wpdb->get_results($query,ARRAY_A);
                        if(isset($_GET['debug'])){
                            // echo "<pre>";
                            // print_r($room_price);
                            // echo "</pre>";
                        }
 
                        /*$msg .= "<p style='color:#000000;'>Guest Name: ".$transaction['name']."</p>";
                        $msg .= "<p style='color:#000000;border-bottom:1px solid #be494c;padding-bottom: 40px;margin-bottom: 20px;'>Room Type : ( ".$transaction['qty']." room(s) ) ".$room[$transaction['room_id']-1]['room_type']." (  ".$transaction['checkin']."  -  ".$transaction['checkout']."  ) - Rp. ".number_format($transaction['amount'],0, '.', '.')."- </p>";*/

                        // if(isset($_GET['debug'])){                        
                            $msg .= "<table >";
                            $msg .= "<tr><td style='padding: 5px 5px 5px 0;'>Guest Name</td><td style='padding: 5px 5px 5px 0;'>:</td><td style='padding: 5px 5px 5px 0;'>".$transaction['name']."</td></tr>";
                            $msg .= "<tr><td style='padding: 5px 5px 5px 0;'>Room Type</td><td style='padding: 5px 5px 5px 0;'>:</td><td style='padding: 5px 5px 5px 0;'>".$room[$transaction['room_id']-1]['room_type']."</td></tr>";
                            $msg .= "</table>";
                            $msg .= "<table style='margin-top: 40px;margin-bottom: 40px;'>";
                            $msg .= "<tr><td style='padding: 5px;text-align: center;border: 1px solid #be494c;color: white;background-color: #be494c;'>Date</td><td style='color: white;background-color: #be494c;padding: 5px;text-align: center;border: 1px solid #be494c;'>Rates</td><td style='color: white;background-color: #be494c;padding: 5px;text-align: center;border: 1px solid #be494c;'>Qty</td><td style='padding: 5px;text-align: center;border: 1px solid #be494c;color: white;background-color: #be494c;'>total</td></tr>";
                            foreach ($room_price as $r_price) {
                                $msg .= "<tr><td style='padding: 5px;border: 1px solid #be494c;'>".date('F d, Y',strtotime($r_price['start']))."</td><td style='padding: 5px;border: 1px solid #be494c;'>".$r_price['price']."</td><td style='padding: 5px;border: 1px solid #be494c;'>".$transaction['qty']."</td><td style='padding: 5px;border: 1px solid #be494c;'>Rp. ".number_format($transaction['qty']*$r_price['price'])."- </td></tr>";
                            }
                            $msg .= "<tr><td style='padding: 5px;border: 1px solid #be494c;color: white;background-color: #be494c;' colspan=3>Total</td><td style='padding: 5px;border: 1px solid #be494c;'> Rp. ".number_format($transaction['amount'],0, '.', '.')."- </td></tr>";
                            $msg .= "</table></p>";
                        //  }
                        $current_room++;
                    }
                    $msg .= '           </callout> 
                                    </columns>
                                </row>
                                <wrapper class="secondary">
                                    <spacer size="16"></spacer>
                                    <row>
                                        <p style="color: #be494c;">- Please note that check in time is 14:00 pm and check out time is 12.00 pm </p>
                                        <p style="color: #717275;margin-top: 25px;">We are looking forward to welcoming your guest at Eastparc Hotel Yogyakarta. </p>
                                        <p style="color: #717275;">Should you require any further assistance, please do not hesitate to contact us. </p>
                                    </row>
                                </wrapper>
                            </container>
                            </body></html>
                    ';

                    if(isset($_GET['debug'])){
                        echo $msg;
                        echo "<pre>";
                        $admin_email4= "sidiq@softwareseni.com";
                        // mail($admin_email4,"Eastparc Room Booking - reservation notif",$msg,$header);
                        die();
                        echo "</pre>";
                    }
                    $admin_email= get_option( 'admin_email' );
                    $admin_email= "itm@eastparchotel.com";
                    $admin_email1= "reservation@eastparchotel.com";
                    $admin_email2= "ecommerce@eastparchotel.com";
                    $admin_email3= "gss@eastparchotel.com";
                    $admin_email4= "sidiq@softwareseni.com";
                    $admin_email5= "alfian@softwareseni.com";
                    $adm_msg = $msg;
                   
                    foreach ( $transactions as $transaction) { 
                        if ($transaction['db_sent_notif'] != 1) {
                            
                            $adm_msg .= '<table style="padding-bottom: 40px;border: 1px solid #be494c;border-bottom: 10px solid #be494c;margin-bottom: 25px;color: #000;background-color: #ffffff;padding: 20px;">';
                            $adm_msg .= '<tr><td>Customer email :</td><td> '.$transaction["email"].'</td></p></tr>';
                            $adm_msg .= '<tr><td>Customer phone :</td><td> '.$transaction["phone"].'</td></p></tr>';
                            $adm_msg .= '<tr><td>Customer Request :</td><td> '.$transaction["request"].'</td></p></tr>';
                            if(isset($_POST['credit-payment'])){
                                $adm_msg .= '<tr><td>Card type :</td><td>'.$_POST['card_type'].'</td></p></tr>';
                                $adm_msg .= '<tr><td>Holder name :</td><td>'.$_POST['card_holder'].'</td></p></tr>';
                                $adm_msg .= '<tr><td>Card Number :</td><td>'.$_POST['card_number1'].$_POST['card_number2'].$_POST['card_number3'].$_POST['card_number4'].'</td></p></tr>';
                                $adm_msg .= '<tr><td>Expired date :</td><td>'.$_POST['card_expired'].'</td></p></tr>';
                                $adm_msg .= '</table>';
                            }
                            $msg_user = $msg;
                            $msg_user.="</body></html>";
                            mail($transaction['email'],"Eastparc Room Booking",$msg_user,$header);
                        }
                    }
                    $adm_msg .= "</body></html>";
                    mail($admin_email1,"Eastparc Room Booking - reservation notif",$adm_msg,$header);
                    mail($admin_email2,"Eastparc Room Booking - reservation notif",$adm_msg,$header);
                    mail($admin_email3,"Eastparc Room Booking - reservation notif",$adm_msg,$header);
                    mail($admin_email4,"Eastparc Room Booking - reservation notif",$adm_msg,$header);
                    mail($admin_email5,"Eastparc Room Booking - reservation notif",$adm_msg,$header);
                    $update['db_sent_notif'] = 1;
                    $wpdb->query(sql_update($wpdb->prefix.'ss_books_trans', 'trans_no', $success_id, $update));


                    ?>
                    <div class="col-md-12 text-center">
                        <center><img src="http://demosite.softwareseni.com/eastparc/wp-content/uploads/2016/05/simple-logo.png"></center>                    
                    </div>
                    <p>Dear <?php echo $transaction['name']; ?></p>
                    <p>Thank you for choosing Eastparc Hotel Yogyakarta.</p>
                    <p>We will get in touch with you shortly to finish your transaction. </p>
                   
                    <p>Should you require any further assistance, please do not hesitate to contact us.</p>
               
        <?php
        if (isset($_SERVER['HTTP_COOKIE'])) {
            $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
            foreach($cookies as $cookie) {
                $parts = explode('=', $cookie);
                $name = trim($parts[0]);
                setcookie($name, '', time()-1000);
                setcookie($name, '', time()-1000, '/');
            }
        }
        } else { ?>
        <div>
        <h3>Sorry your transaction failed </h3>
        <p>
        <?php if($transactions[0]['message'])
            echo $transactions[0]['message'];
        ?>  
        </p>
        <p>Please try again or contact our customer service</p>
        </div>
        <div class="clear"></div>
    <?php } ?>


                    <!--  <div class="BCAKlikPay"><img class="col-md-3" src="<?php echo plugins_url();?>/ss-books/images/BCAKlikPay.jpg"></div> -->
                </div>
            </div>    
        </div>
    <?php if ( is_active_sidebar( 'page-footer' ) ) { ?>
        <section id="section-page-footer" class="theme-section <?php echo $page_footer_custom_class; ?>" style="<?php echo $page_footer_style; ?>" >
            <div class="container">
                <div id="page-footer" class="footer-wrapper<?php echo $page_footer_div_class; ?>">
                    <?php dynamic_sidebar( 'page-footer' ); ?>
                </div>
            </div>
        </section>
    <?php } ?>
        
<?php get_footer(); ?>