<?php /* Template Name: Deals Page Template */ get_header(); ?>
<div id="wrapper" class="page page-deals">
    <?php
    $images = rwmb_meta('indohotels_imgpages', 'size=big-slider'); // Since 4.8.0
    if (!empty($images)) : ?>
        <div class="section main-slider slider-room">
            <div id="slider-main" class="owl-carousel">
                <?php
                foreach ($images as $image) {
                    echo '<div class="owl-slide" style="background-image: url(\'' . $image['full_url'] . '\')"></div>';
                }
                ?>
            </div>
            <!-- end .slider-main -->
        </div>
        <!-- end .main-slider -->
    <?php endif; ?>

    <div class="container">
        <div class="section content-deals">
            <h1 class="heading-title" <?php echo empty($images) ? 'style="margin-top:80px"' : ''; ?>>
                <?php _e(the_title(), karisma_text_domain); ?>
            </h1>
            <div class="row">
                <?php
                $args = array(
                    'post_type' => 'deals',
                );
                query_posts($args);
                if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="box-content">
                                <div class="box-image gallery-deals" id="post-<?php the_ID(); ?>">
                                    <?php if (has_post_thumbnail()) : ?>
                                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" id="post-<?php the_ID(); ?>">
                                            <?php the_post_thumbnail(array(300, 150)); ?>
                                        </a>
                                        <?php
                                        $images = rwmb_meta('indohotels_imgDeals', 'size=gallery-slide');
                                        if (!empty($images)) {
                                            foreach ($images as $image) {
                                                $full_img_src = wp_get_attachment_image_src($image['ID'], 'large');
                                                $thumb_img_src = wp_get_attachment_image_src($image['ID'], 'logo-menu-thumb');
                                                echo '<a class="hidden" href="' . get_the_permalink() . '"><img src="' . $full_img_src[0] . '" src-data="' . $full_img_src[0] . '" alt="' . get_the_title() . '" </a>';
                                                // echo '<a class="hidden" href="' . $full_img_src[0] . '"><img src="' . $full_img_src[0] . '" src-data="' . $full_img_src[0] . '" alt="' . get_the_title() . '" </a>';
                                            }
                                        } ?>
                                    <?php endif; ?>
                                </div>
                                <!-- end .box-image -->
                            </div>
                            <!-- end .box-content -->
                        </div>
                        <!-- end .col-md-6 -->
                    <?php endwhile; ?>
                <?php else : ?>
                    <article>
                        <h2>
                            <?php pll_e('Sorry, nothing to display.', karisma_text_domain); ?>
                        </h2>
                    </article>
                <?php endif; ?>
            </div>
            <!-- end .row -->
        </div>
        <!-- end .content-intro -->
    </div>
    <!-- end .container -->
</div>
<!-- end .content -->
<?php get_footer(); ?>