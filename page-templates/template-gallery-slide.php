<?php /* Template Name: Gallery Slide Page Template */ get_header(); ?>

<div id="wrapper" class="page detail-facility">
    <?php
    global $post;
    $post_slug = $post->post_name;
    $args = array('post_type'=>'page', 'name'=>$post_slug);
    query_posts($args);
    if (have_posts()): while (have_posts()) : the_post(); ?>
    <div class="section facility-detail">
        <div class="slider-for bigslide">
            <?php
                $images = rwmb_meta( 'indohotels_imgpages', 'size=big-slider' ); // Since 4.8.0
                if ( !empty( $images ) ) {
                    foreach ( $images as $image ) {
            ?>
            <div class="item">
                <span>
                    <img data-lazy="<?php echo $image['url']; ?>" class="img-responsive" />
                </span>
            </div>
            <?php
            }
        }
        ?>
        </div>

        <div class="container slideshow">
            <div class="slider-nav global-thumbimg">
                <?php
            $images = rwmb_meta( 'indohotels_imgpages', 'size=album-grid' ); // Since 4.8.0
            if ( !empty( $images ) ) {
                foreach ( $images as $image ) {
            ?>
                <div class="item">
                    <span>
                        <img src="<?php echo $image['url']; ?>" class="img-responsive" />
                    </span>
                </div>
                <?php
                }
            }
            ?>

            </div>
            <!-- end .slider-nav -->
        </div>
        <!-- end .container -->
    </div>
    <?php endwhile; ?>

    <?php else: ?>

    <!-- article -->
    <article>

        <h1>
            <?php pll_e( 'Sorry, nothing to display.', karisma_text_domain ); ?>
        </h1>

    </article>
    <!-- /article -->

    <?php endif; ?>
</div>
<!-- end .content -->

<?php get_footer(); ?>
