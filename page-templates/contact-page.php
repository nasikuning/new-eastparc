<?php /* Template Name: Contact Page Template */ get_header(); ?>
<div id="wrapper" class="page contact-page">
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

      <?php
      $images = rwmb_meta('indohotels_imgpages', 'size=big-slider'); // Since 4.8.0
      if (!empty($images)) : ?>
        <div class="section main-slider slider-room">
          <div id="slider-main" class="owl-carousel">
            <?php
            foreach ($images as $image) {
              echo '<div class="owl-slide" style="background-image: url(\'' . $image['full_url'] . '\')"></div>';
            }
            ?>
          </div>
          <!-- end .slider-main -->
        </div>
        <!-- end .main-slider -->
      <?php endif; ?>

      <div class="container">
        <div class="section content-contact">
          <h1 class="heading-title" <?php echo empty($images) ? 'style="margin-top:80px"' : ''; ?>><?php _e(the_title(), karisma_text_domain); ?></h1>

          <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="contact-address text-center">
                <?php if (!empty(rwmb_meta('contact_address'))) : ?>
                  <div class="contact-item">
                    <div class="box-icon">
                      <div class="icon-inner">
                        <span class="icon">
                          <i class="icon-location"></i>
                        </span>
                      </div>
                    </div>
                    <p><?php echo rwmb_meta('contact_address'); ?></p>
                  </div><!-- end .contact-item -->
                <?php endif; ?>
                <?php if (!empty(rwmb_meta('contact_email'))) : ?>
                  <div class="contact-item">
                    <div class="box-icon">
                      <div class="icon-inner">
                        <span class="icon">
                          <i class="icon-email-mail-streamline"></i>
                        </span>
                      </div>
                    </div>
                    <?php
                    $values = rwmb_meta('contact_email');
                    foreach ($values as $value) {
                      echo '<p>' . $value . '</p>';
                    }
                    ?>
                  </div><!-- end .contact-item -->
                <?php endif; ?>
                <?php if (!empty(rwmb_meta('contact_phone'))) : ?>
                  <div class="contact-item">
                    <div class="box-icon">
                      <div class="icon-inner">
                        <span class="icon">
                          <i class="icon-phone"></i>
                        </span>
                      </div>
                    </div>
                    <?php
                    $values = rwmb_meta('contact_phone');
                    foreach ($values as $value) {
                      echo '<p>' . $value . '</p>';
                    }
                    ?>
                  </div><!-- end .contact-item -->
                <?php endif; ?>
                <?php if (!empty(rwmb_meta('contact_mobile'))) : ?>
                  <div class="contact-item">
                    <div class="box-icon">
                      <div class="icon-inner">
                        <span class="icon">
                          <i class="icon-phone-1"></i>
                        </span>
                      </div>
                    </div>
                    <?php
                    $values = rwmb_meta('contact_mobile');
                    foreach ($values as $value) {
                      echo '<p>' . $value . '</p>';
                    }
                    ?>
                  </div><!-- end .contact-item -->
                <?php endif; ?>
                <?php if (!empty(rwmb_meta('contact_fax'))) : ?>
                  <div class="contact-item">
                    <div class="box-icon">
                      <div class="icon-inner">
                        <span class="icon">
                          <i class="icon-fax"></i>
                        </span>
                      </div>
                    </div>
                    <?php
                    $values = rwmb_meta('contact_fax');
                    foreach ($values as $value) {
                      echo '<p>' . $value . '</p>';
                    }
                    ?>
                  </div><!-- end .contact-item -->
                <?php endif; ?>
              </div><!-- end .contact-address -->
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="contact-form">
                <?php echo do_shortcode('[gravityform id="1" title="true" description="true" ajax="true"]'); ?>
              </div>
            </div>
          </div><!-- end .row -->


        </div><!-- end .content-intro -->
      </div><!-- end .container -->

      <div class="contact-map">
        <!-- <div id="map-canvas" style="height: 350px; width: 100%;"></div> -->
        <?php echo rwmb_meta('map'); ?>
      </div><!-- end .row -->
    <?php endwhile; ?>

  <?php else : ?>

    <!-- article -->
    <article>

      <h1><?php pll_e('Sorry, nothing to display.', karisma_text_domain); ?></h1>

    </article>
    <!-- /article -->

  <?php endif; ?>
</div><!-- end .content -->

<?php get_footer(); ?>