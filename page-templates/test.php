<?php 
/* Template Name: Test Page Template */ get_header(); ?>

  <div id="wrapper" class="page">

    <div class="section main-slider">
      <div id="slider-main" class="owl-carousel">
      <?php 
        global $post;
        $post_slug = $post->post_name;
        $args = array('post_type'=>'page', 'name'=>$post_slug);
        query_posts($args);
        if (have_posts()): while (have_posts()) : the_post();
         $images = rwmb_meta( 'indohotels_imgpages', 'size=gallery-slide' ); // Since 4.8.0
      
        if ( !empty( $images ) ) {
            foreach ( $images as $image ) {
      ?>
            <div class="owl-slide" style="background-image: url('<?php echo $image['full_url']; ?>')"></div>
        
      <?php
              }
        }
      ?>
      <?php endwhile; ?>
      <?php endif; ?>
      </div><!-- end .slider-main -->
    </div><!-- end .main-slider -->

    <div class="container">
      <div class="section content-sitemap">
        <h1 class="heading-title"><?php the_title(); ?></h1>

        <?php $sitemaploopposts = new WP_Query( array( 
                        'post_type' => 'post', 
                        'posts_per_page' => -1, 
                        'post_status' => 'publish' 
                ));?>

                <?php while ( $sitemaploopposts->have_posts() ) : $sitemaploopposts->the_post(); ?>

                        <a href="<?php echo get_permalink($post->ID); ?>" rel="dofollow" title="<?php the_title(); ?>">
                                <?php the_title(); ?>
                        </a>

                <?php endwhile; ?>

                <?php wp_reset_query(); ?>

        </div><!-- end .content-intro -->

        </div><!-- end .container -->

    </div><!-- end .content -->

    <?php get_footer(); ?>