<?php
// phpinfo();die();
?>
<?php /* Template Name: BCA KlikPay Payment Template */ get_header(); ?>
<div id="wrapper" class="page">
    <?php if (have_posts()): while (have_posts()) : the_post(); ?>
    <?php
    $images = rwmb_meta( 'indohotels_imgpages', 'size=big-slider' ); // Since 4.8.0
    if ( !empty( $images ) ) : ?>
    <div class="section main-slider slider-room">
        <div id="slider-main" class="owl-carousel">
            <?php 
          foreach ( $images as $image ) {
            echo '<div class="owl-slide" style="background-image: url(\''. $image['full_url'].'\')"></div>';
            }
        ?>
        </div>
        <!-- end .slider-main -->
    </div>
    <!-- end .main-slider -->
    <?php endif; ?>
    <div class="section content-book book-detail">
        <div class="container">
            <h1 class="heading-title" <?php echo empty( $images ) ? 'style="margin-top:80px"' : ''; ?>>
                <?php the_title(); ?>
            </h1>
            <?php
                global $post, $wpdb;

            $book_list = krs_books_get_cookie();
       
       if (empty($book_list)) {
           wp_redirect(get_site_url());
           die();
       }

       if ($book_list) {
 
           foreach ($book_list as $list) :

            $vroom = processDiscount($list['promotion_code'], $list['total_price'], $list['total_night'],$list['book_room_type'],$list['book_num_room']);
            $total_payment  +=  $vroom['total'];
            $actual_price = $list['total_price'];
            $total_actual_price += $list['total_price'];
               // echo '<pre>';
               // print_r($actual_price);die();
           ?>
            <table class="col-md-6 room-price-list">
                <tbody>
                    <?php
                   global $wpdb;

                   ?>
                    <tr>
                        <td>Room Type</td>
                        <td>:</td>
                        <td><?php echo $list['room_name']; ?></td>
                    </tr>
                    <tr>
                        <td>Number of Room</td>
                        <td>:</td>
                        <td><?php echo $list['book_num_room']; ?></td>
                    </tr>
                    <tr>
                        <td>Total Night</td>
                        <td>:</td>
                        <td><?php echo $list['total_night']; ?></td>
                    </tr>
                    <tr>
                        <td>Check In</td>
                        <td>:</td>
                        <td><?php echo $list['book_checkin']; ?></td>
                    </tr>
                    <tr>
                        <td>Check Out</td>
                        <td>:</td>
                        <td><?php echo $list['book_checkout']; ?></td>
                    </tr>
                    <tr>
                        <td>Name</td>
                        <td>:</td>
                        <td><?php echo $list['book_title'] . " " . $list['book_first_name'] . " " . $list['book_last_name']; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Phone</td>
                        <td>:</td>
                        <td><?php echo $list['book_phone']; ?></td>
                    </tr>
                    <tr>
                        <td>E-Mail</td>
                        <td>:</td>
                        <td><?php echo $list['book_email']; ?></td>
                    </tr>
                    <tr>
                        <td>Price</td>
                        <td>:</td>
                        <td><?php echo number_format( $actual_price ) . " IDR";  
                            if ($vroom['total']==true){
                                echo '&nbsp; | &nbsp; Discount '.number_format($vroom['deduction']).' IDR';
                            }
                            ?>
                        </td>
                    </tr>
                    <?php if($vroom['total']==true && $vroom['code']) : ?>
                    <tr>
                        <td>Membership Code</td>
                        <td>:</td>
                        <td><?php 
                        if ($vroom['total']==true){
                            echo '&nbsp;<strong><span style="color:red;">'.$vroom['code'].'</span></strong>';
                        } else {
                            echo 'No Promotion Code';
                        }
                        ?>
                        </td>
                    </tr>
                    <?php endif;?>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><a class="btn btn-danger"
                                href="<?php echo site_url() ; ?>/book-room/?step=summ&cancel_id=<?php echo $list['book_checkin'] . $list['book_checkout'] . $list['book_room_type'] . preg_replace('/\s+/', '', $list['book_first_name']) . preg_replace('/\s+/', '', $list['book_last_name']); ?>">Cancel
                        </td>
                    </tr>
                </tbody>
            </table>
            <?php endforeach;
       }
       ?>
            <table class="table total-payment">
                <tr>
                    <td colspan="12"> total payment </td>
                    <td><?php echo 'Rp. '.number_format( $total_payment, 0, ',', '.'); ?></td>
                </tr>
            </table>
            <?php
    class KlikPayKeys 
    {
        var $trace = array();
        
        final public function signature($klikPayCode, $transactionNo, $currency = "IDR", $clearKey, $transactionDate, $totalAmount) {
            $this->trace = array(
                "klikPayCode" => $klikPayCode,
                "transactionNo" => $transactionNo,
                "currency" => $currency,
                "transactionDate" => $transactionDate,
                "totalAmount" => $totalAmount
            );

            $klikPayCode = preg_match('/[A-Z0-9a-z]+/',$klikPayCode,$m) ? substr($m[0],0,10) : "";

            $transactionNo = preg_match('/[A-Z0-9a-z]+/',$transactionNo,$m) ? substr($m[0],0,18) : "";

            $currency = preg_match('/[A-Z]+/',$currency,$m) ? substr($m[0],0,5) : "";
            $transactionDate = preg_match('/\d\d\/\d\d\/\d{4} \d\d:\d\d:\d\d/',$transactionDate,$m) ? substr($m[0],0,19) : "";
            $clearKey = preg_match('/[0-9A-Za-z]+/',$clearKey,$m) ? substr($m[0],0,32) : "";
            $totalAmount = preg_match('/(\d+)\.?/',$totalAmount,$m) ? substr($m[1],0,12) : "";      
            $keyId = strlen($clearKey)==32?$clearKey:$this->keyid($clearKey);

            $this->trace["keyId"] = $keyId;

            $str1 = $klikPayCode.$transactionNo.$currency.$keyId;
            $str2 = intval(str_replace("/","",substr($transactionDate,0,10))) + intval($totalAmount);
            $this->trace["firstval"] = $str1;
            $this->trace["secondval"] = $str2;

            $str1 = $this->dohash($str1);

            $str2 = $this->dohash($str2);

            $str3 = str_replace("-","",bcadd($str1,$str2));

            $this->trace["thirdval"] = $str3;

            return $str3;
        }

        final public function authkey($klikPayCode,$transactionNo,$currency="IDR",$transactionDate,$clearKey) {
            ini_set("default_charset", "utf-8");
            $this->trace = array(
                "klikPayCode" => $klikPayCode,
                "transactionNo" => $transactionNo,
                "currency" => $currency,
                "transactionDate" => $transactionDate,
                "clearKey" => $clearKey
            );
            $keyId = $this->keyid($clearKey);
            $tripledes = $this->tripledes($this->tomd5($this->concat($klikPayCode,$transactionNo,$currency,$transactionDate,$keyId)),$keyId);

            return $tripledes;
        }

        final public function keyid($clearKey) {
            $hexs = array('0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F');
            for($i=0,$x=mb_strlen($clearKey, 'utf-8'),$r="";$i<$x;$i++)
            $r .= $hexs[(ord($clearKey[$i])&0xFF)/16] . $hexs[(ord($clearKey[$i])&0xFF)%16];
            return $r;
        }

        private function dohash($str) {
            $min = -2147483648;
            $max = 2147483647;
            $str = (string) $str;

            for($i=0, $x=strlen($str), $hash=0; $i<$x; $i++){

                $hash = bcadd(bcmul($hash,31),ord($str[$i]));
                while ($hash > $max) {
                    $hash = bcsub(bcsub(bcadd($hash,$min),$max),1);
                }
                while ($hash < $min) {
                    $hash = bcadd(bcsub(bcadd($hash,$max),$min),1);
                }
            }
            return $hash;
        }

        private function concat($klikPayCode,$transactionNo,$currency="IDR",$transactionDate,$keyId) {
            if (strlen($klikPayCode) < 3) return "ERROR: klikPayCode invalid";
            if (strlen($transactionNo) < 1) return "ERROR: transactionNo invalid";
            if (!preg_match('/^(\d\d)\/(\d\d)\/(\d{4}) (\d\d):(\d\d):(\d\d)$/',$transactionDate,$m)) return "ERROR: Format transactionDate invalid";
            if (intval($m[1]) > 31 || intval($m[2]) > 12) return "ERROR: Format transactionDate invalid";
            $klikPayCode = str_pad(substr($klikPayCode,0,10),10,"0",STR_PAD_RIGHT);
            $this->trace['data01'] = $klikPayCode;
            $transactionNo = str_pad(substr($transactionNo,0,18),18,"A",STR_PAD_RIGHT);
            $this->trace['data02'] = $transactionNo;
            $currency = str_pad(substr($currency,0,5),5,"1",STR_PAD_RIGHT);
            $this->trace['data03'] = $currency;
            $transactionDate = str_pad(substr($transactionDate,0,19),19,"C",STR_PAD_LEFT);
            $this->trace['data04'] = $transactionDate;
            $this->trace['step01'] = $keyId;
            $keyId = str_pad($keyId,32,"E",STR_PAD_RIGHT);
            $str = $klikPayCode.$transactionNo.$currency.$transactionDate.$keyId;
            $this->trace['step02'] = $str;
            return $str;
        }

        private function tomd5($str) {
            if (strlen($str) != 84) return "ERROR: String Format invalid";
            $this->trace['step03'] = strtoupper(md5($str));
            return strtoupper(md5($str));
        }

        private function tripledes($str,$key) {
            if (strlen($str) != 32) return "ERROR: String md5 invalid ($str)";
            if (strlen($key) != 32) return "ERROR: KeyId invalid";
            try {
                for ($i=0,$n=strlen($str)/2;$i<$n;$i++) {
                    $bhk[$i] = chr(intval(substr($str,2*$i,2),16));
                }
                for ($i=0,$n=strlen($key)/2;$i<$n;$i++) {
                    $bk[$i] = chr(intval(substr($key,2*$i,2),16));
                }
                for ($i=0;$i<8;$i++) {
                    $bk[] = $bk[$i];
                }

                $bk = implode("",$bk);
                $bhk = implode("",$bhk);

                $mmod = mcrypt_module_open(MCRYPT_TRIPLEDES, '', 'ecb', '');

                mcrypt_generic_init($mmod,$bk, str_pad("",8,"\0",STR_PAD_LEFT));
                $cout = mcrypt_generic($mmod,$bhk);
                mcrypt_module_close($mmod);
                for ($i=0,$n=strlen($cout),$out="";$i<$n;$i++) {
                    $v = dechex(ord($cout[$i])&0xFF);
                    $out .= strlen($v)==1 ? "0".$v : $v;
                }

                $this->trace['step04'] = strtoupper($out);
                return strtoupper($out);
            } catch (Exception $e) {
                echo "ERROR: General Exception :\n".print_r($e,1);
                die;
            }
        }
    }



    $klikPayCode = "02EAST0461";
    $transactionNo = "EASTPARC".time();
    $currency = "IDR";
    // $clearKey = "ClearKeyDev2East";

    $clearKey = "dNie56bn74ndfcxl";
    $trans_date = strtotime(gmdate('Y-m-d H:i:s',time()+(25200)));
    $transactionDate = date('d/m/Y H:i:s', $trans_date);
    $totalAmount = $total_payment.".00";

    $bcakey = new KlikPayKeys;
    //echo 'KPCODE: '.$klikPayCode.' TRNO: '.$transactionNo.' CRNC: '.$currency.' TRADAT: '.$transactionDate.' CKEY: '.$clearKey.' TOTAMNT: '.$totalAmount;
    $signature = $bcakey->signature($klikPayCode, $transactionNo, $currency, $clearKey, $transactionDate, $totalAmount);

    if (strpos($signature,'.')){
        $signature = substr($signature,0,strpos($signature,'.'));
    }

    // $authkey = $bcakey->authkey($c, $transactionNo, $currency, $transactionDate, $clearKey);
    $authkey = $bcakey->authkey($klikPayCode, $transactionNo, $currency, $transactionDate, $clearKey);

    global $wpdb;
    // Save the transaction data into database
    $trans['trans_no']     = $transactionNo;
    $trans['trans_date']   = date('Y-m-d H:i:s', $trans_date);
    $trans['pay_type']     = '01';
    $trans['currency']     = 'IDR';
    $trans['actual_price']     = $total_actual_price ;
    $trans['total_amount'] = $totalAmount;
    $trans['discount'] = $vroom['deduction'];
    $trans['signature']    = $signature;
    $trans['authkey']      = $authkey;  
    $trans['status']       = 'unpaid';
    
    $wpdb->query(sql_new($wpdb->prefix . 'ss_books_trans', $trans));

            // Save the detail transaction
            foreach ($book_list as $detail) {
                $vroom = processDiscount($detail['promotion_code'], $detail['total_price'], $detail['total_night'], $detail['book_room_type'],$detail['book_num_room']);
                $amount_and_disc =   $vroom['total'];
                
                $trans_detail['trans_no']   = $merchantTransactionID;
                $trans_detail['room_id']    = $detail['book_room_type'];
                $trans_detail['checkin']    = $detail['book_checkin'];
                $trans_detail['checkout']   = $detail['book_checkout'];
                $trans_detail['arrival_time']   = $detail['book_arrival'];
                $trans_detail['name']       = $detail['book_title']." ".$detail['book_first_name']." ".$detail['book_last_name'];
                $trans_detail['title']      = $detail['book_title'];
                $trans_detail['first_name'] = $detail['book_first_name'];
                $trans_detail['last_name']  = $detail['book_last_name'];
                $trans_detail['phone']  = $detail['book_phone'];
                $trans_detail['email']      = $detail['book_email'];
                $trans_detail['qty']        = $detail['book_num_room'];
                $trans_detail['adult']      = $detail['book_person_ad'];
                $trans_detail['child']      = $detail['book_person_chld'];
                $trans_detail['request']    = $detail['book_request'];
                $trans_detail['amount']     = $detail['total_price'];
                $trans_detail['amount_and_disc']     = $amount_and_disc;
                
                $wpdb->query(sql_new($wpdb->prefix . 'ss_books_trans_detail', $trans_detail));
            }

                // $post_url = "https://sandbox.sprintasia.net/klikpay/";
                //$post_url = "https://simpg.sprintasia.net/klikpay/webgw";
                // $post_url = "http://simpg.sprintasia.net:8779/klikpay/webgw";
                //$post_url = "https://simpg.sprintasia.net/klikpay/webgw";
                // $post_url = "https://202.6.215.230:8081/purchasing/purchase.do?action=loginRequest";
                
                $post_url = " https://klikpay.klikbca.com/purchasing/purchase.do?action=loginRequest";
                // $post_url = "http://www.eastparchotel.com/book-result/?booking_number=".$transactionNo;

        ?>
            <div class="text-center">
                <label for="confirmation" class="label-confirmation">
                    <input type="checkbox" id="confirmation" required>
                    I understand and agree with the <a
                        href="<?php bloginfo('url');?>/bca-klikpay-terms-and-conditions/"> terms and conditions </a> for
                    this booking.
                </label>
            </div>
            <form class="payment-form" id="payment_form" method="post" action="<?php echo $post_url; ?>">
                <input type="hidden" name="klikPayCode" value="02EAST0461">
                <input type="hidden" name="transactionNo" value="<?php echo $transactionNo;?>">
                <input type="hidden" name="totalAmount" value="<?php echo $total_payment.'.00'; ?>">
                <input type="hidden" name="currency" value="IDR">
                <input type="hidden" name="payType" value="01">
                <input type="hidden" name="callback"
                    value="<?php echo site_url(); ?>/payment/status/?trans=<?php echo $transactionNo;?>">
                <input type="hidden" name="transactionDate" value="<?php echo $transactionDate; ?>">
                <input type="hidden" name="descp" value="">
                <input type="hidden" name="miscFee" value="0.00">
                <input type="hidden" name="signature" value="<?php echo $signature;?>">
                <button type="submit" class="btn btn-primary">Proceed</button>
                <a href="<?php echo site_url() . '/book-room/?step=cancel'; ?>" class="btn btn-danger">cancel
                    booking</a>
            </form>
        </div>
    </div>
    <?php endwhile; ?>

    <?php else: ?>

    <!-- article -->
    <article>

        <h1>
            <?php pll_e( 'Sorry, nothing to display.', karisma_text_domain ); ?>
        </h1>

    </article>
    <!-- /article -->

    <?php endif; ?>
</div>
<!-- end .content -->

<?php get_footer(); ?>
<script>
    var input = document.querySelector("#phone");
    if (input) {
        window.intlTelInput(input, {
            initialCountry: "id",
        });
    }
    jQuery("#payment_form").submit(function (event) {
        if (jQuery("#confirmation").prop('checked') != true) {
            alert('Please check "I understand and agree with the Terms and Conditions for this booking."')
            event.preventDefault();
        }
    });
</script>