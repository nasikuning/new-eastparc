<?php
/* Template Name: Rooms Page Template1 */ get_header(); ?>

<div id="wrapper" class="page">
	<div class="section main-slider slider-room">
		<div id="slider-main" class="owl-carousel">
			<?php
			global $post;
			$post_slug = $post->post_name;
			$args = array('post_type' => 'page', 'name' => $post_slug);
			query_posts($args);
			if (have_posts()) : while (have_posts()) : the_post();
					$images = rwmb_meta('indohotels_imgpages', 'size=big-slider'); // Since 4.8.0

					if (!empty($images)) {
						foreach ($images as $image) {
							echo '<div class="owl-slide" style="background-image: url(\'' . $image['url'] . '\')"></div>';
						}
					} ?>
				<?php endwhile; ?>
			<?php endif; ?>
		</div><!-- end .slider-main -->
		<?php if (ot_get_option('krs_booking_engine') == 'on') :  ?>
			<div class="booking-box no-homepage">
				<div class="container">
					<h2 class="text-center visible-xs">
						<?php pll_e('Instant Booking', karisma_text_domain); ?>
					</h2>
				</div>
				<form class="main-booking" action="<?php echo pll_current_language() == 'en' ? '/room-search/' : '/id/cari-kamar/'; ?>" method="GET" role="form" onSubmit="return validateBook()">
					<div class="container">
						<div class="row fields">
							<div class="col-md-4 col-xs-12 field">
								<div id="datepicker" class="row">
									<div class="col-xs-6 field">
										<label for=""><?php pll_e('Check-in', karisma_text_domain); ?></label>
										<div class="field-inner clearfix">
											<span readonly="true" id="from_date_day"><?php echo date("l", strtotime($_REQUEST['start'])); ?></span>
											<input readonly="true" id="from_date" type="text" class="from_date from_date form-control" name="start" value="<?php echo $_REQUEST['start'] ?>">
											<input readonly="true" id="mobile_date" style="display:none" type="text">
										</div>
										<!-- end .field-inner -->
									</div>
									<div class="col-xs-6 field">
										<label for=""><?php pll_e('Check-out', karisma_text_domain); ?></label>
										<div class="field-inner clearfix">
											<span readonly="true" id="to_date_day"><?php echo date("l", strtotime($_REQUEST['end'])); ?></span>
											<input readonly="true" type="text" id="to_date" class="to_date to_date_mobile form-control" name="end" value="<?php echo $_REQUEST['end'] ?>">
										</div>
										<!-- end .field-inner -->
									</div>
								</div>
							</div>

							<div class="col-md-4 col-xs-12 field">
								<div id="totalnumber" class="row">
									<div class="col-sm-6 field">
										<div class="form-group">
											<label for=""><?php pll_e('Total Night(s)', karisma_text_domain); ?></label>
											<div class="box-number night field-inner clearfix">
												<button type="button" class="qtyminus" field="night"><i class="fa fa-minus" value=""></i></button>
												<input type="text" id="t_night" readonly="true" name="night" class="t_night qty form-control" value="<?php echo !empty($_REQUEST['night']) ? $_REQUEST['night'] : 1; ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
												<button type="button" class="qtyplus" field="night"><i class="fa fa-plus" value=""></i></button>
											</div>
											<!-- end .field-inner -->
										</div>
									</div>
									<div class="col-sm-6 field">
										<div class="form-group">
											<label for=""><?php pll_e('Number of Room(s)', karisma_text_domain); ?></label>
											<div class="box-number one field-inner clearfix">
												<button type="button" class="qtyminus" field="room"><i class="fa fa-minus"></i></button>
												<input type="text" readonly="true" name="room" value="<?php echo !empty($_REQUEST['room']) ? $_REQUEST['room'] : 1; ?>" class="qty form-control" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
												<button type="button" class="qtyplus" field="room"><i class="fa fa-plus"></i></button>
											</div>
											<!-- end .field-inner -->
										</div>
									</div>
								</div>
							</div>
							<input type="hidden" name="refferer" value="eastparchotel.com">

							<div class="col-md-4 col-xs-12 field">
								<div class="form-group book-button">
									<label class="total-book" for=""></label><label class="last-book" for=""></label>
									<button type="submit" class="btn btn-yellow"><?php pll_e('Check Availability', karisma_text_domain); ?></button>
								</div>
							</div>
						</div>
						<p><?php printf(__('Occupancy per room is limited to 2 adults. <a href="%s">Click here</a> to read hotel\'s policy on accompanying children,
            extra beds and breakfast'), esc_url(get_permalink())); ?></p>

						<!-- <div class="booking-powered-by text-center visible-xs">
            <img class="center-block" src="<?php echo krs_url ?>asset/images/powered-by-indohotels.svg" alt="">
          </div> -->
					</div>
				</form>

			</div>
		<?php endif; ?>
		<!-- end .booking-box -->
	</div><!-- end .main-slider -->

	<div class="container">
		<!-- container -->
		<!-- section -->
		<section>
			<?php
			$args = array('post_type' => 'rooms');
			query_posts($args);
			if (have_posts()) : while (have_posts()) : the_post(); ?>
					<div class="box-container col-md-12">
						<div class="room-thumb thumbnail">
							<!-- article -->
							<article id="post-<?php the_ID(); ?>" <?php post_class('rooms-search-post'); ?>>
								<div class="row">
									<div class="col-md-5">
										<!-- post thumbnail -->
										<div class="thumb">
											<?php if (has_post_thumbnail()) : // Check if thumbnail exists 
											?>
												<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
													<?php the_post_thumbnail(array(500, 280)); // Declare pixel size you need inside the array 
													?>
												</a>
											<?php endif; ?>
										</div>
										<!-- /post thumbnail -->
									</div>
									<div class="col-md-7">
										<div class="box-info">
											<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
											<p class="subtitle">
												<?php pll_e(rwmb_meta('indohotels_room_balcony'), karisma_text_domain); ?></p>
											<div class="row">
												<div class="col-xs-6">
													<ul class="info-detail">
														<li>
															<span class="detail-left"><?php pll_e('Bed Size', karisma_text_domain); ?></span>
															<span class="dot">:</span>
															<span class="detail-right"><?php pll_e(rwmb_meta('bed_size'), karisma_text_domain); ?></span>
														</li>
														<li>
															<span class="detail-left"><?php pll_e('Occupancy', karisma_text_domain); ?></span>
															<span class="dot">:</span>
															<span class="detail-right"><?php pll_e(rwmb_meta('room_occupancy'), karisma_text_domain); ?><?php pll_e('persons', karisma_text_domain); ?></span>
														</li>
														<li>
															<span class="detail-left"><?php pll_e('Room Size', karisma_text_domain); ?></span>
															<span class="dot">:</span>
															<span class="detail-right"><?php pll_e(rwmb_meta('room_size'), karisma_text_domain); ?>
																m<sup>2</sup></span>
														</li>
														<li>
															<span class="detail-left"><?php pll_e('View', karisma_text_domain); ?></span>
															<span class="dot">:</span>
															<span class="detail-right"><?php pll_e(rwmb_meta('room_view')); ?></span>
														</li>
													</ul><!-- end .info-detail -->
												</div>
												<div class="col-xs-6 text-right">
													<div>
														<?php
														$room_book_switch = get_post_meta(get_the_ID(), '_krs_room_book_switch_meta_key', true);
														$direct_url = get_post_meta(get_the_ID(), '_krs_button_direct_url_meta_key', true);
														$direct_text = get_post_meta(get_the_ID(), '_krs_button_direct_text_meta_key', true);
														$total_night = is_numeric((int)$_REQUEST['night']) ? $_REQUEST['night'] : 1;
														$total_room = is_numeric((int)$_REQUEST['room']) ? $_REQUEST['room'] : 1;
														$price = krs_get_room_price(get_the_ID(), date('Y-m-d', strtotime($_REQUEST['start'])), date('Y-m-d', strtotime($_REQUEST['end'])), 1, 'search');

														if (($room_book_switch == '1' || empty($direct_url)) && $price) {
															$target = '';
															$url = get_permalink() . '?start=' . $_REQUEST['start'] . '&end=' . $_REQUEST['end'] . '&night=' . $total_night . '&room=' . $total_room;
															$text = 'Room Details';
														} else {
															$target = 'target="_blank"';
															$url = $direct_url;
															$text = $direct_text;
														}


														if ($price && $room_book_switch == '1') :
														?>
															<div> <strong>Rp <?php echo number_format(($price), 0, ',', '.'); ?></strong></div>
															<div><?php pll_e('/ room / night', karisma_text_domain); ?></div>
														<?php endif; ?>
														<a <?php echo $target; ?> href="<?php echo $url; ?>" title="<?php echo $text; ?>" class="nbutton">
															<?php pll_e($text, karisma_text_domain); ?></a>
													</div>
												</div>
											</div>

										</div><!-- end .box-info -->
									</div>
								</div>
							</article>
							<!-- /article -->
						</div>
					</div>

				<?php endwhile; ?>

			<?php else : ?>

				<!-- article -->
				<article>
					<h2><?php pll_e('Sorry, nothing to display.', 'indohotels'); ?></h2>
				</article>
				<!-- /article -->

			<?php endif; ?>

			<?php get_template_part('pagination'); ?>

		</section>
		<!-- /section -->
	</div> <!-- end container -->
</div><!-- end .content -->

<?php get_footer(); ?>