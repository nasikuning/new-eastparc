<?php /* Template Name: Gallery Page Template */ get_header(); ?>
<div id="wrapper" class="page">
  <?php
  $images = rwmb_meta('indohotels_imgpages', 'size=big-slider'); // Since 4.8.0
  if (!empty($images)) : ?>
    <div class="section main-slider slider-room">
      <div id="slider-main" class="owl-carousel">
        <?php
        foreach ($images as $image) {
          echo '<div class="owl-slide" style="background-image: url(\'' . $image['full_url'] . '\')"></div>';
        }
        ?>
      </div>
      <!-- end .slider-main -->
    </div>
    <!-- end .main-slider -->
  <?php endif; ?>

  <div class="container">
    <div class="section content-gallery">
      <h1 class="heading-title" <?php echo empty($images) ? 'style="margin-top:80px"' : ''; ?>>
        <?php _e(the_title(), karisma_text_domain); ?>
      </h1>
      <div class="row">
        <?php
        $args = array(
          'post_type' => 'gallery',
        );
        query_posts($args);
        if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div class="col-sm-6 col-xs-12">
              <div class="box-content">
                <div class="box-image">
                  <?php if (has_post_thumbnail()) : ?>
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                      <?php the_post_thumbnail(array(300, 150)); ?>
                    </a>
                  <?php endif; ?>
                </div>
                <!-- end .box-image -->
                <div class="box-info text-center">
                  <h4>
                    <?php the_title(); ?>
                  </h4>
                  <a href="<?php the_permalink(); ?>" class="nbutton"><?php pll_e('See More', karisma_text_domain); ?></a>
                </div>
                <!-- end .box-info -->
              </div>
              <!-- end .box-content -->
            </div>
            <!-- end .col-md-6 -->
          <?php endwhile; ?>
        <?php else : ?>
          <article>
            <h2>
              <?php pll_e('Sorry, nothing to display.', karisma_text_domain); ?>
            </h2>
          </article>
        <?php endif; ?>
      </div>
      <!-- end .row -->
    </div>
    <!-- end .content-intro -->

  </div>
  <!-- end .container -->

</div>
<!-- end .content -->

<?php get_footer(); ?>