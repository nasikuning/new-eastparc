<?php /* Template Name: Virtual Account Payment Template */ get_header(); ?>
<div id="wrapper" class="page">
    <?php if (have_posts()): while (have_posts()) : the_post(); ?>
    <?php
    $images = rwmb_meta( 'indohotels_imgpages', 'size=big-slider' ); // Since 4.8.0
    if ( !empty( $images ) ) : ?>
    <div class="section main-slider slider-room">
        <div id="slider-main" class="owl-carousel">
            <?php 
          foreach ( $images as $image ) {
            echo '<div class="owl-slide" style="background-image: url(\''. $image['full_url'].'\')"></div>';
            }
        ?>
        </div>
        <!-- end .slider-main -->
    </div>
    <!-- end .main-slider -->
    <?php endif; ?>
    <div class="section content-book book-detail">
        <div class="container">
            <h1 class="heading-title" <?php echo empty( $images ) ? 'style="margin-top:80px"' : ''; ?>>
                <?php the_title(); ?>
            </h1>
            <?php
                global $post, $wpdb;

            $book_list = krs_books_get_cookie();
       
       if (empty($book_list)) {
           wp_redirect(get_site_url());
           die();
       }

       if ($book_list) {
 
           foreach ($book_list as $list) :

            $vroom = processDiscount($list['promotion_code'], $list['total_price'], $list['total_night'],$list['book_room_type'],$list['book_num_room']);
            $total_payment  +=  $vroom['total'];
            $actual_price = $list['total_price'];
            $total_actual_price += $list['total_price'];
               // echo '<pre>';
               // print_r($actual_price);die();
           ?>
            <table class="col-md-6 room-price-list">
                <tbody>
                    <?php
                   global $wpdb;

                   ?>
                    <tr>
                        <td>Room Type</td>
                        <td>:</td>
                        <td><?php echo $list['room_name']; ?></td>
                    </tr>
                    <tr>
                        <td>Number of Room</td>
                        <td>:</td>
                        <td><?php echo $list['book_num_room']; ?></td>
                    </tr>
                    <tr>
                        <td>Total Night</td>
                        <td>:</td>
                        <td><?php echo $list['total_night']; ?></td>
                    </tr>
                    <tr>
                        <td>Check In</td>
                        <td>:</td>
                        <td><?php echo $list['book_checkin']; ?></td>
                    </tr>
                    <tr>
                        <td>Check Out</td>
                        <td>:</td>
                        <td><?php echo $list['book_checkout']; ?></td>
                    </tr>
                    <tr>
                        <td>Name</td>
                        <td>:</td>
                        <td><?php echo $list['book_title'] . " " . $list['book_first_name'] . " " . $list['book_last_name']; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Phone</td>
                        <td>:</td>
                        <td><?php echo $list['book_phone']; ?></td>
                    </tr>
                    <tr>
                        <td>E-Mail</td>
                        <td>:</td>
                        <td><?php echo $list['book_email']; ?></td>
                    </tr>
                    <tr>
                        <td>Price</td>
                        <td>:</td>
                        <td><?php echo number_format( $actual_price ) . " IDR";  
                            if ($vroom['total']==true){
                                echo '&nbsp; | &nbsp; Discount '.number_format($vroom['deduction']).' IDR';
                            }
                            ?>
                        </td>
                    </tr>
                    <?php if($vroom['total']==true && $vroom['code']) : ?>
                    <tr>
                        <td>Membership Code</td>
                        <td>:</td>
                        <td><?php 
                        if ($vroom['total']==true){
                            echo '&nbsp;<strong><span style="color:red;">'.$vroom['code'].'</span></strong>';
                        } else {
                            echo 'No Promotion Code';
                        }
                        ?>
                        </td>
                    </tr>
                    <?php endif;?>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><a class="btn btn-danger"
                                href="<?php echo site_url() ; ?>/book-room/?step=summ&cancel_id=<?php echo $list['book_checkin'] . $list['book_checkout'] . $list['book_room_type'] . preg_replace('/\s+/', '', $list['book_first_name']) . preg_replace('/\s+/', '', $list['book_last_name']); ?>">Cancel
                        </td>
                    </tr>
                </tbody>
            </table>
            <?php endforeach;
       }
       ?>
        <table class="table total-payment">
            <tr>
                <td colspan="12"> total payment </td>
                <td><?php echo 'Rp. '.number_format( $total_payment, 0, ',', '.'); ?></td>
            </tr>
        </table>
        <?php
            $siteID = "Eastparchotel";    
            $serviceVersion = "1.2";
            $merchantTransactionID = "EASTPARC".time();;
            $ProfileCode = 'C2C637F3-E63E-232D-F51310A4234F3BC5';
            $transactionType = "AUTHORIZATION";
            $soml = "";
            $currency = "IDR";
            $amount = $total_payment;
            $checkSum = md5($amount.$currency.$merchantTransactionID.$serviceVersion.$siteID.$soml.$transactionType.$ProfileCode);
            $trans_date = strtotime(gmdate('Y-m-d H:i:s',time()+(25200)));
            $transactionDate = date('d/m/Y H:i:s', $trans_date);
            
            global $wpdb;
            // Save the transaction data into database
            $trans['trans_no']     = $merchantTransactionID;
            $trans['trans_date']   = date('Y-m-d H:i:s', $trans_date);
            $trans['actual_price']     =  $total_actual_price;
            $trans['amount']     =  $amount;
            $trans['total_amount']     =  $amount;
            $trans['discount']     =  $vroom['deduction'];
            $trans['merchantTransactionID']  =  $merchantTransactionID;
            $trans['checkSumResponse'] = $checkSum;

            $wpdb->query(sql_new($wpdb->prefix . 'ss_books_credit_trans', $trans));

            // Save the detail transaction
            foreach ($book_list as $detail) {
                $vroom = processDiscount($detail['promotion_code'], $detail['total_price'], $detail['total_night'], $detail['book_room_type'], $detail['book_num_room']);
                $amount_and_disc =   $vroom['total'];
                
                $trans_detail['trans_no']   = $merchantTransactionID;
                $trans_detail['room_id']    = $detail['book_room_type'];
                $trans_detail['checkin']    = $detail['book_checkin'];
                $trans_detail['checkout']   = $detail['book_checkout'];
                $trans_detail['arrival_time']   = $detail['book_arrival'];
                $trans_detail['name']       = $detail['book_title']." ".$detail['book_first_name']." ".$detail['book_last_name'];
                $trans_detail['title']      = $detail['book_title'];
                $trans_detail['first_name'] = $detail['book_first_name'];
                $trans_detail['last_name']  = $detail['book_last_name'];
                $trans_detail['phone']  = $detail['book_phone'];
                $trans_detail['email']      = $detail['book_email'];
                $trans_detail['qty']        = $detail['book_num_room'];
                $trans_detail['adult']      = $detail['book_person_ad'];
                $trans_detail['child']      = $detail['book_person_chld'];
                $trans_detail['request']    = $detail['book_request'];
                $trans_detail['amount']     = $detail['total_price'];
                $trans_detail['amount_and_disc']     = $amount_and_disc;
                
                $wpdb->query(sql_new($wpdb->prefix . 'ss_books_trans_detail', $trans_detail));
            }

            $query = "DELETE FROM ss_books_credit_trans AS t WHERE datediff(now(), t.trans_date) > 5 AND t.status = 'unpaid'";
            $wpdb->get_results($query);

            // $post_url = "https://training.doappx.com/sprintAsia/api/webAuthorization.cfm";
            $post_url = "https://acquire.doappx.com/sprint/doacquire/api/webAuthorization.cfm";

        ?>
            <div class="text-center">
                <label for="confirmation" class="label-confirmation">
                    <input type="checkbox" id="confirmation" name="confirmation" required>
                    I understand and agree with the <a href="<?php bloginfo('url'); ?>/credit-card-terms-conditions">Terms and Conditions</a> for this booking.
                </label>
            </div>
            <form class="payment-form" id="payment_form" method="post" action="<?php echo $post_url; ?>">
                <input type="hidden" name="siteID" value="<?php echo $siteID; ?>">
                <input type="hidden" name="serviceVersion" value="<?php echo $serviceVersion; ?>">
                <input type="hidden" name="checkSum" value="<?php echo $checkSum; ?>">
                <input type="hidden" name="merchantTransactionID" value="<?php echo $merchantTransactionID; ?>">
                <input type="hidden" name="transactionType" value="<?php echo $transactionType; ?>">
                <input type="hidden" name="currency" value="<?php echo $currency; ?>">
                <input type="hidden" name="amount" value="<?php echo $amount; ?>">

                <button type="submit" class="btn btn-primary">Proceed</button>
                <a href="<?php echo site_url() . '/book-room/?step=cancel'; ?>" class="btn btn-danger">cancel booking</a>
            </form>
        </div>
    </div>
    <?php endwhile; ?>

    <?php else: ?>

    <!-- article -->
    <article>

        <h1>
            <?php pll_e( 'Sorry, nothing to display.', karisma_text_domain ); ?>
        </h1>

    </article>
    <!-- /article -->

    <?php endif; ?>
</div>
<!-- end .content -->

<?php get_footer(); ?>
<script>
    var input = document.querySelector("#phone");
    if(input) {
        window.intlTelInput(input, {
            initialCountry: "id",
        });
    }
    jQuery( "#payment_form" ).submit(function( event ) {
        if(jQuery("#confirmation").prop('checked') != true){
            alert('Please check "I understand and agree with the Terms and Conditions for this booking."')
            event.preventDefault();     
        }
    });
</script>