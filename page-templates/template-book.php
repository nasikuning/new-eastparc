<?php /* Template Name: Book Page Template */ get_header(); ?>
<div id="wrapper" class="page">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <?php
            $images = rwmb_meta('indohotels_imgpages', 'size=big-slider'); // Since 4.8.0
            if (!empty($images)) : ?>
                <div class="section main-slider slider-room">
                    <div id="slider-main" class="owl-carousel">
                        <?php
                        foreach ($images as $image) {
                            echo '<div class="owl-slide" style="background-image: url(\'' . $image['full_url'] . '\')"></div>';
                        }
                        ?>
                    </div>
                    <!-- end .slider-main -->
                </div>
                <!-- end .main-slider -->
            <?php endif; ?>

            <?php
            global  $wpdb;
            $room_id = $_REQUEST['krs_rooms'];
            $checkin_day = $_REQUEST['start'] ? date('l', strtotime($_REQUEST['start'])) : date('l');
            $checkin = $_REQUEST['start'] ? date('d-m-Y', strtotime($_REQUEST['start'])) : date('d-m-Y');
            $checkout_day = $_REQUEST['end'] ? date('l', strtotime($_REQUEST['end'])) : date('l', strtotime("+1 day"));
            $checkout = $_REQUEST['end'] ? date('d-m-Y', strtotime($_REQUEST['end'])) : date('d-m-Y', strtotime("+1 day"));
            $night = is_numeric((int) $_REQUEST['night']) ? $_REQUEST['night'] : 1;
            $room = is_numeric((int) $_REQUEST['room']) ? $_REQUEST['room'] : 1;

            $checkin_default = $_REQUEST['start'] ? date('Y-m-d', strtotime($_REQUEST['start'])) :  date('Y-m-d');
            $checkout_default = $_REQUEST['start'] ? date('Y-m-d', strtotime($_REQUEST['end'])) : date('Y-m-d', strtotime("+1 day"));

            $room_price_date_q = "SELECT * FROM wp_ss_room_avail WHERE room = '{$room_id}' AND start >= '{$checkin_default}' AND end < '{$checkout_default}'";
            $room_price_date = $wpdb->get_results($room_price_date_q, ARRAY_A);
            // echo '<pre>';
            // print_r($room_price_date);
            // die();
            $total_price = 0;
            $total_night = 0;
            foreach ($room_price_date as $key => $price_date) {
                $total_price += ($price_date['price'] * $room);
                $total_night++;
            }

            $step = $_GET['step'];

            if (isset($_POST['book_this_room'])) {
                if (empty($err_message)) {
                    $book_date = $_POST['book_checkin'] . $_POST['book_checkout'] . $_POST['book_room_type'] . preg_replace('/\s+/', '', $_POST['book_first_name']) . preg_replace('/\s+/', '', $_POST['book_last_name']);
                    foreach ($_POST as $item_name => $item_value) {
                        ss_books_set_cookie($book_date, $item_name, $item_value);
                    }
                    wp_redirect($current_url . "?step='summ'");
                    die();
                }
            }

            ?>

            <div class="section content-book book-detail">
                <div class="container">
                    <h1 class="heading-title" <?php echo empty($images) ? 'style="margin-top:80px"' : ''; ?>>
                        <?php the_title(); ?>
                    </h1>
                    <?php
                    $post   = get_post($room_id);
                    switch ($step) {
                        case "cancel":
                            if (isset($_SERVER['HTTP_COOKIE'])) {
                                $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
                                foreach ($cookies as $cookie) {
                                    if (strpos($cookie, 'ss_books_')) {
                                        try {
                                            $parts = explode('=', $cookie);
                                            $name = trim($parts[0]);
                                            setcookie($name, '', time() - 3600);
                                            setcookie($name, '', time() - 3600, '/');
                                        } catch (\Throwable $th) {
                                            throw $th->getMessage();
                                        }
                                    }
                                }
                            }
                            wp_redirect(get_site_url());
                            break;

                        case "summ":

                            if ($_GET['cancel_id']) {
                                $list_cookie = krs_books_get_cookie();

                                if (isset($list_cookie) && !empty($list_cookie)) {
                                    foreach ($list_cookie as $key => $value) {
                                        if ($key == $_GET['cancel_id']) {
                                            $a = krs_books_unset_cookie(urldecode($_GET['cancel_id']));
                                        }
                                    }
                                }
                                $v = krs_books_unset_cookie($_GET['cancel_id']);
                            }



                            $book_list = krs_books_get_cookie();

                            if (empty($book_list)) {
                                wp_redirect(get_site_url());
                                die();
                            }

                            if ($book_list) {
                                // echo '<pre>';
                                //     print_r($book_list);die();
                                foreach ($book_list as $list) :

                                    $actual_price = !empty($list['total_price']) ? $list['total_price'] : 0;
                                    $total_payment  +=  $actual_price;

                    ?>
                                    <table class="col-md-6 room-price-list">
                                        <tbody>
                                            <?php
                                            global $wpdb;

                                            ?>
                                            <tr>
                                                <td>Room Type</td>
                                                <td>:</td>
                                                <td><?php echo $list['room_name']; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Number of Room</td>
                                                <td>:</td>
                                                <td><?php echo $list['book_num_room']; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Total Night</td>
                                                <td>:</td>
                                                <td><?php echo !empty($list['total_night']) ? $list['total_night'] : 1; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Promotion Code</td>
                                                <td>:</td>
                                                <td><?php echo !empty($list['promotion_code']) ? $list['promotion_code'] : 'No Promotion Code'; ?></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <?php if ($vroom['status'] == true) { ?>
                                                        Normal Price
                                                    <?php } else { ?>
                                                        Price
                                                    <?php } ?>
                                                </td>
                                                <td>:</td>
                                                <td><?php echo 'Rp. ' . number_format($actual_price, 0, ',', '.');
                                                    if ($vroom['status'] == true) {
                                                        // echo '&nbsp; | &nbsp; Discount '.number_format($vroom['deduction']).' IDR';
                                                    }
                                                    ?></td>
                                            </tr>

                                            <tr>
                                                <td>Check In</td>
                                                <td>:</td>
                                                <td><?php echo $list['book_checkin']; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Check Out</td>
                                                <td>:</td>
                                                <td><?php echo $list['book_checkout']; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Name</td>
                                                <td>:</td>
                                                <td><?php echo $list['book_title'] . " " . $list['book_first_name'] . " " . $list['book_last_name']; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Phone</td>
                                                <td>:</td>
                                                <td><?php echo $list['book_phone']; ?></td>
                                            </tr>
                                            <tr>
                                                <td>E-Mail</td>
                                                <td>:</td>
                                                <td><?php echo $list['book_email']; ?></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td><a class="btn btn-danger" href="?step=summ&cancel_id=<?php echo $list['book_checkin'] . $list['book_checkout'] . $list['book_room_type'] . preg_replace('/\s+/', '', $list['book_first_name']) . preg_replace('/\s+/', '', $list['book_last_name']); ?>">Cancel
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                            <?php endforeach;
                            }
                            ?>
                            <table class="table total-payment">
                                <tr>
                                    <td colspan="12"> total payment </td>
                                    <td><?php echo 'Rp. ' . number_format($total_payment, 0, ',', '.'); ?></td>
                                </tr>
                            </table>
                            <form class='summary-form' action="<?php bloginfo('url'); ?>/book-room/" method="post">
                                <a href="<?php bloginfo('url'); ?>/" class="btn btn-primary">book more</a>
                                <button type="submit" name="payment-confirm" value="true" class="btn btn-primary">BCA KlikPay</button>
                                <button type="submit" name="payment-credit-card" value="true" class="btn btn-primary">Credit Card</button>
                                <button type="submit" name="payment-va-permata" value="true" class="btn btn-primary">VA Permata</button>
                                <a href="<?php echo $current_url . '?step=cancel'; ?>" class="btn btn-danger">cancel booking</a>
                            </form>
                        <?php

                            break;

                        default:

                            if (isset($_POST['payment-credit-card']) && ($_POST['payment-credit-card'] == "true")) {
                                wp_redirect(get_site_url() . "/book-room/credit-card/");
                                die();
                            } else if (isset($_POST['payment-va-permata']) && ($_POST['payment-va-permata'] == "true")) {
                                wp_redirect(get_site_url() . "/book-room/va-payment/");
                                die();
                            } else if (isset($_POST['payment-confirm']) && ($_POST['payment-confirm'] == "true")) {
                                $book_list = ss_books_get_cookie();
                                if (empty($book_list)) {
                                    wp_redirect(get_site_url());
                                    die();
                                }
                                wp_redirect(get_site_url() . "/book-room/confirm-payment/");
                                die();
                            }

                            if (!$_REQUEST['krs_rooms']) {
                                wp_redirect(get_home_url());
                                die();
                            }
                        ?>

                            <div class="row">
                                <div class="col-md-7">
                                    <div class="card" style="margin-bottom: 20px">
                                        <form action="" method="POST">
                                            <div class="form-group">
                                                <label for="room_name">Room</label>
                                                <input type="text" class="form-control" name="room_name" id="room_name" value="<?php the_title(); ?>" required readonly>
                                            </div>
                                            <div class="form-group">
                                                <label for="book_num_room">Number of Room</label>
                                                <input type="text" class="form-control" name="book_num_room" id="book_num_room" value="<?php echo is_numeric($_REQUEST['room']) ? $_REQUEST['room'] : 1;  ?>" required readonly>
                                            </div>
                                            <div class="form-group">
                                                <select id="book_title" name="book_title" class="form-control" required>
                                                    <option value="Mr.">Mr.</option>
                                                    <option value="Mrs.">Mrs.</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="book_first_name">First Name</label>
                                                <input type="text" class="form-control" name="book_first_name" id="book_first_name" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="book_last_name">Last Name</label>
                                                <input type="text" class="form-control" name="book_last_name" id="book_last_name" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="book_phone">Mobile Phone Number:</label>
                                                <input type="tel" class="form-control" name="book_phone" id="book_phone" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="book_email">Email:</label>
                                                <input type="email" class="form-control" name="book_email" id="book_email" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="book_confirm_email">Confirm Email:</label>
                                                <input type="email" class="form-control" name="book_confirm_email" id="book_confirm_email" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="promotion_code">Promotion Code:</label>
                                                <input type="text" class="form-control" name="promotion_code" id="promotion_code" placeholder="Promotion Code:">
                                            </div>
                                            <!-- <button type="button" class="nbutton">Apply Code</button> -->
                                            <?php if (isset($_GET['start'])) { ?>
                                                <div class="form-group">
                                                    <label>Cost Summary</label>
                                                    <div class='ginput_container'>
                                                        Total price for <?php echo $room; ?> room(s), <?php echo $total_night; ?> night(s) is <b> <?php echo number_format(($total_price), 0, ',', '.'); ?> IDR</b>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <button type="submit" class="nbutton">Go to Payment</button>
                                            <input type="hidden" name="book_room_type" value="<?php echo $room_id; ?>">
                                            <input type="hidden" name="book_this_room" id="book_this_room" value="1">
                                            <input type="hidden" name="book_checkin" id="book_checkin" value="<?php echo $checkin_default; ?>">
                                            <input type="hidden" name="book_checkout" id="book_checkout" value="<?php echo $checkout_default; ?>">
                                            <input type="hidden" name="total_night" value="<?php echo $total_night; ?>">
                                            <input type="hidden" name="total_price" value="<?php echo $total_price; ?>">
                                        </form>
                                    </div>
                                </div>
                                <div class="col-md-5">

                                    <div class="card" style="margin-bottom: 20px">

                                        <div class="card-body" style="
                                    background-color: white;
                                    border: 1px solid #eaeaea;
                                    padding: 20px;
                                ">
                                            <div class="row">
                                                <div class="col-xs-5">
                                                    <strong>Check-in</strong>
                                                </div>
                                                <div class="col-xs-7">
                                                    <div class="" style="color:#b70015;">
                                                        <strong><?php echo $checkin_day . ', ' . $checkin; ?>, From 14:00</strong>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-5">
                                                    <strong>Check-out</strong>
                                                </div>
                                                <div class="col-xs-7">
                                                    <div class="" style="color:#b70015;">
                                                        <strong><?php echo $checkout_day . ', ' . $checkout; ?>, Before 12:00</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div>

                                        <div class="row">
                                            <div class="col-md-5">
                                                <!-- post thumbnail -->
                                                <div class="thumb">
                                                    <a target="_blank" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                                        <div class="thumbnail">
                                                            <?php the_post_thumbnail('medium', array('class' => 'img-responsive')); ?>
                                                        </div>
                                                    </a>
                                                </div>
                                                <!-- /post thumbnail -->
                                            </div>
                                            <div class="col-md-7">
                                                <div class="box-info">
                                                    <h5><?php the_title(); ?></h5>
                                                    <p class="subtitle">
                                                        <?php pll_e(rwmb_meta('indohotels_room_balcony'), karisma_text_domain); ?></p>
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <ul class="info-detail">
                                                                <li>
                                                                    <span class="detail-left"><?php pll_e('Bed Size', karisma_text_domain); ?></span>
                                                                    <span class="dot">:</span>
                                                                    <span class="detail-right"><?php pll_e(rwmb_meta('bed_size'), karisma_text_domain); ?></span>
                                                                </li>
                                                                <li>
                                                                    <span class="detail-left"><?php pll_e('Occupancy', karisma_text_domain); ?></span>
                                                                    <span class="dot">:</span>
                                                                    <span class="detail-right"><?php pll_e(rwmb_meta('room_occupancy'), karisma_text_domain); ?><?php pll_e('persons', karisma_text_domain); ?></span>
                                                                </li>
                                                                <li>
                                                                    <span class="detail-left"><?php pll_e('Room Size', karisma_text_domain); ?></span>
                                                                    <span class="dot">:</span>
                                                                    <span class="detail-right"><?php pll_e(rwmb_meta('room_size'), karisma_text_domain); ?>
                                                                        m<sup>2</sup></span>
                                                                </li>
                                                                <li>
                                                                    <span class="detail-left"><?php pll_e('View', karisma_text_domain); ?></span>
                                                                    <span class="dot">:</span>
                                                                    <span class="detail-right"><?php pll_e(rwmb_meta('room_view')); ?></span>
                                                                </li>
                                                            </ul><!-- end .info-detail -->
                                                        </div>
                                                    </div>

                                                </div><!-- end .box-info -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                    <?php } ?>
                    <!-- end .row -->
                </div>
                <!-- end .container -->
            </div>
            <!-- end .content-intro -->

        <?php endwhile; ?>

    <?php else : ?>

        <!-- article -->
        <article>

            <h1>
                <?php pll_e('Sorry, nothing to display.', karisma_text_domain); ?>
            </h1>

        </article>
        <!-- /article -->

    <?php endif; ?>
</div>
<!-- end .content -->

<?php get_footer(); ?>
<script>
    var input = document.querySelector("#phone");
    if (input) {
        window.intlTelInput(input, {
            initialCountry: "id",
        });
    }
    jQuery(document).ready(function() {
        jQuery('#promotion_code').on('keyup', function() {
            var promocode = jQuery('#promotion_code').val();
            console.log(promocode);
        });
    });
</script>