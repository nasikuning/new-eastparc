<?php get_header(); ?>
<div id="wrapper" class="page">
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
      <?php
      $images = rwmb_meta('indohotels_imgpages', 'size=big-slider'); // Since 4.8.0
      if (!empty($images)) : ?>
        <div class="section main-slider slider-room">
          <div id="slider-main" class="owl-carousel">
            <?php
            foreach ($images as $image) {
              echo '<div class="owl-slide" style="background-image: url(\'' . $image['full_url'] . '\')"></div>';
            }
            ?>
          </div>
          <!-- end .slider-main -->
        </div>
        <!-- end .main-slider -->
      <?php endif; ?>
      <div class="container">
        <div class="section content-aboutus">
          <h1 class="heading-title" <?php echo empty($images) ? 'style="margin-top:80px"' : ''; ?>>
            <?php the_title(); ?>
          </h1>
          <?php the_content(); ?>
        </div>
        <!-- end .content-intro -->
      </div>
      <!-- end .container -->
    <?php endwhile; ?>

  <?php else : ?>

    <!-- article -->
    <article>

      <h1>
        <?php pll_e('Sorry, nothing to display.', karisma_text_domain); ?>
      </h1>

    </article>
    <!-- /article -->

  <?php endif; ?>
</div>
<!-- end .content -->

<?php get_footer(); ?>