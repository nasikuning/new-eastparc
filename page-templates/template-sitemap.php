<?php
/* Template Name: Sitemap Page Template */ get_header(); ?>

<div id="wrapper" class="page">

  <?php
  $images = rwmb_meta('indohotels_imgpages', 'size=big-slider'); // Since 4.8.0
  if (!empty($images)) : ?>
    <div class="section main-slider slider-room">
      <div id="slider-main" class="owl-carousel">
        <?php
        foreach ($images as $image) {
          echo '<div class="owl-slide" style="background-image: url(\'' . $image['full_url'] . '\')"></div>';
        }
        ?>
      </div>
      <!-- end .slider-main -->
    </div>
    <!-- end .main-slider -->
  <?php endif; ?>

  <div class="container">
    <div class="section content-sitemap">
      <h1 class="heading-title" <?php echo empty($images) ? 'style="margin-top:80px"' : ''; ?>>
        <?php _e(the_title(), karisma_text_domain); ?>
      </h1>

      <dic class="row">
        <div class="sort-map col-xs-4">
          <ul class="item-map">
            <li style="margin-bottom:20px"><a href="<?php echo get_site_url(); ?>/rooms-suites/">Rooms &amp; Suites</a>
              <ul class="page-list">
                <?php
                $args = array('post_type' => 'rooms');
                query_posts($args);
                if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <li class="page_item page-item-57">
                      <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    </li>
                  <?php endwhile; ?>
                <?php endif; ?>
              </ul>
            </li>
            <li style="margin-bottom:20px"><a href="<?php echo get_site_url(); ?>/meetings-events-list/">Meetings &amp; Events</a>
              <ul class="page-list">
                <?php
                $args = array('post_type' => 'meetings-events');
                query_posts($args);
                if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <li class="page_item page-item-57">
                      <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    </li>
                  <?php endwhile; ?>
                <?php endif; ?>
              </ul>
            </li>
          </ul>
        </div>
        <div class="sort-map col-xs-4">
          <ul class="item-map">
            <li style="margin-bottom:20px"><a href="<?php echo get_site_url(); ?>/restaurant-cafe-list/">Restaurant &amp; Cafe</a>
              <ul class="page-list">
                <?php
                $args = array('post_type' => 'restaurant-cafe');
                query_posts($args);
                if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <li class="page_item page-item-57">
                      <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    </li>
                  <?php endwhile; ?>
                <?php endif; ?>
              </ul>
            </li>
            <li style="margin-bottom:20px"><a href="<?php echo get_site_url(); ?>/facilities-services/">Facilities &amp; Services</a>
              <ul class="page-list">
                <?php
                $args = array(
                  'post_type' => 'hotel-info',
                  'category_name' => 'facilities',
                );
                query_posts($args);
                if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <li class="page_item page-item-57">
                      <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    </li>
                  <?php endwhile; ?>
                <?php endif; ?>
              </ul>
            </li>
            <li style="margin-bottom:20px"><a href="<?php echo get_site_url(); ?>/beauty-retail/">Beauty &amp; Retail</a>
              <ul class="page-list">
                <?php
                $args = array('post_type' => 'beauty-retail');
                query_posts($args);
                if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <li class="page_item page-item-57">
                      <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    </li>
                  <?php endwhile; ?>
                <?php endif; ?>
              </ul>
            </li>
          </ul>
        </div>
        <div class="sort-map col-xs-4">
          <ul class="item-map">
            <li style="margin-bottom:20px"><a href="<?php echo get_site_url(); ?>/deals/">Deals</a>
              <ul class="page-list">
                <?php
                $args = array('post_type' => 'deals');
                query_posts($args);
                if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <li class="page_item page-item-57">
                      <a href="<?php echo get_site_url(); ?>/deals/"><?php the_title(); ?></a>
                    </li>
                  <?php endwhile; ?>
                <?php endif; ?>
              </ul>
            </li>
          </ul>
        </div>
      </dic>

    </div>
    <!-- end .content-intro -->

  </div>
  <!-- end .container -->

</div>
<!-- end .content -->

<?php get_footer(); ?>