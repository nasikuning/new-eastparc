<?php
/* Template Name: Rooms Page Template */ get_header();
?>

<div id="wrapper" class="page">

  <?php
  $images = rwmb_meta('indohotels_imgpages', 'size=big-slider'); // Since 4.8.0
  if (!empty($images)) : ?>
    <div class="section main-slider slider-room">
      <div id="slider-main" class="owl-carousel">
        <?php
        foreach ($images as $image) {
          echo '<div class="owl-slide" style="background-image: url(\'' . $image['full_url'] . '\')"></div>';
        }
        ?>
      </div>
      <!-- end .slider-main -->
    </div>
    <!-- end .main-slider -->
  <?php endif; ?>


  <div class="container">
    <div class="section content-rooms">
      <h1 class="heading-title text-center" <?php echo empty($images) ? 'style="margin-top:80px"' : ''; ?>>
        <?php pll_e(the_title(), karisma_text_domain); ?></h1>
      <div class="row">
        <?php
        $args = array('post_type' => 'rooms');
        query_posts($args);
        if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="box-content">
                <div class="box-image">
                  <?php if (has_post_thumbnail()) : ?>
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                      <?php the_post_thumbnail('room-homepage'); ?>
                    </a>
                  <?php endif; ?>
                </div>
                <div class="box-info text-center">
                  <h2><?php the_title(); ?></h2>
                  <p class="subtitle"><?php pll_e(rwmb_meta('indohotels_room_balcony'), karisma_text_domain); ?></p>
                  <ul class="info-detail">
                    <li>
                      <span class="detail-left"><?php pll_e('Bed Size', karisma_text_domain); ?></span>
                      <span class="dot">:</span>
                      <span class="detail-right"><?php pll_e(rwmb_meta('bed_size'), karisma_text_domain); ?></span>
                    </li>
                    <li>
                      <span class="detail-left"><?php pll_e('Occupancy', karisma_text_domain); ?></span>
                      <span class="dot">:</span>
                      <span class="detail-right"><?php pll_e(rwmb_meta('room_occupancy'), karisma_text_domain); ?><?php pll_e('persons', karisma_text_domain); ?></span>
                    </li>
                    <li>
                      <span class="detail-left"><?php pll_e('Room Size', karisma_text_domain); ?></span>
                      <span class="dot">:</span>
                      <span class="detail-right"><?php pll_e(rwmb_meta('room_size'), karisma_text_domain); ?> m<sup>2</sup></span>
                    </li>
                    <li>
                      <span class="detail-left"><?php pll_e('View', karisma_text_domain); ?></span>
                      <span class="dot">:</span>
                      <span class="detail-right"><?php pll_e(rwmb_meta('room_view')); ?></span>
                    </li>
                  </ul><!-- end .info-detail -->
                  <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="nbutton"><?php pll_e('Room Details', karisma_text_domain); ?></a>
                </div><!-- end .box-info -->
              </div><!-- end .box-content -->
            </div><!-- end .col-md-6 -->
          <?php endwhile; ?>

        <?php else : ?>

          <!-- article -->
          <article>
            <h2><?php pll_e('Sorry, nothing to display.', karisma_text_domain); ?></h2>
          </article>
          <!-- /article -->

        <?php endif; ?>
      </div><!-- end .row -->
    </div><!-- end .content-intro -->
  </div><!-- end .container -->

</div><!-- end .content -->

<?php get_footer(); ?>