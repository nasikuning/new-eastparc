<?php
/*-----------------------------------------------*/
/* KARISMA FRAMEWORK
/* Version :1.01
/* You can add your own function here! 
/* But don't change anything in this file!
/*-----------------------------------------------*/

// require_once (TEMPLATEPATH.'/vendor/autoload.php');
// Load karisma framework
date_default_timezone_set('Asia/Jakarta');

require_once (TEMPLATEPATH . '/includes/init.php');
$karisma_declare = new KARISMA();
$karisma_declare->init();
?>