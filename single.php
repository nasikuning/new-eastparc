<?php get_header(); ?>
<div id="wrapper" class="page">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<div class="section main-slider">
				<div id="slider-main" class="owl-carousel">
					<?php
					$images = rwmb_meta('indohotels_imgpages', 'size=gallery-slide'); // Since 4.8.0

					if (!empty($images)) {
						foreach ($images as $image) {
							echo '<div class="owl-slide" style="background-image: url(\'' . $image['full_url'] . '\')"></div>';
						}
					} else if (has_post_thumbnail()) {
						echo '<div class="owl-slide" style="background-image: url(\'' . get_the_post_thumbnail_url(get_the_ID(), 'full') . '\')"></div>';
					} ?>
				</div><!-- end .slider-main -->
			</div><!-- end .main-slider -->
			<div class="container">
				<div class="section content-post">
					<h1 class="heading-title"><?php the_title(); ?></h1>
					<div class="meta-info">
						<?php the_date(); ?>
						</div>
						<div class="wrap-content">
							<?php the_content(); ?>
						</div>
				</div><!-- end .content-intro -->
			</div><!-- end .container -->
		<?php endwhile; ?>

	<?php else : ?>

		<!-- article -->
		<article>

			<h1><?php pll_e('Sorry, nothing to display.', karisma_text_domain); ?></h1>

		</article>
		<!-- /article -->

	<?php endif; ?>
</div><!-- end .content -->
<?php get_footer(); ?>