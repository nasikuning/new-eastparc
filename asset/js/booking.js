const hotelPricing = document.getElementById('krs_hotel_booking_pricing');
const krsPricingTable = document.querySelectorAll('.krs-pricing-table ');
if(hotelPricing && krsPricingTable) {
    if(hotelPricing.querySelector('#rooms')) {
        hotelPricing.querySelector('#rooms').addEventListener('change', function () {        
            window.location.href = window.location.href+'&krs-room='+this.value;
        })
    }
}

// document.querySelector('[name=pricing-table-form]').addEventListener('submit', function () {
//     const pricingTable = this.querySelectorAll('.krs-pricing-table');
//     for (let s = 0; s < pricingTable.length; s++) {
//         pricingTable[s].querySelector('input').getAttribute('name').replace(/__INDEX__/, i - 1000);
//     }
//     return false;
// });

jQuery('form[name="pricing-table-form"]').submit(function () {
    const pricingTable = this.querySelectorAll('.krs-pricing-table');
    for (let s = 0; s < pricingTable.length; s++) {
        var getInput = pricingTable[s].querySelectorAll('input');
        for (let i = 0; i < getInput.length; i++) {
            getInput[i].setAttribute('name') = getInput[i].getAttribute('name').replace(/__INDEX__/, s);
        }
    }
});


function enableEdit(e) {
    var pricingTable = e.target.closest('.krs-pricing-table');
    var pricingPrice = pricingTable.querySelectorAll('.krs-pricing-price');

    for (let pp = 0; pp < pricingPrice.length; pp++) {

        if(pricingPrice[pp].readOnly == false) {
            pricingPrice[pp].readOnly = true;
            e.target.classList.remove('dashicons-yes');
            e.target.classList.add('dashicons-edit');
        } else {
            pricingPrice[pp].readOnly = false;
            e.target.classList.remove('dashicons-edit');
            e.target.classList.add('dashicons-yes');
        }
    }
}



function bookingActionBtn(el) {
    var actionBtn = el.querySelectorAll('.krs-pricing-controls a');

    for (let a = 0; a < actionBtn.length; a++) {
        actionBtn[a].addEventListener('click', function (e) {
            switch (e.target.getAttribute('data-action')) {
                case 'edit':
                    enableEdit(e);
                break;
                case 'clone':
                    duplicatePriceTable(e);
                break;
                case 'remove':
                    removePriveTable(e);
                break;
    
                default:
            }
        })
        
    }
}

// function duplicatePriceTable(e) {
//     const tableRegular = e.target.closest('.krs-pricing-table');
//     const additionalPlan = document.querySelector('#krs-pricing-plan-list');
//     document.querySelector('#krs-no-plan-message').style.display = 'none';

//     var el = document.createElement('div');
//     el.innerHTML = tableRegular.outerHTML;

//     if(el.firstChild.classList.contains('regular-price')) {
//         el.firstChild.classList.remove('regular-price');
//     }
//     additionalPlan.appendChild(el.firstChild);
//     var lastPlan = additionalPlan.querySelectorAll('.krs-pricing-table');
    
//     console.log(lastPlan[lastPlan.length-1]);
//     bookingActionBtn(lastPlan[lastPlan.length-1]);
//     getDatepicker();
// }

function duplicatePriceTable(e) {
    const tableRegular = e.target.closest('.krs-pricing-table');
    const additionalPlan = document.querySelector('#krs-pricing-plan-list');
    document.querySelector('#krs-no-plan-message').style.display = 'none';

    var el = document.createElement('div');
    el.innerHTML = document.querySelector('#tmpl-krs-pricing-table').querySelector('.krs-pricing-table').outerHTML;

    if(el.firstChild.classList.contains('regular-price')) {
        el.firstChild.classList.remove('regular-price');
    }
    additionalPlan.appendChild(el.firstChild);

    var lastPlan = additionalPlan.querySelectorAll('.krs-pricing-table');
    
    bookingActionBtn(lastPlan[lastPlan.length-1]);
    getDatepicker();
}

function removePriveTable(e) {
    const tableRegular = e.target.closest('.krs-pricing-table');
    var alertRemove = confirm('Are you sure you want to remove this pricing table?');
    if(alertRemove) {
        tableRegular.remove();
    }
}
if(document.querySelector('.krs-pricing-table')) {
    // var krsPricingTable = document.querySelectorAll('.krs-pricing-table');

}

function getDatepicker() {
    jQuery(document).ready(function($) {
        $('.datepicker').datepicker({
            dateFormat : 'yy-mm-dd'
        });
    });
}

