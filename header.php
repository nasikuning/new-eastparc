<!doctype html>
<html <?php language_attributes(); ?> class="no-js">

<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <?php wp_head(); ?>

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-137038874-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-137038874-1', {
      'optimize_id': 'GTM-KMWT5SH'
    });
  </script>

  <!-- Google Tag Manager -->
  <script>
    (function(w, d, s, l, i) {
      w[l] = w[l] || [];
      w[l].push({
        'gtm.start': new Date().getTime(),
        event: 'gtm.js'
      });
      var f = d.getElementsByTagName(s)[0],
        j = d.createElement(s),
        dl = l != 'dataLayer' ? '&l=' + l : '';
      j.async = true;
      j.src =
        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
      f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-P63C5HS');
  </script>
  <!-- End Google Tag Manager -->

  <!-- Facebook Pixel Code -->
  <script>
    ! function(f, b, e, v, n, t, s) {
      if (f.fbq) return;
      n = f.fbq = function() {
        n.callMethod ?
          n.callMethod.apply(n, arguments) : n.queue.push(arguments)
      };
      if (!f._fbq) f._fbq = n;
      n.push = n;
      n.loaded = !0;
      n.version = '2.0';
      n.queue = [];
      t = b.createElement(e);
      t.async = !0;
      t.src = v;
      s = b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
      'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '749465412193808');
    fbq('track', 'PageView');
  </script>
  <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=749465412193808&ev=PageView&noscript=1" /></noscript>
  <!-- End Facebook Pixel Code -->

  <script id="mcjs">
    ! function(c, h, i, m, p) {
      m = c.createElement(h), p = c.getElementsByTagName(h)[0], m.async = 1, m.src = i, p.parentNode.insertBefore(m, p)
    }(document, "script", "https://chimpstatic.com/mcjs-connected/js/users/d4448ccd35296635488ac223f/3061e62447a41ff8c3c40d357.js");
  </script>
</head>

<body <?php body_class(get_locale()); ?>>

  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P63C5HS" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->

  <header>
    <div class="section-top">
      <div class="container clearfix">
        <div class="top-logo pull-left">
          <?php krs_headlogo(); ?>
        </div>
        <div class="pull-right">
          <?php if (is_active_sidebar('home-top-widget')) : ?>
            <div id="home-top-widget" class="home-top-widget widget-area" role="complementary">
              <ul class="home-top-widget">
                <?php dynamic_sidebar('home-top-widget'); ?>
              </ul>
            </div>
            <!-- #home-top-widget -->
          <?php endif; ?>
        </div>
      </div>
      <!-- end .container -->

    </div>
    <!-- end .section-top -->
    <div class="navbar navbar-default" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" onclick="openNav()">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>

        </div>
        <div class="navbar-collapse collapse">
          <?php karisma_nav(); ?>
        </div>
        <!-- mobile menu -->
        <div class="mobmenu">
          <div id="mobinav" class="sidenav">
            <div class="mobinav-inner login">
              <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
              <?php karisma_nav_mobile(); ?>
            </div>
          </div>
          <!-- end .mobinav -->
          <span class="icon-list-add mobico" onclick="openNav()"></span>
        </div>
        <!-- end .mobmenu -->
      </div>
    </div>
  </header>