<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class('blog-post'); ?>>
			<div class="row">
				<div class="col-md-4">
					<!-- post thumbnail -->
					<div class="thumb">
						<?php if (has_post_thumbnail()) : // Check if thumbnail exists 
						?>
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
								<?php the_post_thumbnail('gallery-slide', array('class' => 'img-responsive')); // Declare pixel size you need inside the array 
								?>
							</a>
						<?php endif; ?>
					</div>
					<!-- /post thumbnail -->
				</div>
				<div class="col-md-8">
					<div class="box-info">
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
							<h2><?php the_title(); ?></h2>
						</a>
						<div class="meta-info">
						<?php the_date(); ?>
						</div>

						<?php the_excerpt(); ?>
						<div class="read-more"><a class="nbutton" href="<?php the_permalink(); ?>" rel="nofollow">Read More</a></div>
					</div><!-- end .box-info -->
				</div>
			</div>
		</article>
		<!-- /article -->
	<?php endwhile; ?>
<?php else : ?>
	<article>
		<h2>
			<?php pll_e('Sorry, nothing to display.', karisma_text_domain); ?>
		</h2>
	</article>
<?php endif; ?>
